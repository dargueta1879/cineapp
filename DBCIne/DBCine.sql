USE [DBCine]
GO
/****** Object:  Table [dbo].[Asiento]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asiento](
	[AsiendoId] [int] IDENTITY(1,1) NOT NULL,
	[NombreAsiento] [varchar](100) NULL,
	[Ala] [varchar](10) NULL,
	[EstadoAsiendoId] [int] NOT NULL,
	[SalaId] [int] NOT NULL,
 CONSTRAINT [PK3] PRIMARY KEY NONCLUSTERED 
(
	[AsiendoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Asociado]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asociado](
	[AsociadoId] [int] IDENTITY(1,1) NOT NULL,
	[SocioId] [int] NOT NULL,
	[CineId] [int] NOT NULL,
 CONSTRAINT [PK12] PRIMARY KEY NONCLUSTERED 
(
	[AsociadoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cartelera]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cartelera](
	[CarteleraId] [int] IDENTITY(1,1) NOT NULL,
	[HorarioId] [int] NOT NULL,
	[PeliculaId] [int] NOT NULL,
	[SalaId] [int] NOT NULL,
	[EstadoCarteleraId] [int] NOT NULL,
 CONSTRAINT [PK22] PRIMARY KEY NONCLUSTERED 
(
	[CarteleraId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cine]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cine](
	[CineId] [int] IDENTITY(1,1) NOT NULL,
	[NombreCine] [varchar](100) NULL,
	[Direccion] [varchar](100) NULL,
	[Departamento] [varchar](100) NULL,
 CONSTRAINT [PK1] PRIMARY KEY NONCLUSTERED 
(
	[CineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Descuento]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Descuento](
	[TipoDescuentoId] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoDescuento] [varchar](100) NULL,
	[TasaDescuento] [numeric](10, 2) NULL,
	[EstadoDescuentoId] [int] NOT NULL,
 CONSTRAINT [PK20] PRIMARY KEY NONCLUSTERED 
(
	[TipoDescuentoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetallePuntos]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetallePuntos](
	[DetallePuntosId] [int] IDENTITY(1,1) NOT NULL,
	[NombreDetalle] [varchar](100) NULL,
	[NumPuntos] [int] NULL,
	[SocioId] [int] NOT NULL,
 CONSTRAINT [PK26] PRIMARY KEY NONCLUSTERED 
(
	[DetallePuntosId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleTicket]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleTicket](
	[DetTicketId] [int] IDENTITY(1,1) NOT NULL,
	[ProductoId] [int] NOT NULL,
	[AsiendoId] [int] NULL,
	[TicketId] [int] NOT NULL,
	[TipoDescuentoId] [int] NULL,
	[CarteleraId] [int] NULL,
 CONSTRAINT [PK19] PRIMARY KEY NONCLUSTERED 
(
	[DetTicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[EmpleadoId] [int] IDENTITY(1,1) NOT NULL,
	[DPI] [varchar](100) NULL,
	[Nombres] [varchar](100) NULL,
	[Apellidos] [varchar](100) NULL,
	[EstadoEmpleadoId] [int] NULL,
 CONSTRAINT [PK7] PRIMARY KEY NONCLUSTERED 
(
	[EmpleadoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoAsiento]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoAsiento](
	[EstadoAsiendoId] [int] IDENTITY(1,1) NOT NULL,
	[NombreEsado] [varchar](100) NULL,
	[Descripcion] [varchar](100) NULL,
 CONSTRAINT [PK4] PRIMARY KEY NONCLUSTERED 
(
	[EstadoAsiendoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoCartelera]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoCartelera](
	[EstadoCarteleraId] [int] IDENTITY(1,1) NOT NULL,
	[NombreEstadoCartelera] [varchar](100) NULL,
	[Descripcion] [varchar](100) NULL,
 CONSTRAINT [PK24] PRIMARY KEY NONCLUSTERED 
(
	[EstadoCarteleraId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoDescuento]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoDescuento](
	[EstadoDescuentoId] [int] IDENTITY(1,1) NOT NULL,
	[NombreEstado] [varchar](100) NULL,
	[DescripcionEstadoDescuento] [varchar](100) NULL,
 CONSTRAINT [PK23] PRIMARY KEY NONCLUSTERED 
(
	[EstadoDescuentoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoEmpleado]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoEmpleado](
	[EstadoEmpleadoId] [int] IDENTITY(1,1) NOT NULL,
	[NombreEstadoEmpleado] [varchar](100) NULL,
 CONSTRAINT [PK27] PRIMARY KEY NONCLUSTERED 
(
	[EstadoEmpleadoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoPelicula]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoPelicula](
	[EstadoPeliculaId] [int] IDENTITY(1,1) NOT NULL,
	[NombreEstadoPelicula] [varchar](100) NULL,
	[Descripcion] [varchar](100) NULL,
 CONSTRAINT [PK25] PRIMARY KEY NONCLUSTERED 
(
	[EstadoPeliculaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoSocio]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoSocio](
	[EstadoSocioId] [int] NOT NULL,
	[NombreEstado] [varchar](100) NULL,
 CONSTRAINT [PK10] PRIMARY KEY NONCLUSTERED 
(
	[EstadoSocioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstadoTicket]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstadoTicket](
	[EstadoTicketId] [int] IDENTITY(1,1) NOT NULL,
	[NombreEstadoTicket] [varchar](100) NULL,
 CONSTRAINT [PK17] PRIMARY KEY NONCLUSTERED 
(
	[EstadoTicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Existencia]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Existencia](
	[ExistenciaId] [int] IDENTITY(1,1) NOT NULL,
	[ProductoId] [int] NOT NULL,
	[CineId] [int] NOT NULL,
	[Cantidad] [int] NULL,
	[PrecioCompra] [int] NULL,
	[PrecioPuntos] [int] NULL,
 CONSTRAINT [PK6] PRIMARY KEY NONCLUSTERED 
(
	[ExistenciaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Horario]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horario](
	[HorarioId] [int] IDENTITY(1,1) NOT NULL,
	[Dia] [varchar](100) NULL,
	[HoraInicio] [varchar](100) NULL,
	[HoraFin] [varchar](100) NULL,
 CONSTRAINT [PK15] PRIMARY KEY NONCLUSTERED 
(
	[HorarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pelicula]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pelicula](
	[PeliculaId] [int] IDENTITY(1,1) NOT NULL,
	[NombrePelicula] [varchar](100) NULL,
	[Duracion] [varchar](100) NULL,
	[Clasificacion] [varchar](100) NULL,
	[EstadoPeliculaId] [int] NULL,
 CONSTRAINT [PK14] PRIMARY KEY NONCLUSTERED 
(
	[PeliculaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Planilla]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Planilla](
	[PlanillaId] [int] IDENTITY(1,1) NOT NULL,
	[EmpleadoId] [int] NOT NULL,
	[CineId] [int] NOT NULL,
 CONSTRAINT [PK8] PRIMARY KEY NONCLUSTERED 
(
	[PlanillaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[ProductoId] [int] IDENTITY(1,1) NOT NULL,
	[NombreProducto] [varchar](100) NULL,
	[Descripcion] [varchar](100) NULL,
	[RutaFoto] [varchar](255) NULL,
 CONSTRAINT [PK5] PRIMARY KEY NONCLUSTERED 
(
	[ProductoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleInUser]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleInUser](
	[RoleInUserId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[UserId] [int] NULL,
	[PlanillaId] [int] NULL,
 CONSTRAINT [PK_RoleInUser] PRIMARY KEY CLUSTERED 
(
	[RoleInUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sala]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sala](
	[SalaId] [int] IDENTITY(1,1) NOT NULL,
	[NombreSala] [varchar](100) NULL,
	[CineId] [int] NOT NULL,
 CONSTRAINT [PK2] PRIMARY KEY NONCLUSTERED 
(
	[SalaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Socio]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Socio](
	[SocioId] [int] IDENTITY(1,1) NOT NULL,
	[SocioRecomendadoId] [int] NULL,
	[CodCliente] [varchar](100) NULL,
	[Nombres] [varchar](100) NULL,
	[Apellidos] [varchar](100) NULL,
	[DiaNacimiento] [varchar](100) NULL,
	[MesNacimiento] [varchar](100) NULL,
	[AñoNacimiento] [varchar](100) NULL,
	[NumPuntos] [varchar](100) NULL,
	[CarneEscolar] [varchar](100) NULL,
	[CarneAnciano] [varchar](100) NULL,
	[EstadoSocioId] [int] NOT NULL,
	[TipoSocioId] [int] NOT NULL,
	[Contraseña] [varchar](100) NULL,
 CONSTRAINT [PK9] PRIMARY KEY NONCLUSTERED 
(
	[SocioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ticket](
	[TicketId] [int] IDENTITY(1,1) NOT NULL,
	[CineId] [int] NOT NULL,
	[SocioId] [int] NULL,
	[EstadoTicketId] [int] NULL,
	[Dia] [varchar](100) NULL,
	[Mes] [varchar](10) NULL,
	[Ano] [varchar](100) NULL,
	[Minutos] [int] NULL,
	[Hora] [int] NULL,
	[Contrasena] [varchar](100) NULL,
 CONSTRAINT [PK13] PRIMARY KEY NONCLUSTERED 
(
	[TicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoSocio]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoSocio](
	[TipoSocioId] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoSocio] [varchar](100) NULL,
	[Descripcion] [varchar](100) NULL,
 CONSTRAINT [PK11] PRIMARY KEY NONCLUSTERED 
(
	[TipoSocioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](56) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](128) NULL,
	[IsConfirmed] [bit] NULL,
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordChangedDate] [datetime] NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[PasswordVerificationToken] [nvarchar](128) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](30) NOT NULL,
	[ProviderUserId] [nvarchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 16/04/2018 3:57:24 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[PlanillaId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [IsConfirmed]
GO
ALTER TABLE [dbo].[webpages_Membership] ADD  DEFAULT ((0)) FOR [PasswordFailuresSinceLastSuccess]
GO
ALTER TABLE [dbo].[Asiento]  WITH CHECK ADD  CONSTRAINT [RefEstadoAsiento9] FOREIGN KEY([EstadoAsiendoId])
REFERENCES [dbo].[EstadoAsiento] ([EstadoAsiendoId])
GO
ALTER TABLE [dbo].[Asiento] CHECK CONSTRAINT [RefEstadoAsiento9]
GO
ALTER TABLE [dbo].[Asiento]  WITH CHECK ADD  CONSTRAINT [RefSala8] FOREIGN KEY([SalaId])
REFERENCES [dbo].[Sala] ([SalaId])
GO
ALTER TABLE [dbo].[Asiento] CHECK CONSTRAINT [RefSala8]
GO
ALTER TABLE [dbo].[Asociado]  WITH CHECK ADD  CONSTRAINT [RefCine13] FOREIGN KEY([CineId])
REFERENCES [dbo].[Cine] ([CineId])
GO
ALTER TABLE [dbo].[Asociado] CHECK CONSTRAINT [RefCine13]
GO
ALTER TABLE [dbo].[Asociado]  WITH CHECK ADD  CONSTRAINT [RefSocio12] FOREIGN KEY([SocioId])
REFERENCES [dbo].[Socio] ([SocioId])
GO
ALTER TABLE [dbo].[Asociado] CHECK CONSTRAINT [RefSocio12]
GO
ALTER TABLE [dbo].[Cartelera]  WITH CHECK ADD  CONSTRAINT [RefEstadoCartelera29] FOREIGN KEY([EstadoCarteleraId])
REFERENCES [dbo].[EstadoCartelera] ([EstadoCarteleraId])
GO
ALTER TABLE [dbo].[Cartelera] CHECK CONSTRAINT [RefEstadoCartelera29]
GO
ALTER TABLE [dbo].[Cartelera]  WITH CHECK ADD  CONSTRAINT [RefHorario25] FOREIGN KEY([HorarioId])
REFERENCES [dbo].[Horario] ([HorarioId])
GO
ALTER TABLE [dbo].[Cartelera] CHECK CONSTRAINT [RefHorario25]
GO
ALTER TABLE [dbo].[Cartelera]  WITH CHECK ADD  CONSTRAINT [RefPelicula24] FOREIGN KEY([PeliculaId])
REFERENCES [dbo].[Pelicula] ([PeliculaId])
GO
ALTER TABLE [dbo].[Cartelera] CHECK CONSTRAINT [RefPelicula24]
GO
ALTER TABLE [dbo].[Cartelera]  WITH CHECK ADD  CONSTRAINT [RefSala26] FOREIGN KEY([SalaId])
REFERENCES [dbo].[Sala] ([SalaId])
GO
ALTER TABLE [dbo].[Cartelera] CHECK CONSTRAINT [RefSala26]
GO
ALTER TABLE [dbo].[Descuento]  WITH CHECK ADD  CONSTRAINT [RefEstadoDescuento28] FOREIGN KEY([EstadoDescuentoId])
REFERENCES [dbo].[EstadoDescuento] ([EstadoDescuentoId])
GO
ALTER TABLE [dbo].[Descuento] CHECK CONSTRAINT [RefEstadoDescuento28]
GO
ALTER TABLE [dbo].[DetallePuntos]  WITH CHECK ADD  CONSTRAINT [RefSocio31] FOREIGN KEY([SocioId])
REFERENCES [dbo].[Socio] ([SocioId])
GO
ALTER TABLE [dbo].[DetallePuntos] CHECK CONSTRAINT [RefSocio31]
GO
ALTER TABLE [dbo].[DetalleTicket]  WITH CHECK ADD  CONSTRAINT [RefAsiento20] FOREIGN KEY([AsiendoId])
REFERENCES [dbo].[Asiento] ([AsiendoId])
GO
ALTER TABLE [dbo].[DetalleTicket] CHECK CONSTRAINT [RefAsiento20]
GO
ALTER TABLE [dbo].[DetalleTicket]  WITH CHECK ADD  CONSTRAINT [RefCartelera35] FOREIGN KEY([CarteleraId])
REFERENCES [dbo].[Cartelera] ([CarteleraId])
GO
ALTER TABLE [dbo].[DetalleTicket] CHECK CONSTRAINT [RefCartelera35]
GO
ALTER TABLE [dbo].[DetalleTicket]  WITH CHECK ADD  CONSTRAINT [RefDescuento23] FOREIGN KEY([TipoDescuentoId])
REFERENCES [dbo].[Descuento] ([TipoDescuentoId])
GO
ALTER TABLE [dbo].[DetalleTicket] CHECK CONSTRAINT [RefDescuento23]
GO
ALTER TABLE [dbo].[DetalleTicket]  WITH CHECK ADD  CONSTRAINT [RefProducto19] FOREIGN KEY([ProductoId])
REFERENCES [dbo].[Producto] ([ProductoId])
GO
ALTER TABLE [dbo].[DetalleTicket] CHECK CONSTRAINT [RefProducto19]
GO
ALTER TABLE [dbo].[DetalleTicket]  WITH CHECK ADD  CONSTRAINT [RefTicket22] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Ticket] ([TicketId])
GO
ALTER TABLE [dbo].[DetalleTicket] CHECK CONSTRAINT [RefTicket22]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [RefEstadoEmpleado33] FOREIGN KEY([EstadoEmpleadoId])
REFERENCES [dbo].[EstadoEmpleado] ([EstadoEmpleadoId])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [RefEstadoEmpleado33]
GO
ALTER TABLE [dbo].[Existencia]  WITH CHECK ADD  CONSTRAINT [RefCine2] FOREIGN KEY([CineId])
REFERENCES [dbo].[Cine] ([CineId])
GO
ALTER TABLE [dbo].[Existencia] CHECK CONSTRAINT [RefCine2]
GO
ALTER TABLE [dbo].[Existencia]  WITH CHECK ADD  CONSTRAINT [RefProducto1] FOREIGN KEY([ProductoId])
REFERENCES [dbo].[Producto] ([ProductoId])
GO
ALTER TABLE [dbo].[Existencia] CHECK CONSTRAINT [RefProducto1]
GO
ALTER TABLE [dbo].[Pelicula]  WITH CHECK ADD  CONSTRAINT [RefEstadoPelicula30] FOREIGN KEY([EstadoPeliculaId])
REFERENCES [dbo].[EstadoPelicula] ([EstadoPeliculaId])
GO
ALTER TABLE [dbo].[Pelicula] CHECK CONSTRAINT [RefEstadoPelicula30]
GO
ALTER TABLE [dbo].[Planilla]  WITH CHECK ADD  CONSTRAINT [RefCine6] FOREIGN KEY([CineId])
REFERENCES [dbo].[Cine] ([CineId])
GO
ALTER TABLE [dbo].[Planilla] CHECK CONSTRAINT [RefCine6]
GO
ALTER TABLE [dbo].[Planilla]  WITH CHECK ADD  CONSTRAINT [RefEmpleado5] FOREIGN KEY([EmpleadoId])
REFERENCES [dbo].[Empleado] ([EmpleadoId])
GO
ALTER TABLE [dbo].[Planilla] CHECK CONSTRAINT [RefEmpleado5]
GO
ALTER TABLE [dbo].[RoleInUser]  WITH CHECK ADD  CONSTRAINT [FK_RoleInUser_Planilla] FOREIGN KEY([PlanillaId])
REFERENCES [dbo].[Planilla] ([PlanillaId])
GO
ALTER TABLE [dbo].[RoleInUser] CHECK CONSTRAINT [FK_RoleInUser_Planilla]
GO
ALTER TABLE [dbo].[RoleInUser]  WITH CHECK ADD  CONSTRAINT [FK_RoleInUser_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[RoleInUser] CHECK CONSTRAINT [FK_RoleInUser_UserProfile]
GO
ALTER TABLE [dbo].[RoleInUser]  WITH CHECK ADD  CONSTRAINT [FK_RoleInUser_webpages_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[RoleInUser] CHECK CONSTRAINT [FK_RoleInUser_webpages_Roles]
GO
ALTER TABLE [dbo].[Sala]  WITH CHECK ADD  CONSTRAINT [RefCine7] FOREIGN KEY([CineId])
REFERENCES [dbo].[Cine] ([CineId])
GO
ALTER TABLE [dbo].[Sala] CHECK CONSTRAINT [RefCine7]
GO
ALTER TABLE [dbo].[Socio]  WITH CHECK ADD  CONSTRAINT [RefEstadoSocio10] FOREIGN KEY([EstadoSocioId])
REFERENCES [dbo].[EstadoSocio] ([EstadoSocioId])
GO
ALTER TABLE [dbo].[Socio] CHECK CONSTRAINT [RefEstadoSocio10]
GO
ALTER TABLE [dbo].[Socio]  WITH CHECK ADD  CONSTRAINT [RefSocio34] FOREIGN KEY([SocioRecomendadoId])
REFERENCES [dbo].[Socio] ([SocioId])
GO
ALTER TABLE [dbo].[Socio] CHECK CONSTRAINT [RefSocio34]
GO
ALTER TABLE [dbo].[Socio]  WITH CHECK ADD  CONSTRAINT [RefTipoSocio11] FOREIGN KEY([TipoSocioId])
REFERENCES [dbo].[TipoSocio] ([TipoSocioId])
GO
ALTER TABLE [dbo].[Socio] CHECK CONSTRAINT [RefTipoSocio11]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [RefCine16] FOREIGN KEY([CineId])
REFERENCES [dbo].[Cine] ([CineId])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [RefCine16]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [RefEstadoTicket18] FOREIGN KEY([EstadoTicketId])
REFERENCES [dbo].[EstadoTicket] ([EstadoTicketId])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [RefEstadoTicket18]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [RefSocio17] FOREIGN KEY([SocioId])
REFERENCES [dbo].[Socio] ([SocioId])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [RefSocio17]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_RoleId]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_UserId]
GO
