//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CineApp.Persistence
{
    using System;
    using System.Collections.Generic;
    
    public partial class Planilla
    {
        public Planilla()
        {
            this.RoleInUser = new HashSet<RoleInUser>();
        }
    
        public int PlanillaId { get; set; }
        public int EmpleadoId { get; set; }
        public int CineId { get; set; }
    
        public virtual Cine Cine { get; set; }
        public virtual Empleado Empleado { get; set; }
        public virtual ICollection<RoleInUser> RoleInUser { get; set; }
    }
}
