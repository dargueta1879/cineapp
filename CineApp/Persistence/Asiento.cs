//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CineApp.Persistence
{
    using System;
    using System.Collections.Generic;
    
    public partial class Asiento
    {
        public Asiento()
        {
            this.DetalleTicket = new HashSet<DetalleTicket>();
        }
    
        public int AsiendoId { get; set; }
        public string NombreAsiento { get; set; }
        public string Ala { get; set; }
        public int EstadoAsiendoId { get; set; }
        public int SalaId { get; set; }
    
        public virtual ICollection<DetalleTicket> DetalleTicket { get; set; }
        public virtual EstadoAsiento EstadoAsiento { get; set; }
        public virtual Sala Sala { get; set; }
    }
}
