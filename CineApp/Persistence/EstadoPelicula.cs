//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CineApp.Persistence
{
    using System;
    using System.Collections.Generic;
    
    public partial class EstadoPelicula
    {
        public EstadoPelicula()
        {
            this.Pelicula = new HashSet<Pelicula>();
        }
    
        public int EstadoPeliculaId { get; set; }
        public string NombreEstadoPelicula { get; set; }
        public string Descripcion { get; set; }
    
        public virtual ICollection<Pelicula> Pelicula { get; set; }
    }
}
