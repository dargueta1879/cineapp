//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CineApp.Persistence
{
    using System;
    using System.Collections.Generic;
    
    public partial class Socio
    {
        public Socio()
        {
            this.Asociado = new HashSet<Asociado>();
            this.DetallePuntos = new HashSet<DetallePuntos>();
            this.Ticket = new HashSet<Ticket>();
            this.Socio1 = new HashSet<Socio>();
        }
    
        public int SocioId { get; set; }
        public Nullable<int> SocioRecomendadoId { get; set; }
        public string CodCliente { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string DiaNacimiento { get; set; }
        public string MesNacimiento { get; set; }
        public string AñoNacimiento { get; set; }
        public string NumPuntos { get; set; }
        public string CarneEscolar { get; set; }
        public string CarneAnciano { get; set; }
        public int EstadoSocioId { get; set; }
        public int TipoSocioId { get; set; }
        public string Contraseña { get; set; }
    
        public virtual ICollection<Asociado> Asociado { get; set; }
        public virtual ICollection<DetallePuntos> DetallePuntos { get; set; }
        public virtual EstadoSocio EstadoSocio { get; set; }
        public virtual ICollection<Ticket> Ticket { get; set; }
        public virtual ICollection<Socio> Socio1 { get; set; }
        public virtual Socio Socio2 { get; set; }
        public virtual TipoSocio TipoSocio { get; set; }
    }
}
