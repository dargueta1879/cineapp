﻿using AutentitacionProyect.Repositories;
using Elecciones.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Elecciones.Controllers
{
    public class GeneralController: Controller
    {

         private readonly IUnitOfWork _uow;
         public GeneralController(IUnitOfWork uow)
        {
            _uow = uow;
        }

         public ActionResult Gestionar()
         {
             return View("Gestion");
         }



    }
}