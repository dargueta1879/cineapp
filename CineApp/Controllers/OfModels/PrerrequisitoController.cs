﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
//using Elecciones.Core.Model;
//using Elecciones.Core.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
//using Elecciones.Models;

namespace AutentitacionProyect.Controllers
{
    public class PrerrequisitoController : Controller
    {
        private readonly IUnitOfWork _uow;
        public PrerrequisitoController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Prerrequisito
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetCurso()
        {
            var data = _uow.CursoRepository.GetAll().Select(x => new
            {
                x.CodCurso,
                x.NombreCurso
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, int DetallePensumId)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("PrerrequisitoId", ListSortDirection.Ascending));
            }





            var data = _uow.PrerrequisitoRepository.GetPrerrequisitoGridData(DetallePensumId).ToDataSourceResult(request);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, PrerrequisitoGridItem item)
        {
            //item.DetallePensumId = sessiones.getSession("DetallePensumId");
            var indexExist = _uow.PrerrequisitoRepository.FindBy(x => x.DetallePensumId == item.DetallePensumId && x.CursoId == item.CursoId).Any();

            if (indexExist)
            {
                var errorMessage = "El curso: " + item.NombreCurso + " ya es prerrequisito";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var prerrequisitoNuevo = new Prerrequisito
            {
                CursoId = item.CursoId,
                DetallePensumId=item.DetallePensumId,
                
            };

            _uow.PrerrequisitoRepository.Add(prerrequisitoNuevo);

            _uow.Save();

            

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, PrerrequisitoGridItem item)
        {
            var indexExist = _uow.PrerrequisitoRepository.FindBy(x => x.CursoId == item.CursoId && x.DetallePensumId == item.DetallePensumId && x.PrerrequisitoId != item.PrerrequisitoId).Any();
            if (indexExist)
            {
                var errorMessage = "El Prerrequisito: " + item.CursoId + " ya existe dentro del detalle del pensum, por favor verifique los datos a actualizar";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var prerrequisitoAEditar = _uow.PrerrequisitoRepository.GetById(item.PrerrequisitoId);
            prerrequisitoAEditar.CursoId = item.CursoId;
            prerrequisitoAEditar.DetallePensumId = item.DetallePensumId;

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, PrerrequisitoGridItem item)
        {


            var prerrequisitoAEditar = _uow.PrerrequisitoRepository.GetById(item.PrerrequisitoId);

            _uow.PrerrequisitoRepository.Delete(prerrequisitoAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        //public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        //{

        //    var data = _uow.SalonRepository.GetSalonGridData();

        //    if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
        //    if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending)); }

        //    var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

        //    var export = new XlsExportCollections
        //    {
        //        Title = "Salones",
        //        RightFooter = XlsHeaderFooter.PageNumber,
        //        PaperSize = XlsEnums.PaperSize.Legal,
        //        PageOrientation = XlsEnums.PageOrientation.Landscape,
        //        RepeatHeader = true,
        //        DecamelizeColumnNames = true
        //    };

        //    export.HiddenColumnsName.Add("CodEdificio");

        //    export.PrepareWorkbook(filteredData);
        //    export.DumpWorkbook("Salones", HttpContext.ApplicationInstance.Response);

        //    return null;
        //}

    }
}