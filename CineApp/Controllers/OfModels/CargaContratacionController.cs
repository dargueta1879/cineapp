﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
using AutentitacionProyect.Models;
using AutentitacionProyect.Filters;

namespace AutentitacionProyect.Controllers
{
    [InitializeSimpleMembership] 
    public class CargaContratacionController : Controller
    {
        private AcademicLoadDBEntities db = new AcademicLoadDBEntities();
          private readonly IUnitOfWork _uow;
          public CargaContratacionController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        //
        // GET: /CargaAcademica/

        public ActionResult Index()
        {


            Autentication permiso = new Autentication(_uow);
            if (permiso.isUserInRole("CargaAcademica", "Index"))
            {

                var cargaacademica = _uow.CargaAcademicaRepository.GetCargasAcademicas();
                return View(cargaacademica);

            }








            return RedirectToAction("Login", "Account");


        }

        //
        // GET: /CargaAcademica/Details/5

        public ActionResult Details(decimal id = 0)
        {
            CargaAcademica cargaacademica = db.CargaAcademica.Find(id);
            if (cargaacademica == null)
            {
                return HttpNotFound();
            }
            return View(cargaacademica);
        }

        //
        // GET: /CargaAcademica/Create

        public ActionResult Create()
        {
            ViewBag.FacultadId = new SelectList(db.Facultad, "FacultadId", "NombreFacultad");
            return View();
        }

        //
        // POST: /CargaAcademica/Create

        [HttpPost]
        public ActionResult Create(CargaAcademica cargaacademica)
        {
            if (ModelState.IsValid)
            {
                db.CargaAcademica.Add(cargaacademica);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FacultadId = new SelectList(db.Facultad, "FacultadId", "Estado", cargaacademica.FacultadId);
            return View(cargaacademica);
        }

        //
        // GET: /CargaAcademica/Edit/5

        public ActionResult Edit(decimal id = 0)
        {
            CargaAcademica cargaacademica = db.CargaAcademica.Find(id);
            if (cargaacademica == null)
            {
                return HttpNotFound();
            }
            ViewBag.FacultadId = new SelectList(db.Facultad, "FacultadId", "Estado", cargaacademica.FacultadId);
            return View(cargaacademica);
        }

        //
        // POST: /CargaAcademica/Edit/5

        [HttpPost]
        public ActionResult Edit(CargaAcademica cargaacademica)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cargaacademica).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FacultadId = new SelectList(db.Facultad, "FacultadId", "Estado", cargaacademica.FacultadId);
            return View(cargaacademica);
        }

        //
        // GET: /CargaAcademica/Delete/5

        public ActionResult Delete(decimal id = 0)
        {
            CargaAcademica cargaacademica = db.CargaAcademica.Find(id);
            if (cargaacademica == null)
            {
                return HttpNotFound();
            }
            return View(cargaacademica);
        }

        //
        // POST: /CargaAcademica/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(decimal id)
        {
            CargaAcademica cargaacademica = db.CargaAcademica.Find(id);
            db.CargaAcademica.Remove(cargaacademica);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}