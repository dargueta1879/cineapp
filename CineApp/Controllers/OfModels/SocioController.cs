﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;
using System.IO;
using WebMatrix.WebData;
using System.Web.Security;
using CineApp.Filters;

namespace CineApp.Controllers.OfModels
{
   
    public class SocioController : Controller
    {
        private DBCineEntities db = new DBCineEntities();
        private readonly IUnitOfWork _uow;
        public SocioController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        //

        //
        // GET: /Socio/

        public ActionResult Index()
        {
            var socio = db.Socio.Include(s => s.EstadoSocio).Include(s => s.Socio2).Include(s => s.TipoSocio);
            return View(socio.ToList());
        }

        //
        // GET: /Socio/Details/5

        public ActionResult Details(int id = 0)
        {
            Socio socio = db.Socio.Find(id);
            if (socio == null)
            {
                return HttpNotFound();
            }
            return View(socio);
        }

        //
        // GET: /Socio/Create

        public ActionResult Create()
        {
            ViewBag.EstadoSocioId = new SelectList(db.EstadoSocio, "EstadoSocioId", "NombreEstado");
            ViewBag.SocioRecomendadoId = new SelectList(db.Socio, "SocioId", "CodCliente");
            ViewBag.TipoSocioId = new SelectList(db.TipoSocio, "TipoSocioId", "NombreTipoSocio");
            return View();
        }

        public ActionResult Registro()
        {

            return View();
        }

        [InitializeSimpleMembership]
        [HttpPost]
        public ActionResult Registro(NuevoSocioClass socio)
        {
            String ano = socio.fecha.Split('-')[0];
            String mes = socio.fecha.Split('-')[1];
            String dia = socio.fecha.Split('-')[2];



            var query = _uow.SocioRepository.GetSocioByUsername(socio.usersugerido);
            var socioNuevo = new Socio
            {

                Nombres = socio.nombres,
                Apellidos = socio.apellidos,
                CarneEscolar = socio.estudiante,
                CarneAnciano = socio.jubilacion,
                CodCliente = socio.username,
                Contraseña = socio.contrasena,
                EstadoSocioId = 2,
                TipoSocioId = 1,
                DiaNacimiento = dia,
                AñoNacimiento = ano,
                MesNacimiento = mes,
                NumPuntos = "10"


            };

            if (query.Count() > 0)
            {
                int userrecomendado = query.ElementAt(0).SocioId;
                var socioRecomienda = _uow.SocioRepository.GetById(userrecomendado);
                int puntos = Convert.ToInt32(socioRecomienda.NumPuntos);
                puntos = puntos + 10;
                socioRecomienda.NumPuntos = puntos.ToString();
                socioNuevo.SocioRecomendadoId = userrecomendado;



            }


            _uow.SocioRepository.Add(socioNuevo);

            _uow.Save();


            var asociadoNuevo = new Asociado
            {

                SocioId = socioNuevo.SocioId,
                CineId = socio.repeatSelect1



            };


            _uow.AsociadoRepository.Add(asociadoNuevo);

            _uow.Save();


            if (socio.file != null)
            {

                string fileName = socio.username + "-";
                string extension = Path.GetExtension(socio.file.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                String nombreImagen = fileName;
                //imagepath = "~Image/" + fileName;
                fileName = System.IO.Path.Combine(
                                   Server.MapPath("~/Content/credenciales/"), fileName);
                socio.file.SaveAs(fileName);



            }

            try
            {
                WebSecurity.CreateUserAndAccount(socio.username, socio.contrasena);

            }
            catch (MembershipCreateUserException e)
            {
                return View();
            }





            return View(socioNuevo);


        }

        public ActionResult ResumenRegistro()
        {

            return View();
        }

        //
        // POST: /Socio/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Socio socio)
        {
            if (ModelState.IsValid)
            {
                db.Socio.Add(socio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoSocioId = new SelectList(db.EstadoSocio, "EstadoSocioId", "NombreEstado", socio.EstadoSocioId);
            ViewBag.SocioRecomendadoId = new SelectList(db.Socio, "SocioId", "CodCliente", socio.SocioRecomendadoId);
            ViewBag.TipoSocioId = new SelectList(db.TipoSocio, "TipoSocioId", "NombreTipoSocio", socio.TipoSocioId);
            return View(socio);
        }

        //
        // GET: /Socio/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Socio socio = db.Socio.Find(id);
            if (socio == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoSocioId = new SelectList(db.EstadoSocio, "EstadoSocioId", "NombreEstado", socio.EstadoSocioId);
            ViewBag.SocioRecomendadoId = new SelectList(db.Socio, "SocioId", "CodCliente", socio.SocioRecomendadoId);
            ViewBag.TipoSocioId = new SelectList(db.TipoSocio, "TipoSocioId", "NombreTipoSocio", socio.TipoSocioId);
            return View(socio);
        }

        //
        // POST: /Socio/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Socio socio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(socio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoSocioId = new SelectList(db.EstadoSocio, "EstadoSocioId", "NombreEstado", socio.EstadoSocioId);
            ViewBag.SocioRecomendadoId = new SelectList(db.Socio, "SocioId", "CodCliente", socio.SocioRecomendadoId);
            ViewBag.TipoSocioId = new SelectList(db.TipoSocio, "TipoSocioId", "NombreTipoSocio", socio.TipoSocioId);
            return View(socio);
        }

        //
        // GET: /Socio/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Socio socio = db.Socio.Find(id);
            if (socio == null)
            {
                return HttpNotFound();
            }
            return View(socio);
        }
         [InitializeSimpleMembership]
        public JsonResult GetInfSocio() {

            if (WebSecurity.IsAuthenticated)
            {
                string userName = WebSecurity.CurrentUserName;
                var socio = _uow.SocioRepository.GetSocioByUsername(userName);
                return Json(socio, JsonRequestBehavior.AllowGet);

            }
            else {
                string userName = WebSecurity.CurrentUserName;
                var socio = _uow.SocioRepository.GetSocioByUsername("");
                return Json(socio, JsonRequestBehavior.AllowGet);
            
            }

            
        }

        //
        // POST: /Socio/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Socio socio = db.Socio.Find(id);
            db.Socio.Remove(socio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }


    public class NuevoSocioClass
    {




        public String nombres { get; set; }
        public String apellidos { get; set; }
        public String estudiante { get; set; }
        public String jubilacion { get; set; }
        public String contrasena { get; set; }
        public String contrasena2 { get; set; }
        public String username { get; set; }
        public String usersugerido { get; set; }
        public int repeatSelect1 { get; set; }
        public String fecha { get; set; }
        public HttpPostedFileBase file { get; set; }



    }
}