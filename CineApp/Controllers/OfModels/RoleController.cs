﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Models;
using System.Web.Security;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;



namespace AutentitacionProyect.Controllers
{
    
    public class RoleController : Controller
    {


        private AcademicLoadDBEntities db = new AcademicLoadDBEntities();
        private readonly IUnitOfWork _uow;

        //
        // GET: /User/
        public RoleController(IUnitOfWork uow)
        {
            _uow = uow;
        }


         //Get Index Roles

        [Authorize(Roles = "Administrador")] 
        public ActionResult Index () {
            return View(db.webpages_Roles.ToList());
            

         }

        //Role Create
        //Get
       // [Authorize(Roles = "Admin")]
         public ActionResult RoleCreate()
         {
             return View();
         }

        // [Authorize(Roles = "Admin")]
         [HttpPost]
         [ValidateAntiForgeryToken]
         public ActionResult RoleCreate(string RoleName)
         {
             Roles.CreateRole(Request.Form["RoleName"]);
             return RedirectToAction("Index", "Role");
         }




         /// <summary>
         /// Create a new role to the user
         /// </summary>
         /// <returns></returns>
        //[Authorize(Roles = "Admin")]
         public ActionResult RoleAddToUser()
         {
             SelectList list = new SelectList(Roles.GetAllRoles());
             ViewBag.Roles = list;

             return View();
         }

         /// <summary>
         /// Add role to the user
         /// </summary>
         /// <param name="RoleName"></param>
         /// <param name="UserName"></param>
         /// <returns></returns>
         //[Authorize(Roles = "Admin")]
         [HttpPost]
         [ValidateAntiForgeryToken]
         public ActionResult RoleAddToUser(string RoleName, string UserName)
         {

             if (Roles.IsUserInRole(UserName, RoleName))
             {
                 ViewBag.ResultMessage = "El usuario ya tiene ese rol !";
             }
             else
             {
                 Roles.AddUserToRole(UserName, RoleName);
                 ViewBag.ResultMessage = "Se le ha agregado el nuevo rol !";
             }

             SelectList list = new SelectList(Roles.GetAllRoles());
             ViewBag.Roles = list;
             return View();
         }

         /// <summary>
         /// Get all the roles for a particular user
         /// </summary>
         /// <param name="UserName"></param>
         /// <returns></returns>
         [Authorize(Roles = "Admin")]
         [HttpPost]
         [ValidateAntiForgeryToken]
         public ActionResult GetRoles(string UserName)
         {
             if (!string.IsNullOrWhiteSpace(UserName))
             {
                 ViewBag.RolesForThisUser = Roles.GetRolesForUser(UserName);
                 SelectList list = new SelectList(Roles.GetAllRoles());
                 ViewBag.Roles = list;
             }
             return View("RoleAddToUser");
         }


        ////
        //// GET: /User/

        //public ActionResult Index()
        //{
        //    return View();
        //    // return View(db.UserProfiles.ToList());
        //}

        ////
        //// GET: /User/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    UserProfile userprofile = db.UserProfiles.Find(id);
        //    if (userprofile == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(userprofile);
        //}

        ////
        //// GET: /User/Create

        //public ActionResult Create()
        //{
        //    return View();
        //}

        ////
        //// POST: /User/Create

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(UserProfile userprofile)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.UserProfiles.Add(userprofile);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(userprofile);
        //}

        ////
        //// GET: /User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            
            var roleEdit = _uow.webpages_RolesRepository.GetById(id);
            
            if (roleEdit== null)
            {
                return HttpNotFound();
            }
            return View(roleEdit);
        }

        ////
        //// POST: /User/Edit/5

        [HttpPost]
       
        public ActionResult Edit(webpages_Roles role)
        {
            if (ModelState.IsValid)
            {
                db.Entry(role).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(role);
        }

        ////
        //// GET: /User/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var roleDelete = _uow.webpages_RolesRepository.GetById(id);
            if (roleDelete == null)
            {
                return HttpNotFound();
            }

            _uow.webpages_RolesRepository.Delete(roleDelete);
            _uow.Save();
            return RedirectToAction("Index");
        }

        ////
        //// POST: /User/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    UserProfile userprofile = db.UserProfiles.Find(id);
        //    db.UserProfiles.Remove(userprofile);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}

       

    }
}