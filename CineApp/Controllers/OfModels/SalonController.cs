﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Elecciones.Core.Model;
using Elecciones.Core.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using AutentitacionProyect.Repositories;
using AutentitacionProyect.Persistence;


namespace Elecciones.Controllers
{
    public class SalonController : Controller
    {
        private readonly IUnitOfWork _uow;
        public SalonController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Salon
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetEdificios()
        {
            var data = _uow.EdificioRepository.GetAll().Select(x => new
            {
                x.EdificioId,
                x.NombreEdificio
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending));
            }

            var data = _uow.SalonRepository.GetSalonGridData().ToDataSourceResult(request);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, SalonGridItem item)
        {
            var indexExist = _uow.SalonRepository.FindBy(x => x.NombreSalon == item.NombreSalon && x.EdificioId == item.CodEdificio).Any();
            if (indexExist)
            {
                var errorMessage = "El Salon: " + item.NombreSalon + " ya existe en el edificio: " + item.NombreEdificio;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var salonNuevo = new Salon
            {
                NombreSalon = item.NombreSalon,
                Capacidad = item.Capacidad,
                //Nota = item.Nota,
                EdificioId = item.CodEdificio,
            };

            _uow.SalonRepository.Add(salonNuevo);

            _uow.Save();

            item.SalonId = salonNuevo.SalonId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, SalonGridItem item)
        {
            var indexExist = _uow.SalonRepository.FindBy(x => x.NombreSalon == item.NombreSalon && x.EdificioId == item.CodEdificio && x.SalonId != item.SalonId).Any();
            if (indexExist)
            {
                var errorMessage = "El Salon: " + item.NombreSalon + " ya existe en el edificio: " + item.NombreEdificio;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var salonAEditar = _uow.SalonRepository.GetById(item.SalonId);
            salonAEditar.NombreSalon = item.NombreSalon;
            salonAEditar.Capacidad = item.Capacidad;
            //salonAEditar.Nota = item.Nota;
            salonAEditar.EdificioId = item.CodEdificio;

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, SalonGridItem item)
        {


            var salonAEditar = _uow.SalonRepository.GetById(item.SalonId);

            _uow.SalonRepository.Delete(salonAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        //public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        //{

        //    var data = _uow.SalonRepository.GetSalonGridData();

        //    if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
        //    if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending)); }

        //    var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

        //    var export = new XlsExportCollections
        //    {
        //        Title = "Salones",
        //        RightFooter = XlsHeaderFooter.PageNumber,
        //        PaperSize = XlsEnums.PaperSize.Legal,
        //        PageOrientation = XlsEnums.PageOrientation.Landscape,
        //        RepeatHeader = true,
        //        DecamelizeColumnNames = true
        //    };

        //    export.HiddenColumnsName.Add("CodEdificio");

        //    export.PrepareWorkbook(filteredData);
        //    export.DumpWorkbook("Salones", HttpContext.ApplicationInstance.Response);

        //    return null;
        //}

    }
}