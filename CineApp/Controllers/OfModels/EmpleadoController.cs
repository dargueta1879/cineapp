﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;
using WebMatrix.WebData;
using System.Web.Security;
using CineApp.Filters;

namespace CineApp.Controllers.OfModels
{
    public class EmpleadoController : Controller
    {
        private DBCineEntities db = new DBCineEntities();
        private readonly IUnitOfWork _uow;
        public EmpleadoController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        //
        // GET: /Empleado/

        public ActionResult Index()
        {
            var empleado = db.Empleado.Include(e => e.EstadoEmpleado);
            return View(empleado.ToList());
        }

        public JsonResult ChangeState(EmpleadoGridItem empleado)
        {
            var data = "";

            var empleadoUpdate = _uow.EmpleadoRepository.GetById(empleado.EmpleadoId);
            if (empleado.isActived == true)
            {
                empleadoUpdate.EstadoEmpleadoId = 1;
                data = "Empleado Activado";

            }
            else
            {
                empleadoUpdate.EstadoEmpleadoId = 2;
                data = "Empleado Desactivado";

            }

            _uow.Save();



            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllEmpleados()
        {
            var data = _uow.EmpleadoRepository.GetEmpleadoGridData();

           



            return Json(data, JsonRequestBehavior.AllowGet);
        }



        

        public ActionResult Registro()
        {



            return View();
        }
        [InitializeSimpleMembership]
        [HttpPost]
        public ActionResult Registro(EmpleadoRegisterItem item)
        {

         

            if(item.TipoEmpleadoId==0||item.CineId==0){

                View(item);


            }
            else
            {

                //Primero se creará el usuario
                try
                {
                    WebSecurity.CreateUserAndAccount(item.Correo, item.Contrasena);

                }
                catch (MembershipCreateUserException e)
                {
                    return View();
                }

                //Ahora se creará el Empleado
                var empleadoNuevo = new Empleado
                {
                    Nombres = item.Nombres,
                    Apellidos = item.Apellidos,
                    DPI = item.Dpi,

                    password = item.Contrasena,
                    Username = item.Correo,
                    TipoEmpleadoId = item.TipoEmpleadoId,
                    EstadoEmpleadoId=1



                };

                _uow.EmpleadoRepository.Add(empleadoNuevo);
                _uow.Save();

                var planillaNuevo = new Planilla
                {
                    CineId = item.CineId,
                    EmpleadoId = empleadoNuevo.EmpleadoId



                };

                _uow.PlanillaRepository.Add(planillaNuevo);
                _uow.Save();

                return RedirectToAction("Index");

            }





            return View();
        }

        public JsonResult GetAllTipoEmpleado()
        {
            var te = _uow.TipoEmpleadoRepository.GetAll().Select(x => new { 
            
            x.TipoEmpleadoId,
            x.NombreTipoEmpleado
            
            
            });


            return Json(te, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Empleado/Details/5

        public ActionResult Details(int id = 0)
        {
            Empleado empleado = db.Empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        //
        // GET: /Empleado/Create

        public ActionResult Create()
        {
            ViewBag.EstadoEmpleadoId = new SelectList(db.EstadoEmpleado, "EstadoEmpleadoId", "NombreEstadoEmpleado");
            return View();
        }

        //
        // POST: /Empleado/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                db.Empleado.Add(empleado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoEmpleadoId = new SelectList(db.EstadoEmpleado, "EstadoEmpleadoId", "NombreEstadoEmpleado", empleado.EstadoEmpleadoId);
            return View(empleado);
        }

        //
        // GET: /Empleado/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Empleado empleado = db.Empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoEmpleadoId = new SelectList(db.EstadoEmpleado, "EstadoEmpleadoId", "NombreEstadoEmpleado", empleado.EstadoEmpleadoId);
            return View(empleado);
        }

        //
        // POST: /Empleado/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empleado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoEmpleadoId = new SelectList(db.EstadoEmpleado, "EstadoEmpleadoId", "NombreEstadoEmpleado", empleado.EstadoEmpleadoId);
            return View(empleado);
        }

        //
        // GET: /Empleado/Delete/5

        
        public string Delete(EmpleadoGridItem item)
        {
            var empleado = _uow.EmpleadoRepository.GetById(item.EmpleadoId);

            empleado.EstadoEmpleadoId = 3;
            _uow.Save();
            
            return "Empleado eliminado";
        }

        //
        // POST: /Empleado/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Empleado empleado = db.Empleado.Find(id);
        //    db.Empleado.Remove(empleado);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }

    public class EmpleadoRegisterItem
    {

        public int EmpleadoId { get; set; }
        public int CineId { get; set; }
        public int TipoEmpleadoId { get; set; }
       

        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public string Contrasena { get; set; }
        public string Dpi { get; set; }
        
    }
}