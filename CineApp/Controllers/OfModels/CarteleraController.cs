﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;

namespace CineApp.Controllers.OfModels
{
    public class CarteleraController : Controller
    {
        private DBCineEntities db = new DBCineEntities();
        private readonly IUnitOfWork _uow;
        public CarteleraController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        //
        // GET: /Cartelera/

        public ActionResult Index()
        {
           
            return View();
        }

        //
        // GET: /Cartelera/Details/5


        public JsonResult GetAllCartelera(){



            var data = _uow.CarteleraRepository.GetCarteleraGridData();


            return Json(data, JsonRequestBehavior.AllowGet);
        
        }

        public JsonResult GetAllEstadoCartelera()
        {



            var data = _uow.EstadoCarteleraRepository.GetAll().Select(x=> new{
            
            x.EstadoCarteleraId,
            x.NombreEstadoCartelera
            
            });


            return Json(data, JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetAllHorarios()
        {



            var data = _uow.HorarioRepository.GetAll().Select(x => new
            {

                x.HorarioId,
                x.Dia,
                x.HoraInicio,
                x.HoraFin

            }).OrderBy(x => x.Dia);


            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public string Insert(Cartelera item)
        {
            if (item.HorarioId!=0 && item.PeliculaId!=0 && item.HorarioId!= 0 && item.EstadoCarteleraId!=0)
            {

                var carteleraNuevo = new Cartelera
                {

                   
                    PeliculaId = item.PeliculaId,
                    EstadoCarteleraId = item.EstadoCarteleraId,
                    HorarioId = item.HorarioId,
                    SalaId = item.SalaId

                };

                _uow.CarteleraRepository.Add(carteleraNuevo);

                _uow.Save();

                //item.CursoId = cursoNuevo.CursoId;

                return "Cartelera agregada exitosamente!";

            }
            else
            {
                return "No se ha podido agregar a cartelera debido a que faltan campos";
            }
        }


        public ActionResult Details(int id = 0)
        {
            Cartelera cartelera = db.Cartelera.Find(id);
            if (cartelera == null)
            {
                return HttpNotFound();
            }
            return View(cartelera);
        }

        //
        // GET: /Cartelera/Create

        public ActionResult Create()
        {
            ViewBag.EstadoCarteleraId = new SelectList(db.EstadoCartelera, "EstadoCarteleraId", "NombreEstadoCartelera");
            ViewBag.HorarioId = new SelectList(db.Horario, "HorarioId", "Dia");
            ViewBag.PeliculaId = new SelectList(db.Pelicula, "PeliculaId", "NombrePelicula");
            ViewBag.SalaId = new SelectList(db.Sala, "SalaId", "NombreSala");
            return View();
        }

        //
        // POST: /Cartelera/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cartelera cartelera)
        {
            if (ModelState.IsValid)
            {
                db.Cartelera.Add(cartelera);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoCarteleraId = new SelectList(db.EstadoCartelera, "EstadoCarteleraId", "NombreEstadoCartelera", cartelera.EstadoCarteleraId);
            ViewBag.HorarioId = new SelectList(db.Horario, "HorarioId", "Dia", cartelera.HorarioId);
            ViewBag.PeliculaId = new SelectList(db.Pelicula, "PeliculaId", "NombrePelicula", cartelera.PeliculaId);
            ViewBag.SalaId = new SelectList(db.Sala, "SalaId", "NombreSala", cartelera.SalaId);
            return View(cartelera);
        }

        //
        // GET: /Cartelera/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Cartelera cartelera = db.Cartelera.Find(id);
            if (cartelera == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoCarteleraId = new SelectList(db.EstadoCartelera, "EstadoCarteleraId", "NombreEstadoCartelera", cartelera.EstadoCarteleraId);
            ViewBag.HorarioId = new SelectList(db.Horario, "HorarioId", "Dia", cartelera.HorarioId);
            ViewBag.PeliculaId = new SelectList(db.Pelicula, "PeliculaId", "NombrePelicula", cartelera.PeliculaId);
            ViewBag.SalaId = new SelectList(db.Sala, "SalaId", "NombreSala", cartelera.SalaId);
            return View(cartelera);
        }

        //
        // POST: /Cartelera/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cartelera cartelera)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cartelera).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoCarteleraId = new SelectList(db.EstadoCartelera, "EstadoCarteleraId", "NombreEstadoCartelera", cartelera.EstadoCarteleraId);
            ViewBag.HorarioId = new SelectList(db.Horario, "HorarioId", "Dia", cartelera.HorarioId);
            ViewBag.PeliculaId = new SelectList(db.Pelicula, "PeliculaId", "NombrePelicula", cartelera.PeliculaId);
            ViewBag.SalaId = new SelectList(db.Sala, "SalaId", "NombreSala", cartelera.SalaId);
            return View(cartelera);
        }

        //
        // GET: /Cartelera/Delete/5

        public string Delete(Cartelera item)
        {
            if (item != null)
            {

                var carteleraUpdate = _uow.CarteleraRepository.GetById(item.CarteleraId);
                carteleraUpdate.EstadoCarteleraId = 2;

                _uow.Save();
                return "Cartelera eliminada exitosamente";

            }
            else
            {
                return "No se pudo eliminar cartelera";
            }
        }

        //
        // POST: /Cartelera/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Cartelera cartelera = db.Cartelera.Find(id);
        //    db.Cartelera.Remove(cartelera);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}