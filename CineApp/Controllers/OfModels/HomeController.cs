﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Models;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
using WebMatrix.WebData;
using AutentitacionProyect.Filters;

namespace AutentitacionProyect.Controllers
{
   
    [InitializeSimpleMembership] 
    public class HomeController : Controller
    {

          private readonly IUnitOfWork _uow;

        // GET: /User/
        public HomeController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Página Principal";
            ViewBag.Message = "Cargas Académicas v.1";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {


            
            Autentication permiso = new Autentication(_uow);
            if (permiso.isUserInRole("Home","Contact")){

                ViewBag.Message = "Your contact page.";

                return View();

            }



            
        



            return RedirectToAction("Login", "Account");
        }
    }
}
