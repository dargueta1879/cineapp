﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace AutentitacionProyect.Controllers
{
    public class FacultadController : Controller
    {
        private readonly IUnitOfWork _uow;
        public FacultadController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Facultad
        public ActionResult Index()
        {
            return View();
        }
      
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var data = _uow.FacultadRepository.GetAll().Select(x => new
            {
                x.FacultadId,
                x.NombreFacultad,
                x.Partida
            }).ToDataSourceResult(request);  
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, FacultadGridItem item)
        {
            var indexExist = _uow.FacultadRepository.FindBy(x => x.NombreFacultad == item.NombreFacultad).Any();
            if (indexExist)
            {
                var errorMessage = "La Facultad: " + item.NombreFacultad + " ya existe en la universidad: " + item.NombreFacultad;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var facultadNuevo = new Facultad
            {
                NombreFacultad = item.NombreFacultad,
                Partida = item.Partida
               
            };

            _uow.FacultadRepository.Add(facultadNuevo);

            _uow.Save();

            item.FacultadId =  facultadNuevo.FacultadId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, FacultadGridItem item)
        {
            var indexExist = _uow.FacultadRepository.FindBy(x => x.NombreFacultad == item.NombreFacultad && x.Partida == item.Partida && x.FacultadId == item.FacultadId).Any();
            if (indexExist)
            {
                var errorMessage = "La Facultad: " + item.NombreFacultad + " ya existe dentro del sistema, por favor verificar nuevamente la información que está editando";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var facultadAEditar = _uow.FacultadRepository.GetById(item.FacultadId);
            facultadAEditar.NombreFacultad = item.NombreFacultad;
            facultadAEditar.Partida = item.Partida;
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, FacultadGridItem item)
        {


            var facultadAEditar = _uow.FacultadRepository.GetById(item.FacultadId);

            _uow.FacultadRepository.Delete(facultadAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        //public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        //{

            //var data = _uow.FacultadRepository.GetFacultadGridData();

            //if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
            //if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreFacultad", ListSortDirection.Ascending)); }

            //var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

            //var export = new XlsExportCollections
            //{
            //    Title = "Facultades",
            //    RightFooter = XlsHeaderFooter.PageNumber,
            //    PaperSize = XlsEnums.PaperSize.Legal,
            //    PageOrientation = XlsEnums.PageOrientation.Landscape,
            //    RepeatHeader = true,
            //    DecamelizeColumnNames = true
            //};

            //export.HiddenColumnsName.Add("CodFacultad");

            //export.PrepareWorkbook(filteredData);
            //export.DumpWorkbook("Facultades", HttpContext.ApplicationInstance.Response);

        //    return null;
        //}

    }
}