﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using AutentitacionProyect.Models;
using AutentitacionProyect.Helper;

namespace AutentitacionProyect.Controllers
{
    public class CarreraController : Controller
    {
        private readonly IUnitOfWork _uow;
        public CarreraController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Salon
        
        public ActionResult Index()
        {
            return View();
        }



        
        public ActionResult GetFacultades()
        {
            var data = _uow.FacultadRepository.GetAll().Select(x => new
            {
                x.FacultadId,
                x.NombreFacultad
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request, decimal FacultadId)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("NombreCarrera", ListSortDirection.Ascending));
            }

            var data = _uow.CarreraRepository.GetAll().Select(x => new { x.CarreraId,x.NombreCarrera,x.FacultadId, x.TituloProfesion }).Where(x=> x.FacultadId==FacultadId).ToDataSourceResult(request);
            return Json(data, JsonRequestBehavior.AllowGet);

            //var username = User.Identity.Name;
            //var facultad = RoleHelper.GetUserPermission(username);
            //var fac = _uow.FacultadRepository.FindOneBy(x => x.NombreFacultad.ToUpper() == facultad);
            //var data = _uow.CarreraRepository.GetCarreras(fac.FacultadId).ToDataSourceResult(request);
            //return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, CarreraGridItem item)
        {
            var indexExist = _uow.CarreraRepository.FindBy(x => x.NombreCarrera == item.NombreCarrera && x.FacultadId == item.FacultadId).Any();
            if (indexExist)
            {
                var errorMessage = "La Carrera: " + item.NombreCarrera + " ya existe en la facultad: " + item.NombreFacultad;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }


            //var username = User.Identity.Name;
            ////var facultad = RoleHelper.GetUserPermission(username);
            //var fac = _uow.FacultadRepository.FindOneBy(x => x.NombreFacultad.ToUpper() == facultad);

            var carreraNuevo = new Carrera
            {
                NombreCarrera = item.NombreCarrera,
                FacultadId = item.FacultadId,
                TituloProfesion=item.TituloProfesion
                //FacultadId = fac.FacultadId,
            };

            _uow.CarreraRepository.Add(carreraNuevo);

            _uow.Save();

            item.CarreraId = carreraNuevo.CarreraId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, CarreraGridItem item)
        {
            var indexExist = _uow.CarreraRepository.FindBy(x => x.NombreCarrera == item.NombreCarrera && x.TituloProfesion==item.TituloProfesion).Any();
            if (indexExist)
            {
                var errorMessage = "La Carrera: " + item.NombreCarrera + " ya existe en la facultad: " + item.NombreFacultad;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            //var username = User.Identity.Name;
            ////var facultad = RoleHelper.GetUserPermission(username);
            //var fac = _uow.FacultadRepository.FindOneBy(x => x.NombreFacultad.ToUpper() == facultad);

            var carreraAEditar = _uow.CarreraRepository.GetById(item.CarreraId);
            carreraAEditar.NombreCarrera = item.NombreCarrera;
            carreraAEditar.FacultadId = item.FacultadId;
            carreraAEditar.TituloProfesion = item.TituloProfesion;
            //carreraAEditar.FacultadId = fac.FacultadId;

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, CarreraGridItem item)
        {


            var carreraAEditar = _uow.CarreraRepository.GetById(item.CarreraId);

            _uow.CarreraRepository.Delete(carreraAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }





        public ActionResult setCarreraId(int CarreraId)
        {
            sessiones.setSession("CarreraId", CarreraId.ToString());
            return null;
        }

        public ActionResult setPensumId(int PensumId)
        {
            sessiones.setSession("PensumId", PensumId.ToString());
            return null;
        }

        public ActionResult IndexGestion()
        {
            return View();
        }
    }
}