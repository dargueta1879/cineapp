﻿using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CineApp.Controllers.OfModels
{
    public class CancelarEntradaController : Controller
    {
        private DBCineEntities db = new DBCineEntities();
        private readonly IUnitOfWork _uow;
        public CancelarEntradaController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        //
        // GET: /CancelarEntrada/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Ticket item)
        {
            var ticket = _uow.TicketRepository.GetById(item.TicketId);
            DateTime fecha = DateTime.Now;
            int añoActual = Convert.ToInt32( fecha.Year.ToString());
            int mesActual = Convert.ToInt32( fecha.Month.ToString());
            int diaActual = Convert.ToInt32( fecha.Day.ToString());

            int añoTicket=100;
            int mesTicket=100;
            int diaTicket=100;

           if (ticket!=null)
	            {
                   añoTicket=Convert.ToInt32(ticket.Ano);
                   mesTicket=Convert.ToInt32(ticket.Mes);
                   diaTicket=Convert.ToInt32(ticket.Dia);

		 
	            }
           else
           {
               ViewBag.Mensaje = "No se ha encontrado su reservación.";
               return View();
           }


            //Si cumple la siguiente condición se podrá cancelar la reservacion
            if(añoActual <= añoTicket && mesActual<=mesTicket && diaActual<diaTicket){

                //Si la siguiente condición se cumple se cancelará la reservacion, de otra forma ya está cancelada
                if (ticket.EstadoTicketId==1)
                {
                    ticket.EstadoTicketId = 2;
                    _uow.Save();
                    ViewBag.Mensaje = "Su reservación se ha cancelado con éxito.";


                }
                else
                {
                    ViewBag.Mensaje = "Su reservación ya había sido cancelada.";
                }
               

                return View();
            
            
            }
            else
            {
                ViewBag.Mensaje = "Lo sentimos, ya ha pasado el tiempo para cancelar su reservación.";
                return View();
            }


           
        }

    }
}
