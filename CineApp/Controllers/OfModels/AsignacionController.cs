﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
using Elecciones.Core.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace AutentitacionProyect.Controllers
{
    //[AuthorizeRoles(Role.Administrator)]
    public class AsignacionController : Controller
    {
        private readonly IUnitOfWork _uow;
        public AsignacionController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Asignacion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCarreras()
        {



            var data = _uow.CarreraRepository.GetAll().Select(x => new {x.CarreraId,x.NombreCarrera,x.FacultadId }).OrderBy(x => x.NombreCarrera);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCiclos()
        {

            var data = new List<CicloItem>();
            data.Add(new CicloItem
            {
                CicloId = 1,
                NombreCiclo = "Primer semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 2,
                NombreCiclo = "Segundo semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 3,
                NombreCiclo = "Tercer semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 4,
                NombreCiclo = "Cuarto semestre"
            });

            data.Add(new CicloItem
            {
                CicloId = 5,
                NombreCiclo = "Quinto semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 6,
                NombreCiclo = "Sexto semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 7,
                NombreCiclo = "Septimo semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 8,
                NombreCiclo = "Octavo semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 9,
                NombreCiclo = "Novena semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 10,
                NombreCiclo = "Decima semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 11,
                NombreCiclo = "Onceavo semestre"
            });
            data.Add(new CicloItem
            {
                CicloId = 12,
                NombreCiclo = "Deceavo semestre"
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetCursos()
        {
            //var username = User.Identity.Name;
            //var facultad = RoleHelper.GetUserPermission(username);
            //var fac = _uow.FacultadRepository.FindOneBy(x => x.NombreFacultad.ToUpper() == facultad);
            //var cursos = _uow.CursoRepository.GetAll().Where(x => x.CodFacultad == fac.CodFacultad).Select(x => new { CursoId = x.CodCurso, x.NombreCurso });
            var cursos = _uow.CursoRepository.GetAll().Select(x => new { CursoId = x.CodCurso, x.NombreCurso });
            return Json(cursos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request, int carreraId, int cicloId)
        {
            var data = _uow.AsignacionRepository.GetAsignacionGridData(carreraId, cicloId);

            if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }

            if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreCurso", ListSortDirection.Ascending)); }

            var query = data.ToDataSourceResult(request);

            return Json(query, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest]DataSourceRequest request,AsignacionGridItem item)
        {
            var indexExist = _uow.DetallePensumRepository.FindBy(x => x.PensumId == item.PensumId && x.Ciclo == item.CicloId && x.CursoId == item.CursoId && x.DetallePensumId != item.DetallePensumId).Any();
            if (indexExist)
            {
                var errorMessage = "El curso: " + item.NombreCurso+ " ya existe en el pensum seleccionado";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var detallePensumAEditar = _uow.DetallePensumRepository.GetById(item.DetallePensumId);
            detallePensumAEditar.Ciclo = item.CicloId;
            detallePensumAEditar.Estado = item.Estado;
            detallePensumAEditar.Obligatorio = item.Obligatorio;
            detallePensumAEditar.NoCreditosPrerrequisito = item.NoCreditosPrerrequisito;
            detallePensumAEditar.PensumId = item.PensumId;
            detallePensumAEditar.CursoId= item.CursoId;

            _uow.Save();


            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Create([DataSourceRequest]DataSourceRequest request, AsignacionGridItem item)
        {
            var indexExist = _uow.DetallePensumRepository.FindBy(x => x.Ciclo == item.CicloId && x.PensumId == item.PensumId && x.CursoId == item.CursoId).Any();
            if (indexExist)
            {
                var errorMessage = "El Curso: " + item.NombreCurso+ " ya existe en el pensum seleccionado";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var detallePensumNuevo = new DetallePensum
            {
                Ciclo = item.CicloId,
                Estado = item.Estado,
                Obligatorio = item.Obligatorio,
                NoCreditosPrerrequisito = item.NoCreditosPrerrequisito,
                PensumId = item.PensumId,
                CursoId =  item.CursoId,
            };

            _uow.DetallePensumRepository.Add(detallePensumNuevo);

            _uow.Save();

            item.DetallePensumId = detallePensumNuevo.DetallePensumId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        //public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, AsignacionGridItem item)
        //{
        //    var detalleAEditar = _uow.DetallePensumRepository.GetById(item.DetallePensumId);

        //    _uow.DetallePensumRepository.Delete(detalleAEditar);
        //    _uow.Save();

        //    var result = new[] { item };
        //    return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        //}

        //public EmptyResult ExportGridToExcel([DataSourceRequest]DataSourceRequest request, int carreraId, int cicloId)
        //{

        //    var data = _uow.CargaCursoContratacionRepository.GetAsignacionGridData(carreraId, cicloId);

        //    if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
        //    if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending)); }

        //    var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

        //    var export = new XlsExportCollections
        //    {
        //        Title = "Cursos",
        //        RightFooter = XlsHeaderFooter.PageNumber,
        //        PaperSize = XlsEnums.PaperSize.Legal,
        //        PageOrientation = XlsEnums.PageOrientation.Landscape,
        //        RepeatHeader = true,
        //        DecamelizeColumnNames = true
        //    };


        //    export.PrepareWorkbook(filteredData);
        //    export.DumpWorkbook("Cursos", HttpContext.ApplicationInstance.Response);

        //    return null;
        //}

    }

    public class CicloItem
    {
        public int CicloId { get; set; }
        public string NombreCiclo { get; set; }
    }
}