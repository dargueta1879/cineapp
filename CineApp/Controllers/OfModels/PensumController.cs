﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using AutentitacionProyect.Models;
//using Elecciones.ModelsQuemados;

namespace AutentitacionProyect.Controllers
{
    public class PensumController : Controller
    {
        private readonly IUnitOfWork _uow;
        public PensumController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Pensum
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetCarrera()
        {
            var data = _uow.CarreraRepository.GetAll().Select(x => new
            {
                x.CarreraId,
                x.NombreCarrera
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("", ListSortDirection.Ascending));
            }

            int CarreraId = sessiones.getSession("CarreraId");
            //var data = _uow.PensumRepository.GetPensumGrid().ToDataSourceResult(request);
            var data = _uow.PensumRepository.GetAll().Select(x => new { x.FechaVigencia, x.FechaFinVigencia, x.CarreraId, x.Observaciones, x.PensumId}).Where(x => x.CarreraId==CarreraId).ToDataSourceResult(request);
            

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Create([DataSourceRequest]DataSourceRequest request, PensumGridItem item)
        //public JsonResult Create([DataSourceRequest]DataSourceRequest request, QuemadoPensum quemado)
        {
            //System.Diagnostics.Debug.WriteLine(quemado.FechaVigencia);
            //Pensum item = new Pensum();
            //item.CarreraId = quemado.CarreraId;
            //item.FechaVigencia = System.DateTime.Parse( quemado.FechaVigencia.ToString());




            System.Diagnostics.Debug.WriteLine(item.FechaVigencia.ToString());
        
            var indexExist = _uow.PensumRepository.FindBy(x => x.FechaVigencia == item.FechaVigencia && x.CarreraId==item.CarreraId).Any();
            if (indexExist)
            {
                var errorMessage = "El Pensum: " + item.FechaVigencia + " de la carrera " + item.CarreraId+" ya existe.";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var pensumNuevo = new Pensum
            {
                FechaVigencia = item.FechaVigencia,
                FechaFinVigencia = item.FechaFinVigencia,
                CarreraId= item.CarreraId,
                Observaciones= item.Observaciones,
                
            };

            _uow.PensumRepository.Add(pensumNuevo);

            _uow.Save();

            item.PensumId = pensumNuevo.PensumId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, PensumGridItem item)
        {
            var indexExist = _uow.PensumRepository.FindBy(x => x.PensumId == item.PensumId && x.FechaVigencia == item.FechaVigencia && x.FechaFinVigencia == item.FechaFinVigencia && x.CarreraId == item.CarreraId && x.Observaciones == item.Observaciones).Any();
            if (indexExist)
            {
                var errorMessage = "El Pensum: " + item.PensumId + " ya existe en el sistema";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var pensumAEditar = _uow.PensumRepository.GetById(item.PensumId);
            pensumAEditar.FechaVigencia = item.FechaVigencia;
            pensumAEditar.FechaFinVigencia = item.FechaFinVigencia;
            pensumAEditar.CarreraId = item.CarreraId;
            pensumAEditar.Observaciones = item.Observaciones;

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, PensumGridItem item)
        {
           

            var pensumAEditar = _uow.PensumRepository.GetById(item.PensumId);

            _uow.PensumRepository.Delete(pensumAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        //public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        //{

        //    var data = _uow.PensumRepository.GetPensumGrid();

        //    if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
        //    if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("CodPensum", ListSortDirection.Ascending)); }

        //    var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

        //    var export = new XlsExportCollections
        //    {
        //        Title = "Pensums",
        //        RightFooter = XlsHeaderFooter.PageNumber,
        //        PaperSize = XlsEnums.PaperSize.Legal,
        //        PageOrientation = XlsEnums.PageOrientation.Landscape,
        //        RepeatHeader = true,
        //        DecamelizeColumnNames = true
        //    };

        //    export.HiddenColumnsName.Add("CodPensum");

        //    export.PrepareWorkbook(filteredData);
        //    export.DumpWorkbook("Pensums", HttpContext.ApplicationInstance.Response);

        //    return null;
        //}

        public ActionResult setPensumId(int PensumId)
        {
            sessiones.setSession("PensumId", PensumId.ToString());
            return null;
        }

        public ActionResult IndexGestion()
        {
            return View();
        }
    }
}