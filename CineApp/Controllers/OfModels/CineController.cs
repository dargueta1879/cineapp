﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;

namespace CineApp.Controllers.OfModels
{
    public class CineController : Controller
    {
        private DBCineEntities db = new DBCineEntities();
        private readonly IUnitOfWork _uow;
        public CineController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        //
        // GET: /Pelicula/
        //
        // GET: /Cine/



        public JsonResult GetAllCines() {


            var data = _uow.CineRepository.GetAll().Select(x => new
            {
                x.CineId,
                x.NombreCine,
                x.Direccion
            });


            return Json(data, JsonRequestBehavior.AllowGet);
           
        }

        public JsonResult GetAllSalasByCineId(int id)
        {


            var data = _uow.SalaRepository.GetAll().Select(x => new
            {
                x.CineId,
                x.NombreSala,
                x.SalaId
            }).Where(x => x.CineId == id);


            return Json(data, JsonRequestBehavior.AllowGet);

        }


        public ActionResult Index()
        {
            return View(db.Cine.ToList());
        }

        //
        // GET: /Cine/Details/5

        public ActionResult Details(int id = 0)
        {
            Cine cine = db.Cine.Find(id);
            if (cine == null)
            {
                return HttpNotFound();
            }
            return View(cine);
        }

        //
        // GET: /Cine/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Cine/Create

        [HttpPost]
        public ActionResult Create(Cine cine)
        {
            if (ModelState.IsValid)
            {
                db.Cine.Add(cine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cine);
        }

        //
        // GET: /Cine/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Cine cine = db.Cine.Find(id);
            if (cine == null)
            {
                return HttpNotFound();
            }
            return View(cine);
        }

        //
        // POST: /Cine/Edit/5

        [HttpPost]
        public ActionResult Edit(Cine cine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cine);
        }

        //
        // GET: /Cine/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Cine cine = db.Cine.Find(id);
            if (cine == null)
            {
                return HttpNotFound();
            }
            return View(cine);
        }

        //
        // POST: /Cine/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Cine cine = db.Cine.Find(id);
            db.Cine.Remove(cine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}