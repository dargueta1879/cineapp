﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Elecciones.Core.Common;
using Elecciones.Core.Helper;
using Elecciones.Core.Model;
using Elecciones.Core.Repositories;
using Elecciones.Helpers;

using Elecciones.Models;

namespace Elecciones.Controllers
{
    public class AsignacionPuestoController: Controller
    {



         private readonly IUnitOfWork _uow;
        public AsignacionPuestoController(IUnitOfWork uow)
        {


            _uow = uow;
        }
        // GET: Salon
       
        public ActionResult GetPuesto()
        {

            var data = _uow.PuestoRepository.GetAll().Select(x => new
            {
                x.CodPuesto,
                x.NombreTipoPuesto
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFacultades()
        {
            var data = _uow.FacultadRepository.GetAll().Select(x => new
            {
                x.CodFacultad,
                x.NombreFacultad
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, int UserId)
        {

            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("NombreTipoPuesto", ListSortDirection.Ascending));
            }

            var data = _uow.AsignacionPuestoRepository.GetPuestoGridData(UserId).ToDataSourceResult(request);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, AsignacionPuestoItem item)
        {
            var indexExist = _uow.AsignacionPuestoRepository.FindBy(x => x.IdUserAccounts == item.UserId && x.CodFacultad == item.CodFacultad &&  x.CodPuesto== item.CodPuesto).Any();
            if (indexExist)
            {
                var errorMessage = "El usuario ya existe en la facultad con ese puesto.";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var asignacionNuevo = new AsignacionPuesto
            {
               IdUserAccounts=item.UserId,
               CodFacultad=item.CodFacultad,
               CodPuesto=item.CodPuesto
            };

            _uow.AsignacionPuestoRepository.Add(asignacionNuevo);

            _uow.Save();

            item.CodPuesto = asignacionNuevo.CodAsignacionPuesto;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, AsignacionPuestoItem item)
        {
            var indexExist = _uow.AsignacionPuestoRepository.FindBy(x => x.IdUserAccounts == item.UserId && x.CodFacultad == item.CodFacultad && x.CodPuesto == item.CodPuesto).Any();
            if (indexExist)
            {
                var errorMessage = "El usuario ya existe en la facultad con ese puesto.";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var asignacionAEditar = _uow.AsignacionPuestoRepository.GetById(item.CodAsignacionPuesto);
            asignacionAEditar.CodPuesto = item.CodPuesto;
            asignacionAEditar.CodFacultad = item.CodFacultad;
            

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, SalonGridItem item)
        {


            var salonAEditar = _uow.SalonRepository.GetById(item.CodSalon);

            _uow.SalonRepository.Delete(salonAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        {

            var data = _uow.SalonRepository.GetSalonGridData();

            if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
            if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending)); }

            var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

            var export = new XlsExportCollections
            {
                Title = "Salones",
                RightFooter = XlsHeaderFooter.PageNumber,
                PaperSize = XlsEnums.PaperSize.Legal,
                PageOrientation = XlsEnums.PageOrientation.Landscape,
                RepeatHeader = true,
                DecamelizeColumnNames = true
            };

            export.HiddenColumnsName.Add("CodEdificio");

            export.PrepareWorkbook(filteredData);
            export.DumpWorkbook("Salones", HttpContext.ApplicationInstance.Response);

            return null;
        }

    }
}