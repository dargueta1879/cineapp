﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;

namespace CineApp.Controllers.OfModels
{
    public class AsociadoController : Controller
    {
        private DBCineEntities db = new DBCineEntities();
         private readonly IUnitOfWork _uow;
        public AsociadoController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        //
        // GET: /Asociado/

        public ActionResult Index()
        {
           
            return View();
        }



        public JsonResult GetAllData()
        {
            var data = _uow.SocioRepository.GetSociosGridData();


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeState(SocioGridItem socio)
        {
            var data = "";

            var socioUpdate = _uow.SocioRepository.GetById(socio.SocioId);
            if (socio.isActived == true)
            {
                socioUpdate.EstadoSocioId = 1;
                data = "Asociado Activado";

            }
            else {
                socioUpdate.EstadoSocioId = 2;
                data = "Asociado Desactivado";
            
            }

            _uow.Save();



            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Asociado/Details/5

        public ActionResult Details(int id = 0)
        {
            Asociado asociado = db.Asociado.Find(id);
            if (asociado == null)
            {
                return HttpNotFound();
            }
            return View(asociado);
        }

        //
        // GET: /Asociado/Create

        public ActionResult Create()
        {
            ViewBag.CineId = new SelectList(db.Cine, "CineId", "NombreCine");
            ViewBag.SocioId = new SelectList(db.Socio, "SocioId", "CodCliente");
            return View();
        }

        //
        // POST: /Asociado/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Asociado asociado)
        {
            if (ModelState.IsValid)
            {
                db.Asociado.Add(asociado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CineId = new SelectList(db.Cine, "CineId", "NombreCine", asociado.CineId);
            ViewBag.SocioId = new SelectList(db.Socio, "SocioId", "CodCliente", asociado.SocioId);
            return View(asociado);
        }

        //
        // GET: /Asociado/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Asociado asociado = db.Asociado.Find(id);
            if (asociado == null)
            {
                return HttpNotFound();
            }
            ViewBag.CineId = new SelectList(db.Cine, "CineId", "NombreCine", asociado.CineId);
            ViewBag.SocioId = new SelectList(db.Socio, "SocioId", "CodCliente", asociado.SocioId);
            return View(asociado);
        }

        //
        // POST: /Asociado/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Asociado asociado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(asociado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CineId = new SelectList(db.Cine, "CineId", "NombreCine", asociado.CineId);
            ViewBag.SocioId = new SelectList(db.Socio, "SocioId", "CodCliente", asociado.SocioId);
            return View(asociado);
        }

        //
        // GET: /Asociado/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Asociado asociado = db.Asociado.Find(id);
            if (asociado == null)
            {
                return HttpNotFound();
            }
            return View(asociado);
        }

        //
        // POST: /Asociado/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Asociado asociado = db.Asociado.Find(id);
            db.Asociado.Remove(asociado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}