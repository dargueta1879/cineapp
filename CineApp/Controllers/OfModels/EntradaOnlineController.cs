﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;

namespace CineApp.Controllers.OfModels
{
    public class EntradaOnlineController : Controller
    {
        private DBCineEntities db = new DBCineEntities();
        private readonly IUnitOfWork _uow;
        public EntradaOnlineController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        //
        // GET: /Pelicula/
        //
        // GET: /Cine/



        public JsonResult GetAllCines() {


            var data = _uow.CineRepository.GetAll().Select(x => new
            {
                x.CineId,
                x.NombreCine,
                x.Direccion
            });


            return Json(data, JsonRequestBehavior.AllowGet);
           
        }

        public JsonResult GetCarteleraByCine(int id)
        {


            var data = _uow.CarteleraRepository.GetCarteleraByCine(id);




            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAsientosDisponibles(EntradaItem item)
        {


            var asientosOcupados = _uow.TicketRepository.GetAsientosOcupados(item);
            var todosAsientos = _uow.TicketRepository.GetAllAsientosByCarteleraId(item.CarteleraId);
            IEnumerable<AsientoItem> disponibilidadAsientos=todosAsientos;
            List<AsientoItem> listaAsientos=new List<AsientoItem>();
       
            foreach (var itemAsiento in disponibilidadAsientos)
            {

                AsientoItem asiento=new AsientoItem();
                asiento.AsientoId = itemAsiento.AsientoId;
                asiento.NombreAsiento=itemAsiento.NombreAsiento;
                asiento.Disponible=itemAsiento.Disponible;
                foreach (var itemOcupado in asientosOcupados)
                {

                    if (itemAsiento.AsientoId==itemOcupado.AsientoId)
                    {
                        asiento.Disponible=false;
                    
                    }

                    
                }
                listaAsientos.Add(asiento);

            
                
            }




            return Json(listaAsientos, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetAllSalasByCineId(int id)
        {


            var data = _uow.SalaRepository.GetAll().Select(x => new
            {
                x.CineId,
                x.NombreSala,
                x.SalaId
            }).Where(x => x.CineId == id);


            return Json(data, JsonRequestBehavior.AllowGet);

        }


        public ActionResult Index()
        {
            return View(db.Cine.ToList());
        }

        //
        // GET: /Cine/Details/5

        public ActionResult Details(int id = 0)
        {
            Cine cine = db.Cine.Find(id);
            if (cine == null)
            {
                return HttpNotFound();
            }
            return View(cine);
        }

        //
        // GET: /Cine/Create

        public JsonResult HacerReservacion(EntradaItem item)
        {

            if (item.CarteleraId != 0 && item.asientosSeleccionados != null && item.Mes != "" && item.Dia!="" && item.Año != "" && item.password!=null)
            {

                //A continuación se validará la forma de pago, si es tarjeta de crédito no pasa nada, ahora si son puntos se le restarán
                if(item.FormaPago=="Puntos"){
                    var socio = _uow.SocioRepository.GetById(item.Socio.SocioId);

                    int puntos=Convert.ToInt32(socio.NumPuntos);

                    if (puntos<item.CostoPuntos)
                    {
                        return Json("No cuenta con suficientes puntos.", JsonRequestBehavior.AllowGet);

                        
                    }


                    puntos=puntos-item.CostoPuntos;
                    socio.NumPuntos = puntos.ToString();
                
                
                
                }


                var ticketNuevo = new Ticket
                {


                    TicketId=0,
                    EstadoTicketId=1,
                    Mes=item.Mes,
                    Ano=item.Año,
                    Dia=item.Dia,
                    CineId=item.CineId,
                    Contrasena=item.password

                };
                _uow.TicketRepository.Add(ticketNuevo);
                _uow.Save();

                item.TicketId = ticketNuevo.TicketId;



                foreach (var detTicket in item.asientosSeleccionados)
                {

                    var detalleNuevo = new DetalleTicket
                    {


                        TicketId = ticketNuevo.TicketId,
                        AsiendoId=detTicket.AsiendoId,
                        ProductoId=1,
                        CarteleraId=item.CarteleraId
                        

                    };
                    _uow.DetalleTicketRepository.Add(detalleNuevo);
                    _uow.Save();

                    
                }



                return Json(item, JsonRequestBehavior.AllowGet);






            }
            



            return Json("Debe rellenar todos los campos para realizar la reservación.", JsonRequestBehavior.AllowGet);

            
        }

        //
        // POST: /Cine/Create

        [HttpPost]
        public ActionResult Create(Cine cine)
        {
            if (ModelState.IsValid)
            {
                db.Cine.Add(cine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cine);
        }

        //
        // GET: /Cine/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Cine cine = db.Cine.Find(id);
            if (cine == null)
            {
                return HttpNotFound();
            }
            return View(cine);
        }

        //
        // POST: /Cine/Edit/5

        [HttpPost]
        public ActionResult Edit(Cine cine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cine);
        }

        //
        // GET: /Cine/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Cine cine = db.Cine.Find(id);
            if (cine == null)
            {
                return HttpNotFound();
            }
            return View(cine);
        }

        //
        // POST: /Cine/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Cine cine = db.Cine.Find(id);
            db.Cine.Remove(cine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        //public ActionResult ExportHorarioCargaAcademica(decimal CargaAcademicaId)
        //{

        //    ViewData["CargaAcademicaId"] = CargaAcademicaId;

        //    var horariocargaacademica = _uow.CargaAcademicaRepository.GetHorarioCargaAcademica(CargaAcademicaId);


        //    List<HorarioReport> horarios = new List<HorarioReport>();
        //    foreach (HorarioCargaAcademicaItem item in horariocargaacademica)
        //    {
        //        HorarioReport horario = new HorarioReport();
        //        horario.NombreCarrera = item.NombreCarrera;
        //        horario.NombreCurso = item.NombreCurso;
        //        horario.NombreSalon = item.NombreSalon;
        //        horario.Ciclo = item.Ciclo.ToString();
        //        horario.HoraInicio = item.HoraInicio.Value.Hours.ToString() + ":" + item.HoraInicio.Value.Minutes.ToString();
        //        horario.HoraFin = item.HoraFin.Value.Hours.ToString() + ":" + item.HoraFin.Value.Minutes.ToString();
        //        horario.Dia = item.Dia;

        //        horarios.Add(horario);



        //    }


        //    ReportDocument rd = new ReportDocument();
        //    rd.Load(Path.Combine(Server.MapPath("~/Report/HorarioCargaAcademicaReport.rpt")));

        //    rd.SetDataSource(horarios.ToList());

        //    Response.Buffer = false;
        //    Response.ClearContent();
        //    Response.ClearHeaders();


        //    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    stream.Seek(0, SeekOrigin.Begin);
        //    return File(stream, "application/pdf", "Horarios.pdf");






        //}
    }


    public class EntradaItem {

        public int CarteleraId { get; set; }
        public int PeliculaId { get; set; }
        public int EstadoCarteleraId { get; set; }
        public int HorarioId { get; set; }
        public int CineId { get; set; }
        public int SalaId { get; set; }
        public int TicketId { get; set; }
        public String FormaPago { get; set; }
        public int CostoPuntos { get; set; }
        public SocioItem Socio { get; set; }

        public string NombrePelicula { get; set; }
        public string NombreEstadoCartelera { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFin { get; set; }
        public string Dia { get; set; }
        public string Mes { get; set; }
        public string Año { get; set; }
        public string NombreSala { get; set; }
        public string NombreCine { get; set; }
        public string password { get; set; }

        public IEnumerable<Asiento> Asientos { get; set; }
        public IEnumerable<Asiento> asientosSeleccionados { get; set; }
    
    
    }
    public class SolicutudEntradaItem
    {


        public int CarteleraId { get; set; }
        public int PeliculaId { get; set; }
        public int EstadoCarteleraId { get; set; }
        public int HorarioId { get; set; }
        public int CineId { get; set; }
        public int SalaId { get; set; }
        public int TicketId { get; set; }

        public string NombrePelicula { get; set; }
        public string NombreEstadoCartelera { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFin { get; set; }
        public string Dia { get; set; }
        public string NombreSala { get; set; }
        public string NombreCine { get; set; }
        IEnumerable<Asiento> Asientos { get; set; }


    }
}