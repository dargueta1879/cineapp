﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Elecciones.Core.Model;
using Elecciones.Core.Repositories;
using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace Elecciones.Controllers
{
    public class CatedraticoController : Controller
    {

        private readonly IUnitOfWork _uow;
        public CatedraticoController(IUnitOfWork uow)
        {
            _uow = uow;
        }


        public ActionResult GetFacultades()
        {
            var data = _uow.FacultadRepository.GetAll().Select(x => new
            {
                x.CodFacultad,
                x.NombreFacultad
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Salon
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("NombreCatedratico", ListSortDirection.Ascending));
            }

            var data = _uow.CatedraticoRepository.GetCatedraticoGridData().ToDataSourceResult(request);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, CatedraticoGridItem item)
        {
            var indexExist = _uow.CatedraticoRepository.FindBy(x => x.Dpi == item.Dpi && x.CatedraticoId == item.CatedraticoId).Any();
            if (indexExist)
            {
                var errorMessage = "El Catedratico: " + item.NombreCatedratico + " ya se encuentra creado en el sistema: ";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var catedraticoNuevo = new Catedratico
            {
                NombreCatedratico = item.NombreCatedratico,
                Dpi = item.Dpi,
                CodUniversitario = item.CodUniversitario,
                Plaza = item.Plaza,
                Profesion = item.Profesion,
            };

            _uow.CatedraticoRepository.Add(catedraticoNuevo);

            _uow.Save();

            item.CatedraticoId = catedraticoNuevo.CatedraticoId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, CatedraticoGridItem item)
        {
            var indexExist = _uow.CatedraticoRepository.FindBy(x => x.NombreCatedratico == item.NombreCatedratico && x.Dpi == item.Dpi && x.CodUniversitario == item.CodUniversitario && x.Plaza == item.Plaza && x.Profesion == item.Profesion && x.CatedraticoId != item.CatedraticoId).Any();
            if (indexExist)
            {
                var errorMessage = "El Catedratico: " + item.NombreCatedratico + " ya se encuentra ingresado con los datos que se encuentra editando, por favor verificar la información";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var catedraticoAEditar = _uow.CatedraticoRepository.GetById(item.CatedraticoId);
            catedraticoAEditar.NombreCatedratico = item.NombreCatedratico;
            catedraticoAEditar.Dpi = item.Dpi;
            catedraticoAEditar.CodUniversitario = item.CodUniversitario;
            catedraticoAEditar.Plaza = item.Plaza;
            catedraticoAEditar.Profesion = item.Profesion;

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, CatedraticoGridItem item)
        {


            var catedraticoAEditar = _uow.CatedraticoRepository.GetById(item.CatedraticoId);

            _uow.CatedraticoRepository.Delete(catedraticoAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        {

            var data = _uow.CatedraticoRepository.GetCatedraticoGridData();

            if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
            if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreCatedratico", ListSortDirection.Ascending)); }

            var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

            var export = new XlsExportCollections
            {
                Title = "Catedraticos",
                RightFooter = XlsHeaderFooter.PageNumber,
                PaperSize = XlsEnums.PaperSize.Legal,
                PageOrientation = XlsEnums.PageOrientation.Landscape,
                RepeatHeader = true,
                DecamelizeColumnNames = true
            };

            export.HiddenColumnsName.Add("CodCatedratico");

            export.PrepareWorkbook(filteredData);
            export.DumpWorkbook("Catedraticos", HttpContext.ApplicationInstance.Response);

            return null;
        }
        public ActionResult ReadAsignacion([DataSourceRequest] DataSourceRequest request,int CatedraticoId)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("AsignacionCatedraticoId", ListSortDirection.Ascending));
            }

            var data = _uow.AsignacionCatedraticoFacultadRepository.GetAsignacionCatedraticoGridData(CatedraticoId).ToDataSourceResult(request);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateAsignacion([DataSourceRequest]DataSourceRequest request,AsignacionCatedraticoGridItem item)
        {
            var indexExist = _uow.AsignacionCatedraticoFacultadRepository.FindBy(x => x.CatedraticoId == item.CatedraticoId && x.Facultad.CodFacultad == item.CodFacultad).Any();
            if (indexExist)
            {
                var errorMessage = "El Catedratico"+item.NombreCatedratico+" ya existe asignado en facultad:  "+ item.NombreFacultad;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var asignacionNuevo = new AsignacionCatedraticoFacultad
            {
                FacultadId=item.CodFacultad,
                CatedraticoId=item.CatedraticoId
            };

            _uow.AsignacionCatedraticoFacultadRepository.Add(asignacionNuevo);

            _uow.Save();

            item.AsignacionCatedraticoId= asignacionNuevo.AsignacionCatedraticoFacultadId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAsignacion([DataSourceRequest]DataSourceRequest request, AsignacionCatedraticoGridItem item)
        {
            var indexExist = _uow.AsignacionCatedraticoFacultadRepository.FindBy(x => x.CatedraticoId == item.CatedraticoId && x.Facultad.CodFacultad == item.CodFacultad).Any();
            if (indexExist)
            {
                var errorMessage = "El Catedratico" + item.NombreCatedratico + " ya existe asignado en facultad:  " + item.NombreFacultad;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }


            var asignacionEditar = _uow.AsignacionCatedraticoFacultadRepository.GetById(item.AsignacionCatedraticoId);
            
          
               asignacionEditar.FacultadId = item.CodFacultad;
               asignacionEditar.CatedraticoId = item.CatedraticoId;
           

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, AsignacionCatedraticoGridItem item)
        {


            var asignacionDelete = _uow.AsignacionCatedraticoFacultadRepository.GetById(item.AsignacionCatedraticoId);

            _uow.AsignacionCatedraticoFacultadRepository.Delete(asignacionDelete);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        //public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        //{

        //    var data = _uow.SalonRepository.GetSalonGridData();

        //    if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
        //    if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending)); }

        //    var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

        //    var export = new XlsExportCollections
        //    {
        //        Title = "Salones",
        //        RightFooter = XlsHeaderFooter.PageNumber,
        //        PaperSize = XlsEnums.PaperSize.Legal,
        //        PageOrientation = XlsEnums.PageOrientation.Landscape,
        //        RepeatHeader = true,
        //        DecamelizeColumnNames = true
        //    };

        //    export.HiddenColumnsName.Add("CodEdificio");

        //    export.PrepareWorkbook(filteredData);
        //    export.DumpWorkbook("Salones", HttpContext.ApplicationInstance.Response);

        //    return null;
        //}

    }
}