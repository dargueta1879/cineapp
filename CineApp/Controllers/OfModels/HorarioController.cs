﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Elecciones.Core.Common;
using Elecciones.Core.Model;
using Elecciones.Core.Repositories;
using Elecciones.Helpers;
using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace Elecciones.Controllers
{
    public class HorarioController : Controller
    {
        private readonly IUnitOfWork _uow;
        public HorarioController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Salon
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDia()
        {

            var data = new List<DiaItem>();
            data.Add(new DiaItem
            {
                cod = 1,
                nombre = "Lunes"
            });
            data.Add(new DiaItem
            {
                cod = 2,
                nombre = "Martes"
            });
            data.Add(new DiaItem
            {
                cod = 3,
                nombre = "Miercoles"
            });
            data.Add(new DiaItem
            {
                cod = 4,
                nombre = "Jueves"
            });

            data.Add(new DiaItem
            {
                cod = 5,
                nombre = "Viernes"
            });
            data.Add(new DiaItem
            {
                cod = 6,
                nombre = "Sabado"
            });
            data.Add(new DiaItem
            {
                cod = 7,
                nombre = "Domingo"
            });
           

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSalon()
        {
            var data = _uow.SalonRepository.GetAll().Select(x => new
            {
                x.CodSalon,
                x.NombreSalon
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending));
            }

            var data = _uow.SalonRepository.GetSalonGridData().ToDataSourceResult(request);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, SalonGridItem item)
        {
            var indexExist = _uow.SalonRepository.FindBy(x => x.NombreSalon == item.NombreSalon && x.CodEdificio == item.CodEdificio).Any();
            if (indexExist)
            {
                var errorMessage = "El Salon: " + item.NombreSalon + " ya existe en el edificio: " + item.NombreEdificio;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var salonNuevo = new Salon
            {
                NombreSalon = item.NombreSalon,
                Capacidad = item.Capacidad,
                Nota = item.Nota,
                CodEdificio = item.CodEdificio,
            };

            _uow.SalonRepository.Add(salonNuevo);

            _uow.Save();

            item.CodSalon = salonNuevo.CodSalon;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, SalonGridItem item)
        {
            var indexExist = _uow.SalonRepository.FindBy(x => x.NombreSalon == item.NombreSalon && x.CodEdificio == item.CodEdificio && x.CodSalon != item.CodSalon).Any();
            if (indexExist)
            {
                var errorMessage = "El Salon: " + item.NombreSalon + " ya existe en el edificio: " + item.NombreEdificio;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var salonAEditar = _uow.SalonRepository.GetById(item.CodSalon);
            salonAEditar.NombreSalon = item.NombreSalon;
            salonAEditar.Capacidad = item.Capacidad;
            salonAEditar.Nota = item.Nota;
            salonAEditar.CodEdificio = item.CodEdificio;

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, SalonGridItem item)
        {


            var salonAEditar = _uow.SalonRepository.GetById(item.CodSalon);

            _uow.SalonRepository.Delete(salonAEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        {

            var data = _uow.SalonRepository.GetSalonGridData();

            if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
            if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreSalon", ListSortDirection.Ascending)); }

            var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

            var export = new XlsExportCollections
            {
                Title = "Salones",
                RightFooter = XlsHeaderFooter.PageNumber,
                PaperSize = XlsEnums.PaperSize.Legal,
                PageOrientation = XlsEnums.PageOrientation.Landscape,
                RepeatHeader = true,
                DecamelizeColumnNames = true
            };

            export.HiddenColumnsName.Add("CodEdificio");

            export.PrepareWorkbook(filteredData);
            export.DumpWorkbook("Salones", HttpContext.ApplicationInstance.Response);

            return null;
        }

    }

    class DiaItem {

        public int cod { get; set; }
        public string nombre { get; set; }

       
    
    
    }
}