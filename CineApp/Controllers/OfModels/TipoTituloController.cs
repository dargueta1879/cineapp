﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Elecciones.Core.Model;
using Elecciones.Core.Repositories;
using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace Elecciones.Controllers
{
    public class TipoTituloController : Controller
    {
        private readonly IUnitOfWork _uow;
        public TipoTituloController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: TipoTitulo
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var data = _uow.TipoTituloRepository.GetAll().Select(x => new
            {
                x.CodTipoTitulo,
                x.NombreTipoTitulo,
                x.Descripcion
            }).ToDataSourceResult(request);



            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, TipoTitulo item)
        {
            var indexExist = _uow.TipoTituloRepository.FindBy(x => x.NombreTipoTitulo == item.NombreTipoTitulo).Any();
            if (indexExist)
            {
                var errorMessage = "El tipo de título: " + item.NombreTipoTitulo + " ya existe en la universidad: " + item.NombreTipoTitulo;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var tipotituloNuevo = new TipoTitulo
            {
               NombreTipoTitulo = item.NombreTipoTitulo,
                CodTipoTitulo = item.CodTipoTitulo,
                Descripcion=item.Descripcion

            };

            _uow.TipoTituloRepository.Add(tipotituloNuevo);

            _uow.Save();

            item.CodTipoTitulo = tipotituloNuevo.CodTipoTitulo;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, TipoTitulo item)
        {
            var indexExist = _uow.TipoTituloRepository.FindBy(x => x.NombreTipoTitulo == item.NombreTipoTitulo).Any();
            if (indexExist)
            {
                var errorMessage = "El Tipo de Titulo: " + item.NombreTipoTitulo + " ya existe : ";
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var tipotituloEditar = _uow.TipoTituloRepository.GetById(item.CodTipoTitulo);
            tipotituloEditar.NombreTipoTitulo = item.NombreTipoTitulo;
            

            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, TipoTitulo item)
        {


            var tipotituloEditar = _uow.TipoTituloRepository.GetById(item.CodTipoTitulo);

            _uow.TipoTituloRepository.Delete(tipotituloEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        {

            //var data = _uow.FacultadRepository.GetFacultadGridData();

            //if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
            //if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreFacultad", ListSortDirection.Ascending)); }

            //var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

            //var export = new XlsExportCollections
            //{
            //    Title = "Facultades",
            //    RightFooter = XlsHeaderFooter.PageNumber,
            //    PaperSize = XlsEnums.PaperSize.Legal,
            //    PageOrientation = XlsEnums.PageOrientation.Landscape,
            //    RepeatHeader = true,
            //    DecamelizeColumnNames = true
            //};

            //export.HiddenColumnsName.Add("CodFacultad");

            //export.PrepareWorkbook(filteredData);
            //export.DumpWorkbook("Facultades", HttpContext.ApplicationInstance.Response);

            return null;
        }

    }
}