﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
using Elecciones.Core.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace AutentitacionProyect.Controllers
{
    [Authorize(Roles = "Administrador")] 
    public class AccesosController : Controller
    {
        private readonly IUnitOfWork _uow;
        public AccesosController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        // GET: Facultad
        public ActionResult Index()
        {
            return View();
        }
      
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var data = _uow.AccesosRepository.GetAll().Select(x => new
            {
                x.AccesoId,
                x.Controlador,
                x.Accion
            }).ToDataSourceResult(request);

           
           
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, AccesosGridItem item)
        {
            var indexExist = _uow.AccesosRepository.FindBy(x => x.Controlador == item.Controlador && x.Accion == item.Accion).Any();
            if (indexExist)
            {
                var errorMessage = "Ya existe el Acceso para el Controlador: " + item.Controlador + " y Accion: " + item.Accion;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var accesosNuevo = new Accesos
            {
                Controlador = item.Controlador,
                Accion = item.Accion
               
            };

            _uow.AccesosRepository.Add(accesosNuevo);

            _uow.Save();

            item.AccesoId =  accesosNuevo.AccesoId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, AccesosGridItem item)
        {
            var indexExist = _uow.AccesosRepository.FindBy(x => x.Controlador == item.Controlador && x.Accion == item.Accion).Any();
            if (indexExist)
            {
                var errorMessage = "Ya existe el Acceso   Controlador: " + item.Controlador + " Accion: " + item.Accion;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var accesosEditar = _uow.AccesosRepository.GetById(item.AccesoId);
            accesosEditar.Controlador = item.Controlador;
            accesosEditar.Accion = item.Accion;
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, AccesosGridItem item)
        {


            var accesosEditar = _uow.AccesosRepository.GetById(item.AccesoId);

            _uow.AccesosRepository.Delete(accesosEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        {

            //var data = _uow.FacultadRepository.GetFacultadGridData();

            //if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
            //if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreFacultad", ListSortDirection.Ascending)); }

            //var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

            //var export = new XlsExportCollections
            //{
            //    Title = "Facultades",
            //    RightFooter = XlsHeaderFooter.PageNumber,
            //    PaperSize = XlsEnums.PaperSize.Legal,
            //    PageOrientation = XlsEnums.PageOrientation.Landscape,
            //    RepeatHeader = true,
            //    DecamelizeColumnNames = true
            //};

            //export.HiddenColumnsName.Add("CodFacultad");

            //export.PrepareWorkbook(filteredData);
            //export.DumpWorkbook("Facultades", HttpContext.ApplicationInstance.Response);

            return null;
        }

    }
}