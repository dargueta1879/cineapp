﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;



using System.ComponentModel;
using System.Web.UI.WebControls;
using System.IO;



namespace CineApp.Controllers.OfModels
{
    public class PeliculaController : Controller
    {
        private readonly IUnitOfWork _uow;
        public PeliculaController(IUnitOfWork uow)
        {
            _uow = uow;
        }
        //
        // GET: /Pelicula/

        public ActionResult Index()
        {



            return View();
        }


        public JsonResult GetAll()
        {
            var data = _uow.PeliculaRepository.GetAll().Select(x => new
            {
                x.PeliculaId,
                x.NombrePelicula,
                x.Clasificacion,
                x.Duracion,
                x.EstadoPeliculaId
            }).Where(x => x.EstadoPeliculaId != 2);


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult Get_EmployeeById(string Id)
        //{
        //    using (DemoEntities Obj = new DemoEntities())
        //    {
        //        int EmpId = int.Parse(Id);
        //        return Json(Obj.Employees.Find(EmpId), JsonRequestBehavior.AllowGet);
        //    }
        //}
        ///// <summary>  
        ///// Insert New Employee  
        ///// </summary>  
        ///// <param name="Employe"></param>  
        ///// <returns></returns>  
        public string Insert(Pelicula item)
        {
            if (item != null)
            {

                var peliculaNuevo = new Pelicula
                {

                    PeliculaId = item.PeliculaId,
                    NombrePelicula = item.NombrePelicula,
                    Clasificacion = item.Clasificacion,
                    Duracion = item.Duracion,
                    EstadoPeliculaId = 1,

                };

                _uow.PeliculaRepository.Add(peliculaNuevo);

                _uow.Save();

                //item.CursoId = cursoNuevo.CursoId;

                return "Pelicula agregada exitosamente!";

            }
            else
            {
                return "Pelicula Not Inserted! Try Again";
            }
        }

        [HttpPost]
        public ActionResult CrearPelicula(NuevaPeliculaItem pelicula)
        {
            if (pelicula.file != null)
            {



                string fileName = Path.GetFileNameWithoutExtension(pelicula.file.FileName);
                string extension = Path.GetExtension(pelicula.file.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                String nombreImagen = fileName;
                //imagepath = "~Image/" + fileName;
                fileName = System.IO.Path.Combine(
                                   Server.MapPath("~/Content/peliculas/"), fileName);
                pelicula.file.SaveAs(fileName);







                var peliculaNuevo = new Pelicula
                {

                    PeliculaId = pelicula.PeliculaId,
                    NombrePelicula = pelicula.NombrePelicula,
                    Clasificacion = pelicula.Clasificacion,
                    Duracion = pelicula.Duracion,
                    EstadoPeliculaId = 1,
                    Descripcion=pelicula.Descripcion,
                    Imagen = "/Content/peliculas/" + nombreImagen

                };


                _uow.PeliculaRepository.Add(peliculaNuevo);

                _uow.Save();

                _uow.Save();

                //item.CursoId = cursoNuevo.CursoId;

                return RedirectToAction("Index", "Pelicula");

            }

            return RedirectToAction("Index", "Pelicula");

        }
        ///// <summary>  
        ///// Delete Employee Information  
        ///// </summary>  
        ///// <param name="Emp"></param>  
        ///// <returns></returns>  
        public string Delete(Pelicula item)
        {
            if (item != null)
            {

                var peliculaUpdate = _uow.PeliculaRepository.GetById(item.PeliculaId);
                peliculaUpdate.EstadoPeliculaId = 2;

                _uow.Save();
                return "Película eliminada exitosamente";

            }
            else
            {
                return "No se pudo eliminar película";
            }
        }
        ///// <summary>  
        ///// Update Employee Information  
        ///// </summary>  
        ///// <param name="Emp"></param>  
        ///// <returns></returns>  
        //public string Update_Employee(Employee Emp)
        //{
        //    if (Emp != null)
        //    {
        //        using (DemoEntities Obj = new DemoEntities())
        //        {
        //            var Emp_ = Obj.Entry(Emp);
        //            Employee EmpObj = Obj.Employees.Where(x => x.Emp_Id == Emp.Emp_Id).FirstOrDefault();
        //            EmpObj.Emp_Age = Emp.Emp_Age;
        //            EmpObj.Emp_City = Emp.Emp_City;
        //            EmpObj.Emp_Name = Emp.Emp_Name;
        //            Obj.SaveChanges();
        //            return "Employee Updated Successfully";
        //        }
        //    }
        //    else
        //    {
        //        return "Employee Not Updated! Try Again";
        //    }
        //}  








        //
        // GET: /Pelicula/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    Pelicula pelicula = db.Pelicula.Find(id);
        //    if (pelicula == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pelicula);
        //}

        ////
        //// GET: /Pelicula/Create

        //public ActionResult Create()
        //{
        //    ViewBag.EstadoPeliculaId = new SelectList(db.EstadoPelicula, "EstadoPeliculaId", "NombreEstadoPelicula");
        //    return View();
        //}

        ////
        //// POST: /Pelicula/Create

        //[HttpPost]
        //public ActionResult Create(Pelicula pelicula)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Pelicula.Add(pelicula);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.EstadoPeliculaId = new SelectList(db.EstadoPelicula, "EstadoPeliculaId", "NombreEstadoPelicula", pelicula.EstadoPeliculaId);
        //    return View(pelicula);
        //}

        ////
        //// GET: /Pelicula/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    Pelicula pelicula = db.Pelicula.Find(id);
        //    if (pelicula == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.EstadoPeliculaId = new SelectList(db.EstadoPelicula, "EstadoPeliculaId", "NombreEstadoPelicula", pelicula.EstadoPeliculaId);
        //    return View(pelicula);
        //}

        ////
        //// POST: /Pelicula/Edit/5

        //[HttpPost]
        //public ActionResult Edit(Pelicula pelicula)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(pelicula).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.EstadoPeliculaId = new SelectList(db.EstadoPelicula, "EstadoPeliculaId", "NombreEstadoPelicula", pelicula.EstadoPeliculaId);
        //    return View(pelicula);
        //}

        ////
        //// GET: /Pelicula/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    Pelicula pelicula = db.Pelicula.Find(id);
        //    if (pelicula == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pelicula);
        //}

        ////
        //// POST: /Pelicula/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Pelicula pelicula = db.Pelicula.Find(id);
        //    db.Pelicula.Remove(pelicula);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }


    public class NuevaPeliculaItem
    {


        public int PeliculaId { get; set; }
        public String NombrePelicula { get; set; }
        public String Duracion { get; set; }
        public String Clasificacion { get; set; }
        public String imagepath { get; set; }
        //public Image img { get; set; }
        public HttpPostedFileBase file { get; set; }
        public String Descripcion { get; set; }


    }
}