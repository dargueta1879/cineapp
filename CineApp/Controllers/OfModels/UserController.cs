﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Models;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;

using System.Web.Security;
using WebMatrix.WebData;
using AutentitacionProyect.Filters;

namespace AutentitacionProyect.Controllers
{   [Authorize(Roles = "Administrador")]
    [InitializeSimpleMembership] 
    public class UserController : Controller
    {
         
        private AcademicLoadDBEntities db = new AcademicLoadDBEntities();
        private readonly IUnitOfWork _uow;

        //
        // GET: /User/
        public UserController(IUnitOfWork uow)
        {
            _uow = uow;
        }

    

        public ActionResult Index()
        {

            IEnumerable<UserGridItem> usuarios = _uow.UserProfileRepository.GetUsersGridData();

            return View(usuarios);
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            AutentitacionProyect.Persistence.UserProfile userprofile = db.UserProfile.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            return View(_uow.ColaboradorRepository.GetColaboradorSinUsuarioGridData());
        }

        //
        // POST: /User/Create

        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(UserCreate model)
        {
           
                // Intento de registrar al usuario
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    //WebSecurity.Login(model.UserName, model.Password);
                    var up = _uow.UserProfileRepository.FindBy(x => x.UserName == model.UserName);
                    //var up = _uow.UserProfileRepository.GetUserProfile(model.UserName);
                    var colaboradorEditar = _uow.ColaboradorRepository.GetById(model.ColaboradorId);
                    if (colaboradorEditar!=null){
                        colaboradorEditar.UserId = up.First().UserId;
                        _uow.Save();
                    
                    }
                  
                    


                    return RedirectToAction("Index", "User");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
          

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return View(model);
        }

        //The next action return The users with his roles asigned

        public ActionResult GetRolesOfUser(int userId)
        {

            var allRoles = _uow.webpages_RolesRepository.GetAll().Select(x => new
            {
                x.RoleId,
                x.RoleName
            }).OrderBy(x => x.RoleName);

            List<RoleToUserItem> listRolesToUser = null;
            
            if (allRoles != null)
            {
                listRolesToUser = new List<RoleToUserItem>();
                var rolesToUser = _uow.RoleToUserRepository.GetRolesOfUser(userId);

                foreach (var item in allRoles)
                {
                    //AccesoRolGridItem acceso;
                    //acceso = new AccesoRolGridItem();
                    //acceso.RolId = idRole;

                    //acceso.NombreAccion = item.Accion;
                    //acceso.NombreControlador = item.Controlador;
                    //acceso.AccesoId = item.AccesoId;

                    RoleToUserItem ru = new RoleToUserItem();
                    ru.RoleId = item.RoleId;
                    ru.RoleName = item.RoleName;
                    ru.UserId = userId;


                    if (rolesToUser.Count() > 0)
                    {
                        foreach (var item2 in rolesToUser)
                        {


                            if (item.RoleId == item2.RoleId)
                            {
                                ru.RoleToUserId = item2.RoleToUserId;

                                ru.isSelected = "si";
                                break;
                            }
                            else
                            {

                                ru.isSelected = "no";

                            }
                        }
                    }
                    else
                    {


                        ru.isSelected = "no";

                    }


                    listRolesToUser.Add(ru);







                }

            }




            return Json(listRolesToUser, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditRoleToUser(int roleid, int userid, int roletouserid, string operacion)
        {
            var resultado = "error";

            string nombreRol = _uow.webpages_RolesRepository.FindBy(x => x.RoleId == roleid).First().RoleName;
            string nombreUsuario = _uow.UserProfileRepository.FindBy(x => x.UserId == userid).First().UserName;
            var indexExist = _uow.RoleToUserRepository.FindBy(x => x.UserId == userid && x.RoleId == roleid).Any();

            if (indexExist)
            {

                var roltouserDelete = _uow.RoleToUserRepository.GetById(roletouserid);
                _uow.RoleToUserRepository.Delete(roltouserDelete);
                _uow.Save();
                Roles.RemoveUserFromRole(nombreUsuario, nombreRol);
                resultado = "exito";
                return Json(resultado, JsonRequestBehavior.AllowGet);


            }

            var roletouserNuevo = new RoleToUser
            {
                UserId = userid,
                RoleId = roleid

            };


          
            ViewBag.ResultMessage = "Se le ha agregado el nuevo rol !";

            _uow.RoleToUserRepository.Add(roletouserNuevo);

            Roles.AddUserToRole(nombreUsuario, nombreRol);

            _uow.Save();
            roletouserid = roletouserNuevo.RoleToUserId;

            return Json(roletouserid, JsonRequestBehavior.AllowGet);
        }




        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AutentitacionProyect.Persistence.UserProfile userprofile = db.UserProfile.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
      
        public ActionResult Edit(AutentitacionProyect.Persistence.UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userprofile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userprofile);
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var userDelete = _uow.UserProfileRepository.GetById(id);
            if (userDelete == null)
            {
                return HttpNotFound();
            }

            _uow.UserProfileRepository.Delete(userDelete);
            _uow.Save();
            return RedirectToAction("Index");
        }

        

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // Vaya a http://go.microsoft.com/fwlink/?LinkID=177550 para
            // obtener una lista completa de códigos de estado.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "El nombre de usuario ya existe. Escriba un nombre de usuario diferente.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Ya existe un nombre de usuario para esa dirección de correo electrónico. Escriba una dirección de correo electrónico diferente.";

                case MembershipCreateStatus.InvalidPassword:
                    return "La contraseña especificada no es válida. Escriba un valor de contraseña válido.";

                case MembershipCreateStatus.InvalidEmail:
                    return "La dirección de correo electrónico especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "La respuesta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "La pregunta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidUserName:
                    return "El nombre de usuario especificado no es válido. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.ProviderError:
                    return "El proveedor de autenticación devolvió un error. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                case MembershipCreateStatus.UserRejected:
                    return "La solicitud de creación de usuario se ha cancelado. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                default:
                    return "Error desconocido. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";
            }
        }


    }


    public class UserCreate
    {



        public decimal ColaboradorId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }


    }

    public class Role {

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string isChecked { get; set; }

    }

   
}

