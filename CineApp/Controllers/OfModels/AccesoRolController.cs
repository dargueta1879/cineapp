﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutentitacionProyect.Persistence;
using AutentitacionProyect.Repositories;
using Elecciones.Core.Repositories;
//using ExportTo.ExcelBase;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace AutentitacionProyect.Controllers
{
    [Authorize(Roles = "Administrador")] 
    public class AccesoRolController : Controller
    {
        private readonly IUnitOfWork _uow;
        public AccesoRolController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public ActionResult GetAccesoRol(int idRole)
        {
            var todosAccesos = _uow.AccesosRepository.GetAll().Select(x => new
            {
                x.AccesoId,
                x.Controlador,
                x.Accion
            });

            List<AccesoRolGridItem> listaAccesos=new List<AccesoRolGridItem>();
            var accessAutorizados = _uow.AccesoRolRepository.GetAccesoRolGridData(idRole);
            if (todosAccesos!=null)
            {
                foreach (var item in todosAccesos)
                {
                    AccesoRolGridItem acceso;
                    acceso = new AccesoRolGridItem();
                    acceso.RolId = idRole;

                    acceso.NombreAccion = item.Accion;
                    acceso.NombreControlador = item.Controlador;
                    acceso.AccesoId = item.AccesoId;

                    if (accessAutorizados.Count()>0)
                    {
                        foreach (var item2 in accessAutorizados)
                        {
                           

                            if (item.AccesoId == item2.AccesoId)
                            {
                                acceso.AccesoRolId = item2.AccesoRolId;
                               
                                acceso.isSelected = "si";
                                break;
                            }
                            else
                            {
                               
                                acceso.isSelected = "no";
                                
                            }
                        }
                    }
                    else {
                       

                        acceso.isSelected = "no";
                    
                    }


                        listaAccesos.Add(acceso);

                    
                   

                    


                }
            
            }
            


            return Json(listaAccesos, JsonRequestBehavior.AllowGet);
        }
        //El siguiente metodo agregara o quitara permisos a los roles

        public ActionResult EditAccesoRol(int roleid, int accesoid,int accesorolid,string operacion) {
            var resultado="error";
            var indexExist = _uow.AccesoRolRepository.FindBy(x => x.AccesoId == accesoid && x.RoleId == roleid).Any();

            if (indexExist)
            {
               
                var accesoEliminar = _uow.AccesoRolRepository.GetById(accesorolid);
                _uow.AccesoRolRepository.Delete(accesoEliminar);
                _uow.Save();
                resultado = "exito";
                return Json(resultado, JsonRequestBehavior.AllowGet);


            }

            var accesosNuevo = new AccesoRol
            {
                AccesoId = accesoid,
                RoleId = roleid

            };

            _uow.AccesoRolRepository.Add(accesosNuevo);
           


            _uow.Save();
            accesorolid = accesosNuevo.AccesoRolId;

            return Json(accesorolid, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }
      
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var data = _uow.AccesosRepository.GetAll().Select(x => new
            {
                x.AccesoId,
                x.Controlador,
                x.Accion
            }).ToDataSourceResult(request);

           
           
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create([DataSourceRequest]DataSourceRequest request, AccesosGridItem item)
        {
            var indexExist = _uow.AccesosRepository.FindBy(x => x.Controlador == item.Controlador && x.Accion == item.Accion).Any();
            
            if (indexExist)
            {
                var errorMessage = "Ya existe el Acceso para el Controlador: " + item.Controlador + " y Accion: " + item.Accion;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var accesosNuevo = new Accesos
            {
                Controlador = item.Controlador,
                Accion = item.Accion
               
            };

            _uow.AccesosRepository.Add(accesosNuevo);

            _uow.Save();

            item.AccesoId =  accesosNuevo.AccesoId;

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update([DataSourceRequest]DataSourceRequest request, AccesosGridItem item)
        {
            var indexExist = _uow.AccesosRepository.FindBy(x => x.Controlador == item.Controlador && x.Accion == item.Accion).Any();
            if (indexExist)
            {
                var errorMessage = "Ya existe el Acceso   Controlador: " + item.Controlador + " Accion: " + item.Accion;
                ModelState.AddModelError("Item", errorMessage);
                var resultData = new[] { item };
                return Json(resultData.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
            }

            var accesosEditar = _uow.AccesosRepository.GetById(item.AccesoId);
            accesosEditar.Controlador = item.Controlador;
            accesosEditar.Accion = item.Accion;
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy([DataSourceRequest]DataSourceRequest request, AccesosGridItem item)
        {


            var accesosEditar = _uow.AccesosRepository.GetById(item.AccesoId);

            _uow.AccesosRepository.Delete(accesosEditar);
            _uow.Save();

            var result = new[] { item };
            return Json(result.AsQueryable().ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        public EmptyResult ExportToExcel([DataSourceRequest]DataSourceRequest request)
        {

            //var data = _uow.FacultadRepository.GetFacultadGridData();

            //if (request.Sorts == null) { request.Sorts = new List<SortDescriptor>(); }
            //if (request.Sorts.Count == 0) { request.Sorts.Add(new SortDescriptor("NombreFacultad", ListSortDirection.Ascending)); }

            //var filteredData = data.ToDataSourceResult(request).Data.AsQueryable();

            //var export = new XlsExportCollections
            //{
            //    Title = "Facultades",
            //    RightFooter = XlsHeaderFooter.PageNumber,
            //    PaperSize = XlsEnums.PaperSize.Legal,
            //    PageOrientation = XlsEnums.PageOrientation.Landscape,
            //    RepeatHeader = true,
            //    DecamelizeColumnNames = true
            //};

            //export.HiddenColumnsName.Add("CodFacultad");

            //export.PrepareWorkbook(filteredData);
            //export.DumpWorkbook("Facultades", HttpContext.ApplicationInstance.Response);

            return null;
        }

    }
}