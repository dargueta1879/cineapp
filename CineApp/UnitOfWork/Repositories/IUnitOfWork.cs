﻿using CineApp.Persistence;
using CineApp.UnitOfWork.Repositories;
using System.Data.Entity;


namespace CineApp.UnitOfWork.Repositories
{
    public interface IUnitOfWork 
    {
        DbContext Context { get; }

        //generic interfaces
        // example:
        //IGenericRepository<TableName> TableNameRepository { get; }
        
        IGenericRepository<Pelicula> PeliculaRepository { get; }
        IGenericRepository<Cine> CineRepository { get; }
        IGenericRepository<Sala> SalaRepository { get; }
        IGenericRepository<EstadoCartelera> EstadoCarteleraRepository { get; }
        IGenericRepository<Horario> HorarioRepository { get; }
        IGenericRepository<DetalleTicket> DetalleTicketRepository { get; }
        IGenericRepository<Asociado> AsociadoRepository { get; }
        IGenericRepository<TipoEmpleado> TipoEmpleadoRepository { get; }
        IGenericRepository<Planilla> PlanillaRepository { get; }
        //IGenericRepository<webpages_Roles> webpages_RolesRepository { get; }
        //IGenericRepository<HorarioCurso> HorarioCursoRepository { get; }
       
        //IGenericRepository<CargaAcademica> CargaAcademicaRepository { get; }
        //IGenericRepository<Trabajador> TrabajadorRepository { get; }
        //IGenericRepository<Puesto> PuestoRepository { get; }


        //specific interfaces
        //exmaple:
        //ITableNameRepository TableNameRepository { get; }
        ICarteleraRepository CarteleraRepository { get; }
        ITicketRepository TicketRepository { get; }
        ISocioRepository SocioRepository { get; }
        IEmpleadoRepository EmpleadoRepository { get; }
        //IFacultadRepository FacultadRepository { get; }
        //IAccesosRepository AccesosRepository { get; }
        //IAccesoRolRepository AccesoRolRepository { get; }
        //IColaboradorRepository ColaboradorRepository { get; }
        //IUserProfileRepository UserProfileRepository { get; }
        //IRoleToUserRepository RoleToUserRepository { get; }
        //IAsignacionRepository AsignacionRepository { get; }
        //IDetalleCargaAcademicaRepository DetalleCargaAcademicaRepository { get; }
        //ICargaAcademicaRepository CargaAcademicaRepository { get; }
        //IContratoRepository ContratoRepository { get; }
        

        



        //ICarreraRepository CarreraRepository { get; }
        //IPensumRepository PensumRepository { get; }
        //ICursoRepository CursoRepository { get; }
        //IPrerrequisitoRepository PrerrequisitoRepository { get; }
        //IDetallePensumRepository DetallePensumRepository { get; }
        ////ICargaCursoContratacionRepository CargaCursoContratacionRepository { get; }
        ////IHorarioContratacionRepository HorarioContratacionRepository { get; }
        ////IHorarioCursoRepository HorarioCursoRepository { get; }
        ////IAsignacionCatedraticoFacultadRepository AsignacionCatedraticoFacultadRepository { get; }
        ////ICatedraticoRepository CatedraticoRepository { get; }
        ////IAsignacionPuestoRepository AsignacionPuestoRepository { get; }




        void Save();
        void Dispose();
        int ExecuteCommand(string sql, params object[] parameters);
    }
}
