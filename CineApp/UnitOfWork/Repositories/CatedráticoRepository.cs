﻿using System.Collections.Generic;
using System.Linq;
using Elecciones.Core.Model;

namespace Elecciones.Core.Repositories
{
    public interface ICatedraticoRepository : IGenericRepository<Catedratico>
    {
        IEnumerable<CatedraticoGridItem> GetCatedraticoGridData();
    }
    internal class CatedraticoRepository : GenericRepository<EleccionesEntities, Catedratico>, ICatedraticoRepository
    {
        public CatedraticoRepository(EleccionesEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<CatedraticoGridItem> GetCatedraticoGridData()
        {
            var dc = Context;
            var query = from catedratico in dc.Catedraticoes
                        orderby catedratico.NombreCatedratico
                        
                        select new CatedraticoGridItem
                        {
                            CatedraticoId = catedratico.CatedraticoId,
                            NombreCatedratico = catedratico.NombreCatedratico,
                            Dpi = catedratico.Dpi,
                            CodUniversitario = catedratico.CodUniversitario,
                            Plaza = catedratico.Plaza,
                            Profesion = catedratico.Profesion,
                            PuedeSerBorrado = true,
                        };

            return query;
        }
    }

    public class CatedraticoGridItem
    {
        public int CatedraticoId { get; set; }
        public string NombreCatedratico { get; set; }
        public string Dpi { get; set; }
        public string CodUniversitario { get; set; }
        public string Plaza { get; set; }
        public string Profesion { get; set; }
        public bool PuedeSerBorrado { get; set; }
        
    }
}
