﻿using System.Collections.Generic;
using System.Linq;
using CineApp.Persistence;
using System;
using CineApp.UnitOfWork.Repositories;

namespace CineApp.UnitOfWork.Repositories
{
    public interface IEmpleadoRepository : IGenericRepository<Empleado>
    {
       
        //IEnumerable<CarteleraGridItem> GetCarteleraGridData();
        //IEnumerable<CarteleraGridItem> GetCarteleraByCine(int CineId);
        //IEnumerable<SocioItem> GetSocioByUsername(String username);
        //IEnumerable<SocioGridItem> GetSociosGridData();
        IEnumerable<EmpleadoGridItem> GetEmpleadoGridData();
        
    }

    internal class EmpleadoRepository : GenericRepository<DBCineEntities, Empleado>, IEmpleadoRepository
    {
        public EmpleadoRepository(DBCineEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<EmpleadoGridItem> GetEmpleadoGridData()
        {
            var dc = Context;
            var query = from empleado in dc.Empleado
                        join ee in dc.EstadoEmpleado on empleado.EstadoEmpleadoId equals ee.EstadoEmpleadoId
                        join planilla in dc.Planilla on empleado.EmpleadoId equals planilla.EmpleadoId
                        join cine in dc.Cine on planilla.CineId equals cine.CineId
                        join te in dc.TipoEmpleado on empleado.TipoEmpleadoId equals te.TipoEmpleadoId
                        where empleado.EstadoEmpleadoId!=3

                        select new EmpleadoGridItem
                        {
                            EmpleadoId = empleado.EmpleadoId,
                            Nombres = empleado.Nombres,
                            Apellidos = empleado.Apellidos,
                            
                            Username = empleado.Username,
                           
                            EstadoEmpleadoId = empleado.EstadoEmpleadoId??0,
                            TipoEmpleadoId = te.TipoEmpleadoId,
                            NombreTipoEmpleado = te.NombreTipoEmpleado,
                            NombreEstadoEmpleado = ee.NombreEstadoEmpleado,
                            isActived = (ee.EstadoEmpleadoId == 1) ? true : false,
                            CineId = cine.CineId,
                            NombreCine = cine.NombreCine,
                            Contrasena=empleado.password


                        };

            return query;
        }

        //public CarreraGridItem GetCarreraIdByDetallePensumId(decimal DetallePensumId)
        //{
        //    var dc = Context;
        //    var query = from dp in dc.DetallePensum 
        //                join p in dc.Pensum on dp.PensumId equals p.PensumId
        //                join car in dc.Carrera on p.CarreraId equals car.CarreraId

        //                where dp.DetallePensumId==DetallePensumId
                           
        //                select new CarreraGridItem
        //                {
        //                    CarreraId = car.CarreraId,
        //                    NombreCarrera = car.NombreCarrera,
        //                    FacultadId = car.FacultadId,
        //                    PuedeSerBorrado = true,
        //                };

        //    return query.First()??null;
        //}


        //public IEnumerable<SocioItem> GetSocioByUsername(String username)
        //{
        //    var dc = Context;
        //    var query = from socio in dc.Socio
        //                join es in dc.EstadoSocio on socio.EstadoSocioId equals es.EstadoSocioId
        //                where socio.CodCliente==username
        //                select new SocioItem
        //                {
        //                   CodCliente=username,
        //                   SocioId=socio.SocioId,
        //                   Nombres=socio.Nombres,
        //                   Apellidos=socio.Apellidos,
        //                   Puntos=socio.NumPuntos,
        //                   EstadoSocioId=es.EstadoSocioId
                           


        //                };

        //    return query;
        //}

        //public IEnumerable<SocioGridItem> GetSociosGridData()
        //{
        //    var dc = Context;
        //    var query = from socio in dc.Socio
        //                join es in dc.EstadoSocio on socio.EstadoSocioId equals es.EstadoSocioId
        //                join aso in dc.Asociado on socio.SocioId equals aso.SocioId
        //                join cine in dc.Cine on aso.CineId equals cine.CineId
        //                join ts in dc.TipoSocio on socio.TipoSocioId equals ts.TipoSocioId
                        
        //                select new SocioGridItem
        //                {
        //                    SocioId=socio.SocioId,
        //                    Nombres=socio.Nombres,
        //                    Apellidos=socio.Apellidos,
        //                    NumPuntos=socio.NumPuntos,
        //                    Username=socio.CodCliente,
        //                    CarneAnciano=socio.CarneAnciano,
        //                    CarneEscolar=socio.CarneEscolar,
        //                    EstadoSocioId = socio.EstadoSocioId,
        //                    TipoSocioId=ts.TipoSocioId,
        //                    NombreTipoSocio=ts.NombreTipoSocio,
        //                    NombreEstadoSocio=es.NombreEstado,
        //                    isActived=(es.EstadoSocioId==1)?true:false,
        //                    CineId=cine.CineId,
        //                    NombreCine=cine.NombreCine


        //                };

        //    return query;
        }

        //public IEnumerable<CarteleraGridItem> GetCarteleraByCine(int CineId)
        //{
        //    var dc = Context;
        //    var query = from cartelera in dc.Cartelera
        //                join pelicula in dc.Pelicula on cartelera.PeliculaId equals pelicula.PeliculaId
        //                join horario in dc.Horario on cartelera.HorarioId equals horario.HorarioId
        //                join sala in dc.Sala on cartelera.SalaId equals sala.SalaId
        //                join es in dc.EstadoCartelera on cartelera.EstadoCarteleraId equals es.EstadoCarteleraId
        //                join cine in dc.Cine on sala.CineId equals cine.CineId
        //                where cartelera.EstadoCarteleraId != 2 && sala.CineId==CineId
        //                orderby cine.CineId ascending, horario.HoraInicio ascending, sala.SalaId ascending, pelicula.PeliculaId ascending
        //                select new CarteleraGridItem
        //                {
        //                    CarteleraId = cartelera.CarteleraId,
        //                    PeliculaId = pelicula.PeliculaId,
        //                    EstadoCarteleraId = es.EstadoCarteleraId,
        //                    HorarioId = horario.HorarioId,


        //                    CineId = cine.CineId,
        //                    SalaId = sala.SalaId,
        //                    NombrePelicula = pelicula.NombrePelicula,
        //                    NombreEstadoCartelera = es.NombreEstadoCartelera,
        //                    HoraInicio = horario.HoraInicio,
        //                    HoraFin = horario.HoraFin,
        //                    Dia = horario.Dia,
        //                    NombreSala = sala.NombreSala,
        //                    NombreCine = cine.NombreCine


        //                };

        //    return query;
        //}


        //public IEnumerable<PeliculaItem> GetAllPeliculasInCartelera()
        //{
        //    var dc = Context;
        //    var query = from cartelera in dc.Cartelera
        //                join peli in dc.Pelicula on cartelera.PeliculaId equals peli.PeliculaId
        //                where peli.EstadoPeliculaId==1
        //                group peli by peli.PeliculaId into pe
        //                select new PeliculaItem
        //                {
        //                  PeliculaId=pe.Key,
        //                   NombrePelicula = pe.Select(x => x.NombrePelicula).FirstOrDefault(),
        //                   Clasificacion=pe.Select(x => x.Clasificacion).FirstOrDefault(),
        //                   Duracion=pe.Select(x => x.Duracion).FirstOrDefault(),
        //                   Imagen=pe.Select(x => x.Imagen).FirstOrDefault(),
        //                   Descripcion=pe.Select(x => x.Descripcion).FirstOrDefault()
                            


        //                };

        //    return query;
        //}
    //}
    //public class CarreraComboItem
    //{
    //    public decimal PensumId { get; set; }
    //    public string NombreCarrera { get; set; }
    //}
       
        
    //}

    //public class PeliculaItem
    //{
    //    public int PeliculaId { get; set; }
    //    public int EstadoPeliculaId { get; set; }
 

    //    public string NombrePelicula { get; set; }
    //    public string Descripcion { get; set; }
    //    public string Imagen { get; set; }
    //    public string Clasificacion { get; set; }
    //    public string Duracion { get; set; }
        
    //}

    //public class SocioItem
    //{
    //    public int SocioId { get; set; }
    //    public String CodCliente { get; set; }
    //    public String Nombres { get; set; }
    //    public String Apellidos { get; set; }
    //    public String Puntos { get; set; }
    //    public int EstadoSocioId { get; set; }

    //    //public int EstadoCarteleraId { get; set; }
    //    //public int HorarioId { get; set; }
    //    //public int CineId { get; set; }
    //    //public int SalaId { get; set; }

    //    //public string NombrePelicula { get; set; }
    //    //public string NombreEstadoCartelera { get; set; }
    //    //public string HoraInicio { get; set; }
    //    //public string HoraFin { get; set; }
    //    //public string Dia { get; set; }
    //    //public string NombreSala { get; set; }
    //    //public string NombreCine { get; set; }
    //}

    public class EmpleadoGridItem
    {
        public int EmpleadoId { get; set; }
        public int CineId { get; set; }
        public int TipoEmpleadoId { get; set; }
        public int EstadoEmpleadoId { get; set; }
        public String Username { get; set; }
       
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
       
        public String NombreEstadoEmpleado { get; set; }
        public String NombreCine { get; set; }
        public String NombreTipoEmpleado { get; set; }
        public String Contrasena { get; set; }
        public bool isActived { get; set; }



        //public int EstadoCarteleraId { get; set; }
        //public int HorarioId { get; set; }
        //public int CineId { get; set; }
        //public int SalaId { get; set; }

        //public string NombrePelicula { get; set; }
        //public string NombreEstadoCartelera { get; set; }
        //public string HoraInicio { get; set; }
        //public string HoraFin { get; set; }
        //public string Dia { get; set; }
        //public string NombreSala { get; set; }
        //public string NombreCine { get; set; }
    }



}
