﻿using System.Collections.Generic;
using System.Linq;
using CineApp.Persistence;
using System;
using CineApp.UnitOfWork.Repositories;
using CineApp.Controllers.OfModels;

namespace CineApp.UnitOfWork.Repositories
{
    public interface ITicketRepository : IGenericRepository<Ticket>
    {

        IEnumerable<AsientoItem> GetAsientosOcupados(EntradaItem item);
        IEnumerable<AsientoItem> GetAllAsientosByCarteleraId(int id);
        //IEnumerable<CarteleraGridItem> GetCarteleraByCine(int CineId);
        
    }

    internal class TicketRepository : GenericRepository<DBCineEntities, Ticket>, ITicketRepository
    {
        public TicketRepository(DBCineEntities ctx)
            : base(ctx)
        {
        }



        public IEnumerable<AsientoItem> GetAsientosOcupados(EntradaItem item)
        {
            var dc = Context;
            var query = from cartelera in dc.Cartelera
                        join dt in dc.DetalleTicket on cartelera.CarteleraId equals dt.CarteleraId
                        join ticket in dc.Ticket on dt.TicketId equals ticket.TicketId
                        
                       
                        join asiento in dc.Asiento on dt.AsiendoId equals asiento.AsiendoId
                        where cartelera.CarteleraId == item.CarteleraId && ticket.Dia == item.Dia && ticket.Mes == item.Mes && ticket.Ano==item.Año
                     
                        
                        select new AsientoItem
                        {
                            AsientoId= asiento.AsiendoId,
                            NombreAsiento=asiento.NombreAsiento,
                            Disponible=false


                        };

            return query;
        }

        public IEnumerable<AsientoItem> GetAllAsientosByCarteleraId(int id)
        {
            var dc = Context;
            var query = from cartelera in dc.Cartelera

                        join sala in dc.Sala on cartelera.SalaId equals sala.SalaId
                        join asiento in dc.Asiento on sala.SalaId equals asiento.SalaId
                        where id==cartelera.CarteleraId


                        select new AsientoItem
                        {
                            AsientoId = asiento.AsiendoId,
                            NombreAsiento = asiento.NombreAsiento,
                            Disponible = true


                        };

            return query;
        }
  
       
        
    }




    public class AsientoItem
    {
        public int AsientoId { get; set; }
        public String NombreAsiento { get; set; }
        public bool Disponible { get; set; }
    }




}
