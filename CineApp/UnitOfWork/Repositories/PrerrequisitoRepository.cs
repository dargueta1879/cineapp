﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;

namespace AutentitacionProyect.Repositories
{
    public interface IPrerrequisitoRepository : IGenericRepository<Prerrequisito>
    {
        IEnumerable<PrerrequisitoGridItem> GetPrerrequisitoGridData(int item);
    }
    internal class PrerrequisitoRepository : GenericRepository<AcademicLoadDBEntities, Prerrequisito>, IPrerrequisitoRepository
    {
        public PrerrequisitoRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<PrerrequisitoGridItem> GetPrerrequisitoGridData(int item)
        {
                
            var dc = Context;
                var query = 
                from prerrequisito in dc.Prerrequisito
                join detallePensum in dc.DetallePensum on prerrequisito.DetallePensumId equals detallePensum.DetallePensumId
                join curso in dc.Curso on prerrequisito.CursoId equals curso.CursoId
                where prerrequisito.DetallePensumId==item
                select new PrerrequisitoGridItem
                {
                    PrerrequisitoId = prerrequisito.PrerrequisitoId,
                    DetallePensumId = prerrequisito.DetallePensumId,
                    CursoId = prerrequisito.CursoId,
                    NombreCurso=curso.NombreCurso
                };

            return query;
        }
    }

    public class PrerrequisitoGridItem
    {
        public decimal PrerrequisitoId { get; set; }
        public decimal DetallePensumId { get; set; }
        public decimal CursoId { get; set; }
        public string NombreCurso { get; set; }
        
    }
}