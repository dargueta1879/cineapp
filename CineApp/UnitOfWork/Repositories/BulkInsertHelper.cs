﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace Dorel.Core.Repositories
{
    public interface IBulkInsertHelper
    {
        void BulkInsertAll<T>(IEnumerable<T> entities);
    }

    public class BulkInsertHelper : IBulkInsertHelper
    {
        protected SqlConnection Connection { get; set; }

        public BulkInsertHelper(IDbConnection connection)
        {
            Connection = (SqlConnection) connection;
        }

        public void BulkInsertAll<T>(IEnumerable<T> entities)
        {
            try
            {
                if (Connection.State == ConnectionState.Closed)
                {
                    Connection.Open();
                }


                var type = typeof (T);

                var tableAttribute = (TableAttribute) type.GetCustomAttributes(typeof (TableAttribute), false).Single();

                var bulkCopy = new SqlBulkCopy(Connection) {DestinationTableName = tableAttribute.Name};

                var properties = type.GetProperties().Where(EventTypeFilter).ToArray();
                var table = new DataTable {Locale = CultureInfo.InvariantCulture};

                foreach (var property in properties)
                {
                    Type propertyType = property.PropertyType;
                    if (propertyType.IsGenericType &&
                        propertyType.GetGenericTypeDefinition() == typeof (Nullable<>))
                    {
                        propertyType = Nullable.GetUnderlyingType(propertyType);
                    }

                    table.Columns.Add(new DataColumn(property.Name, propertyType));
                }

                foreach (var entity in entities)
                {
                    table.Rows.Add(properties.Select(
                        property => GetPropertyValue(property.GetValue(entity, null))).ToArray());
                }

                bulkCopy.WriteToServer(table);
            }
            finally
            {
                Connection.Close();
            }
        }

        private bool EventTypeFilter(System.Reflection.PropertyInfo p)
        {
            var attribute = Attribute.GetCustomAttribute(p,
                typeof(AssociationAttribute)) as AssociationAttribute;

            if (attribute == null) return true;
            if (attribute.IsForeignKey == false) return true;

            return false;
        }

        private static object GetPropertyValue(object o)
        {
            if (o == null)
                return DBNull.Value;
            return o;
        }
    }
}
