﻿using System.Collections.Generic;
using System.Linq;
using Elecciones.Core.Model;

namespace Elecciones.Core.Repositories
{
    public interface IAsignacionCatedraticoFacultadRepository : IGenericRepository<AsignacionCatedraticoFacultad>
    {
        IEnumerable<AsignacionCatedraticoGridItem> GetAsignacionCatedraticoGridData(int CatedraticoId);

    }
    internal class AsignacionCatedraticoFacultadRepository : GenericRepository<EleccionesEntities, AsignacionCatedraticoFacultad>, IAsignacionCatedraticoFacultadRepository
    {
        public AsignacionCatedraticoFacultadRepository(EleccionesEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<AsignacionCatedraticoGridItem> GetAsignacionCatedraticoGridData(int CatedraticoId)
        {
            var dc = Context;
            var query = from asig in dc.AsignacionCatedraticoFacultads
                join catedratico in dc.Catedraticoes on asig.CatedraticoId equals catedratico.CatedraticoId
                join facultad in dc.Facultads on asig.FacultadId equals facultad.CodFacultad
                where catedratico.CatedraticoId==CatedraticoId
               
                        select new AsignacionCatedraticoGridItem
                {
                AsignacionCatedraticoId=asig.AsignacionCatedraticoFacultadId,
                CodFacultad=facultad.CodFacultad,
                NombreCatedratico=catedratico.NombreCatedratico,
                NombreFacultad=facultad.NombreFacultad
                };

            return query;
        }
    }

    public class AsignacionCatedraticoGridItem
    {

        public int AsignacionCatedraticoId { get; set; }
        public int CatedraticoId { get; set; }
        public int CodFacultad { get; set; }
        public string NombreFacultad { get; set; }
        public string NombreCatedratico { get; set; }

}
}
