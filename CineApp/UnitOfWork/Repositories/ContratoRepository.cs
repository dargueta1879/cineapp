﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IContratoRepository : IGenericRepository<Contrato>
    {
        //IEnumerable<DetalleCargaAcademicaItem> GetDetalleCargaAcademicaGridDataByIdCargaAcademica(decimal CargaAcademicaId);
        IEnumerable<PuestoItem> GetAllPuestosGridItem();
        IEnumerable<ContratoItem> GetAllContratosByCargaAcademicaId(decimal CargaAcademicaId);
        IEnumerable<HorarioFirmaItem> GetHorariosFirma(decimal ContratoId);
        IEnumerable<DetalleCursoItem> GetDetalleCursosByContratoId(decimal ContratoId);
        ContratoItem GetContratoFormItem(decimal ContratoId);
    }
    internal class ContratoRepository : GenericRepository<AcademicLoadDBEntities, Contrato>, IContratoRepository
    {
        public ContratoRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<DetalleCargaAcademicaItem> GetDetalleCargaAcademicaGridDataByIdCargaAcademica(decimal CargaAcademicaId)
        {
            var dc = Context;
            var query = from dca in dc.DetalleCargaAcademica
                        join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
                        join c in dc.Curso on dp.CursoId equals c.CursoId
                        join ec in dc.EstadoCargaAcademica on dca.EstadoCargaId equals ec.EstadoCargaId
                        join p in dc.Pensum on dp.PensumId equals p.PensumId
                        join col in dc.Colaborador on dca.ColaboradorId equals col.ColaboradorId
                        join car in dc.Carrera on p.CarreraId equals car.CarreraId
                        where dca.CargaAcademicaId==CargaAcademicaId
                        orderby p.CarreraId, dp.Ciclo

                        select new DetalleCargaAcademicaItem
                        {
                            DetalleCargaAcademicaId = dca.DetalleCargaAcademicaId,
                            CargaAcademicaId = dca.CargaAcademicaId,
                            DetallePensumId = dca.DetallePensumId,
                            HorasCursoMensual = dca.HorasCursoMensual ?? 0,
                            EstadoCargaId = dca.EstadoCargaId,
                            ColaboradorId = dca.ColaboradorId,
                            ContratoId = dca.ContratoId ?? 0,
                            NombreCurso = c.NombreCurso,
                            NombreEstado = ec.NombreEstadoCargaAcademica,
                            NombreColaborador = col.PrimerNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
                            PuedeSerBorrado = true,
                            CodCurso=c.CodCurso,
                            Ciclo=dp.Ciclo??0,
                            NombreCarrera=car.NombreCarrera
                        };

            return query;
        }


        public IEnumerable<PuestoItem> GetAllPuestosGridItem()
        {
            var dc = Context;
            var query = from p in dc.Puesto
                
                        orderby p.EscalaSugerida

                        select new PuestoItem
                        {
                            PuestoId=p.PuestoId,
                            NombrePuesto=p.NombrePuesto,
                            Clasificacion=p.Clasificacion,
                            Categoria=p.Categoria,
                            EscalaSugerida=p.EscalaSugerida??0,
                            DescripcionPuesto=p.DescripcionPuesto
                            
                        };

            return query;
        }

        public IEnumerable<ContratoItem> GetAllContratosByCargaAcademicaId(decimal CargaAcademicaId)
        {
            var dc = Context;
            var query = from con in dc.Contrato
                        join ca in dc.CargaAcademica on con.CargaAcademicaId equals ca.CargaAcademicaId
                        join p in dc.Puesto on con.PuestoId equals p.PuestoId
                        join ec in dc.EstadoContrato on con.EstadoContratoId equals ec.EstadoContratoId
                        join col in dc.Colaborador on con.ColaboradorId equals col.ColaboradorId

                        where con.CargaAcademicaId == CargaAcademicaId

                        orderby con.ContratoId ascending

                        select new ContratoItem
                        {

                            ContratoId = con.ContratoId,
                            Escala = con.Escala ?? 0,
                            ColaboradorId = col.ColaboradorId,
                            NombreColaborador = col.PrimerNombre + " " + col.SegundoNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
                            PuestoId = p.PuestoId,
                            NombrePuesto = p.NombrePuesto,
                            EstadoContrato = ec.EstadoContratoId,
                            NombreEstadoContrato = ec.NombreEstadoContrato,
                            Plaza = con.Plaza ?? 0,
                            Bono = con.BonoMensual ?? 0,
                            CargaAcademicaId = ca.CargaAcademicaId,
                            Atribuciones = con.Atribuciones,
                            Clasificacion=p.Clasificacion,
                            CarneUniversitario=col.CarneUniversitarioId,
                            HorasContratacion = con.HorasContratacionMensual ?? 0,
                            

                            //HorarioFirma = GetHorariosFirma(con.ContratoId),
                            //DetalleCursos = GetDetalleCursosByContratoId(con.ContratoId)
                        };

            return query;
        }


        public ContratoItem GetContratoFormItem(decimal ContratoId)
        {
            var dc = Context;
            var query = from con in dc.Contrato
                        
                        join p in dc.Puesto on con.PuestoId equals p.PuestoId
                        join ec in dc.EstadoContrato on con.EstadoContratoId equals ec.EstadoContratoId
                        join col in dc.Colaborador on con.ColaboradorId equals col.ColaboradorId
                        join ca in dc.CargaAcademica on con.CargaAcademicaId equals ca.CargaAcademicaId

                        where con.ContratoId==ContratoId

                       

                        select new ContratoItem
                        {

                            ContratoId = con.ContratoId,
                            Escala = con.Escala ?? 0,
                            ColaboradorId = col.ColaboradorId,
                            NombreColaborador = col.PrimerNombre + " " + col.PrimerApellido,
                            PuestoId = p.PuestoId,
                            NombrePuesto = p.NombrePuesto,
                            EstadoContrato = ec.EstadoContratoId,
                            NombreEstadoContrato = ec.NombreEstadoContrato,
                            Plaza = con.Plaza ?? 0,
                            Bono = con.BonoMensual ?? 0,
                            Atribuciones = con.Atribuciones,
                            Clasificacion = p.Clasificacion,
                            CarneUniversitario=col.CarneUniversitarioId,
                            CargaAcademicaId=con.CargaAcademicaId??0,

                            HorasContratacion = con.HorasContratacionMensual ?? 0

                            //HorarioFirma = GetHorariosFirma(con.ContratoId),
                            //DetalleCursos = GetDetalleCursosByContratoId(con.ContratoId)
                        };


           

            return  query.First();
        }


        public IEnumerable<HorarioFirmaItem> GetHorariosFirma(decimal ContratoId)
        {
            var dc = Context;
            var query = from con in dc.Contrato
                       join hf in dc.HorarioFirma on con.ContratoId equals hf.ContratoId
                       where con.ContratoId==ContratoId

                        select new HorarioFirmaItem
                        {
                            Dia=hf.Dia,
                            HoraInicio=hf.HoraInicio,
                            HoraFin=hf.HoraFin
                            

                           
                        };

            return query;
        }

        public IEnumerable<DetalleCursoItem> GetDetalleCursosByContratoId(decimal ContratoId)
        {
            var dc = Context;
            var query = from con in dc.Contrato
                        join dca in dc.DetalleCargaAcademica on con.ContratoId equals dca.ContratoId
                        join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
                        join cur in dc.Curso on dp.CursoId equals cur.CursoId


                        where con.ContratoId == ContratoId

                        orderby cur.NombreCurso ascending

                        select new DetalleCursoItem
                        {
                           NombreCurso=cur.NombreCurso,
                           DetalleCargaAcademicaId=dca.DetalleCargaAcademicaId




                        };
         

            return query;
        }




        
    }

    //public class DetalleCargaAcademicaItem
    //{
    //    public decimal DetalleCargaAcademicaId { get; set; }
    //    public decimal CargaAcademicaId { get; set; }
    //    public decimal DetallePensumId { get; set; }
    //    public decimal HorasCursoMensual { get; set; }
    //    public int EstadoCargaId { get; set; }
    //    public decimal ContratoId { get; set; }
    //    public decimal ColaboradorId { get; set; }
    //    public string NombreCurso { get; set; }
    //    public string NombreColaborador { get; set; }
    //    public string NombreEstado { get; set; }
    //    public string NombreCarrera { get; set; }
       
    //    public bool PuedeSerBorrado { get; set; }
    //    public string CodCurso { get; set; }
    //    public decimal Ciclo { get; set; }


    //}

    public class PuestoItem
    {
        public decimal PuestoId { get; set; }
       
        public string NombrePuesto { get; set; }
        public string Categoria { get; set; }
        public string Clasificacion { get; set; }
        public decimal EscalaSugerida { get; set; }
        public string DescripcionPuesto { get; set; }
     


    }

    public class HorarioFirmaItem
    {
        public decimal HorarioFirmaId;

        public string Dia { get; set; }
        public TimeSpan? HoraInicio { get; set; }
        public TimeSpan? HoraFin { get; set; }
        



    }

    public class ContratoItem
    {

        public decimal ContratoId { get; set; }
        public decimal Escala { get; set; }
        public decimal ColaboradorId { get; set; }
        public string NombreColaborador { get; set; }
        public decimal PuestoId { get; set; }
        public string NombrePuesto { get; set; }
        public decimal EstadoContrato { get; set; }
        public string NombreEstadoContrato { get; set; }
        public decimal Plaza { get; set; }
        public decimal Bono { get; set; }
        public decimal CargaAcademicaId { get; set; }
        public string Atribuciones { get; set; }
        public IEnumerable<HorarioFirma> HorarioFirma { get; set; }
        public IEnumerable<DetalleCursoItem> DetalleCursos { get; set; }
        public decimal HorasContratacion { get; set; }
        public string Clasificacion { get; set; }
        public string CarneUniversitario { get; set; }
        public decimal Salario { get; set; }





    }

    public class DetalleCursoItem
    {

        public decimal DetalleCargaAcademicaId { get; set; }
        public string NombreCurso { get; set; }


    }
}
