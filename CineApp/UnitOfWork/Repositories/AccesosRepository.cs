﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IAccesosRepository : IGenericRepository<Accesos>
    {
        IEnumerable<AccesosGridItem> GetAccesosGridData();
    }
    internal class AccesosRepository : GenericRepository<AcademicLoadDBEntities, Accesos>, IAccesosRepository
    {
        public AccesosRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<AccesosGridItem> GetAccesosGridData()
        {
            var dc = Context;
            var query = from accesos in dc.Accesos
                        
                        select new AccesosGridItem
                        {
                            AccesoId =accesos.AccesoId ,
                            Controlador = accesos.Controlador,
                            Accion = accesos.Accion
                        };

            return query;
        }
    }

    public class AccesosGridItem
    {
        public int AccesoId { get; set; }
        public string Controlador { get; set; }
        public string Accion { get; set; }
       
        
    }
}
