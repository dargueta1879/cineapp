﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IUserProfileRepository : IGenericRepository<UserProfile>
    {
        IEnumerable<UserGridItem> GetUsersGridData();
        IEnumerable<AccesoItem> GetRolesOfUser(int UserId);
        //UserProfile GetUserProfile(string username);
    }
    internal class UserProfileRepository : GenericRepository<AcademicLoadDBEntities, UserProfile>, IUserProfileRepository
    {
        public UserProfileRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }


       

        //public IEnumerable<UserGridItem> GetUsersGridData()
        //{
        //    var dc = Context;
        //    var query = from c in dc.Colaborador
        //                join up in dc.UserProfile on c.UserId equals up.UserId
        //                into JoinedUser
        //                from col in JoinedUser.DefaultIfEmpty()
        //                join eu in dc.EstadoUsuario on c.EstadoUsuarioId equals eu.EstadoUsuarioId
        //                into JoinedColEst
        //                from col2 in JoinedColEst.DefaultIfEmpty()

        //                select new UserGridItem
        //                {
        //                   UserId=col.UserId==null?0:col.UserId,
        //                   UserName=col.UserName,
        //                   ColaboradorName=c.PrimerNombre+" "+c.PrimerApellido+" "+c.SegundoApellido,
        //                   //ColaboradorId = (int) c.ColaboradorId,
        //                   EstadoId = col2.EstadoUsuarioId == null ? 0 : col2.EstadoUsuarioId,
        //                   EstadoName=col2.NombreEstadoUsuario
        //                };

        //    return query;
        //}

        public IEnumerable<UserGridItem> GetUsersGridData()
        {
            var dc = Context;
            var query = from up in dc.UserProfile
                        join c in dc.Colaborador on up.UserId equals c.UserId
                        into JoinedUser
                        from col in JoinedUser.DefaultIfEmpty()
                        join eu in dc.EstadoUsuario on col.EstadoUsuarioId equals eu.EstadoUsuarioId
                         into JoinedColEst
                        from col2 in JoinedColEst.DefaultIfEmpty()
                        orderby up.UserName
                       

                        select new UserGridItem
                        {
                            UserId = up.UserId,
                            UserName = up.UserName,
                            ColaboradorName = col.PrimerNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
                            ColaboradorId = col.ColaboradorId==null?0 :col.ColaboradorId,
                            EstadoId = col2.EstadoUsuarioId == null ? 0 : col2.EstadoUsuarioId,
                            EstadoName = col2.NombreEstadoUsuario
                        };

            return query;
        }

        public IEnumerable<AccesoItem> GetRolesOfUser(int UserId)
        {
            var dc = Context;
            var query = from up in dc.UserProfile
                        join ru in dc.RoleToUser on up.UserId equals ru.UserId
                        join rol in dc.webpages_Roles on ru.RoleId equals rol.RoleId
                        join ar in dc.AccesoRol on rol.RoleId equals ar.RoleId
                        join acc in dc.Accesos on ar.AccesoId equals acc.AccesoId
                        where up.UserId==UserId


                        select new AccesoItem
                        {
                         
                          Accion=acc.Accion,
                          Controlador=acc.Controlador

                        };

            return query;
        }
    }





    public class UserGridItem
    {
        public int UserId { get; set; }
        public decimal? ColaboradorId { get; set; }
        public int EstadoId { get; set; }
        public string UserName { get; set; }
        public string ColaboradorName { get; set; }
        public string EstadoName { get; set; }

    }

    public class UserProfileItem
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        

    }

    public class AccesoItem { 
    
        public string Accion { get; set; }
        public string Controlador { get; set; }
    
    }
}
