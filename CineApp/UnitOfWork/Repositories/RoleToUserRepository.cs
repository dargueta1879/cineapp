﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IRoleToUserRepository : IGenericRepository<RoleToUser>
    {
        IEnumerable<RoleToUserItem> GetRolesOfUser(int userId);
        //UserProfile GetUserProfile(string username);
    }
    internal class RoleToUserRepository : GenericRepository<AcademicLoadDBEntities, RoleToUser>, IRoleToUserRepository
    {
        public RoleToUserRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }


        public IEnumerable<RoleToUserItem> GetRolesOfUser(int userId)
        {
            var dc = Context;
            var query = 
                        from ru in dc.RoleToUser 
                        where ru.UserId==userId

                        select new RoleToUserItem
                        {
                            RoleToUserId = ru.RoleToUserId,
                            UserId=userId,
                            RoleId=ru.RoleId??0,
                            isSelected = "si"
                        };


            return query;
        }


        //public IEnumerable<UserGridItem> GetRolesToUser()
        //{
        //    var dc = Context;
        //    var query = from rol in dc.webpages_Roles
        //                join c in dc.we on up.UserId equals c.UserId
        //                into JoinedUser
        //                from col in JoinedUser.DefaultIfEmpty()
        //                join eu in dc.EstadoUsuario on col.EstadoUsuarioId equals eu.EstadoUsuarioId
        //                 into JoinedColEst
        //                from col2 in JoinedColEst.DefaultIfEmpty()
        //                orderby up.UserName


        //                select new UserGridItem
        //                {
        //                    UserId = col.UserId ?? 0,
        //                    UserName = up.UserName,
        //                    ColaboradorName = col.PrimerNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
        //                    ColaboradorId = col.ColaboradorId == null ? 0 : col.ColaboradorId,
        //                    EstadoId = col2.EstadoUsuarioId == null ? 0 : col2.EstadoUsuarioId,
        //                    EstadoName = col2.NombreEstadoUsuario
        //                };

        //    return query;
        //}

        //public IEnumerable<UserGridItem> GetUsersGridData()
        //{
        //    var dc = Context;
        //    var query = from c in dc.Colaborador
        //                join up in dc.UserProfile on c.UserId equals up.UserId
        //                into JoinedUser
        //                from col in JoinedUser.DefaultIfEmpty()
        //                join eu in dc.EstadoUsuario on c.EstadoUsuarioId equals eu.EstadoUsuarioId
        //                into JoinedColEst
        //                from col2 in JoinedColEst.DefaultIfEmpty()

        //                select new UserGridItem
        //                {
        //                   UserId=col.UserId==null?0:col.UserId,
        //                   UserName=col.UserName,
        //                   ColaboradorName=c.PrimerNombre+" "+c.PrimerApellido+" "+c.SegundoApellido,
        //                   //ColaboradorId = (int) c.ColaboradorId,
        //                   EstadoId = col2.EstadoUsuarioId == null ? 0 : col2.EstadoUsuarioId,
        //                   EstadoName=col2.NombreEstadoUsuario
        //                };

        //    return query;
        //}

        //public IEnumerable<UserGridItem> GetUsersGridData()
        //{
        //    var dc = Context;
        //    var query = from up in dc.UserProfile
        //                join c in dc.Colaborador on up.UserId equals c.UserId
        //                into JoinedUser
        //                from col in JoinedUser.DefaultIfEmpty()
        //                join eu in dc.EstadoUsuario on col.EstadoUsuarioId equals eu.EstadoUsuarioId
        //                 into JoinedColEst
        //                from col2 in JoinedColEst.DefaultIfEmpty()
        //                orderby up.UserName
                       

        //                select new UserGridItem
        //                {
        //                    UserId = col.UserId??0,
        //                    UserName = up.UserName,
        //                    ColaboradorName = col.PrimerNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
        //                    ColaboradorId = col.ColaboradorId==null?0 :col.ColaboradorId,
        //                    EstadoId = col2.EstadoUsuarioId == null ? 0 : col2.EstadoUsuarioId,
        //                    EstadoName = col2.NombreEstadoUsuario
        //                };

        //    return query;
        //}

        //public UserProfile GetUserProfile(string username)
        //{
        //    var dc = Context;
        //    var query = from up in dc.UserProfile
        //               where up.UserName==username


        //                select new UserProfile
        //                {
        //                    UserId = up.UserId,
        //                    UserName = up.UserName,
                            
        //                };

        //    return  query;
        //}
    }

    //public class UserToRoleItem
    //{
    //    public int AccesoRolId { get; set; }
    //    public int AccesoId { get; set; }
    //    public int RolId { get; set; }
    //    public string isSelected { get; set; }
    //    public string NombreControlador { get; set; }
    //    public string NombreAccion { get; set; }
    //}





    //public class UserGridItem
    //{
    //    public int UserId { get; set; }
    //    public decimal? ColaboradorId { get; set; }
    //    public int EstadoId { get; set; }
    //    public string UserName { get; set; }
    //    public string ColaboradorName { get; set; }
    //    public string EstadoName { get; set; }

    //}

    public class RoleToUserItem
    {
        public int RoleToUserId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string isSelected { get; set; }
    }

    
}
