﻿using System.Collections.Generic;
using System.Linq;
using Elecciones.Core.Model;
using System;

namespace Elecciones.Core.Repositories
{
    public interface IHorarioCursoRepository : IGenericRepository<HorarioCurso>
    {

        IEnumerable<HorarioCursoItem> GetHorarioCursoGrid(int codCargaCursoContratacion);

    }
    internal class HorarioCursoRepository: GenericRepository<EleccionesEntities, HorarioCurso>, IHorarioCursoRepository
    {
        public HorarioCursoRepository(EleccionesEntities ctx)
            : base(ctx)

        {
        }

        public IEnumerable<HorarioCursoItem> GetHorarioCursoGrid(int codCargaCursoContratacion)
        {

            var dc = Context;
            var query = from hc in dc.HorarioCursoes
                join ccc in dc.CargaCursoContratacions on hc.CodCargaCurso equals ccc.CodCargaCurso
                join s in dc.Salons on hc.CodSalon equals s.CodSalon
                        where ccc.CodCargaCurso == codCargaCursoContratacion
                select new HorarioCursoItem
                {
                   
                    HoraInicio=hc.HoraInicio,
                    HoraFin=hc.HoraFin,
                    Dia=hc.Dia,
                    Jornada=hc.Jornada,
                    CodCargaCurso=hc.CodCargaCurso,
                    CodSalon=s.CodSalon,
                    NombreSalon=s.NombreSalon
                    
                };

            return query;
        }
    }

    public class HorarioCursoItem
    {


       
        public TimeSpan? HoraInicio { get; set; }
        public TimeSpan? HoraFin { get; set; }
        
        public string Jornada { get; set; }
        public string Dia { get; set; }
        public int CodCargaCurso { get; set; }
        public int CodSalon { get; set; }
        public string NombreSalon { get; set; }

    }
}
