﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Repositories;
using AutentitacionProyect.Persistence;

namespace AutentitacionProyect.Repositories
{
    public interface ISalonRepository : IGenericRepository<Salon>
    {
        IEnumerable<SalonGridItem> GetSalonGridData();
    }
    internal class SalonRepository : GenericRepository<AcademicLoadDBEntities, Salon>, ISalonRepository
    {
        public SalonRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<SalonGridItem> GetSalonGridData()
        {
            var dc = Context;
            var query = from salon in dc.Salon
                join edificio in dc.Edificio on salon.EdificioId equals edificio.EdificioId
                select new SalonGridItem
                {
                    SalonId = salon.SalonId,
                    NombreSalon = salon.NombreSalon,
                    Capacidad = salon.Capacidad ?? 0,
                   // Nota = salon.N,
                    CodEdificio = edificio.EdificioId,
                    NombreEdificio = edificio.NombreEdificio,
                    PuedeSerBorrado = true,
                };

            return query;
        }
    }

    public class SalonGridItem
    {
        public decimal SalonId { get; set; }
        public string NombreSalon { get; set; }
        public decimal Capacidad { get; set; }
        public string Nota { get; set; }
        public decimal CodEdificio { get; set; }
        public string NombreEdificio { get; set; }
        public bool PuedeSerBorrado { get; set; }
    }
}
