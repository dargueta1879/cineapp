﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;

namespace AutentitacionProyect.Repositories
{
    public interface ICursoRepository : IGenericRepository<Curso>
    {
        IEnumerable<CursoGridItem> GetCursoGridData();
        IEnumerable<CursoGridItem> GetCursosSinDetallePensumGridData();
        //IEnumerable<CursoGridItem> GetCursosPorFacultad(decimal FacultadId);
        //IEnumerable<CarreraComboItem> GetCarreras(int facultadId);


    }
    internal class CursoRepository : GenericRepository<AcademicLoadDBEntities, Curso>, ICursoRepository
    {
        public CursoRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<CursoGridItem> GetCursoGridData()
        {
            var dc = Context;
            var query = from curso in dc.Curso
                //join facultad in dc.Facultad on curso.FacultadId equals facultad.FacultadId
                select new CursoGridItem
                {
                    CursoId = curso.CursoId,
                    NombreCurso = curso.NombreCurso,
                    NoCreditos = curso.NoCreditos ?? 0,
                    Laboratorio = curso.Laboratorio,
                    HorasSemanaMes = curso.HorasSemanaMes,
                    //FacultadId = curso.FacultadId,
                    PuedeSerBorrado = true,
                };
            return query;
        }

        public class CarreraComboItem
        {
            public decimal PensumId { get; set; }
            public string NombreCarrera { get; set; }
        }

        public IEnumerable<CarreraComboItem> GetCarreras(int facultadId)
        {
            var dc = Context;
            var query = from pensum in dc.Pensum
                        join carrera in dc.Carrera on pensum.CarreraId equals carrera.CarreraId
                        where carrera.FacultadId == facultadId
                        select new CarreraComboItem
                        {
                            PensumId = pensum.PensumId,
                            NombreCarrera = carrera.NombreCarrera,
                        };

            return query;
        }

    //    public IEnumerable<CursoGridItem> GetCursosPorFacultad(decimal item)
    //    {
    //        var dc = Context;
    //        var query = from fac in dc.Facultad
                       
    //                    //join cur in dc.CURSO on fac.FacultadId equals cur.FacultadId
    //                    where  fac.FacultadId==item
    //                    //orderby cur.CursoId
    //                    select new CursoGridItem {
        
    //                    CursoId=cur.CursoId,
    //                    NombreCurso=cur.NombreCurso,
    //                    NoCreditos=cur.NoCreditos??0,
    //                    HorasSemanaMes=cur.HorasSemanaMes,
    //                    FacultadId=cur.FacultadId,
    //                    PuedeSerBorrado=true
    //    };
    //        return query;
    //    }

        public IEnumerable<CursoGridItem> GetCursosSinDetallePensumGridData()
        {

            var dc = Context;
            var query = from cur in dc.Curso
                        join dpd in dc.DetallePensum on cur.CursoId equals dpd.CursoId
                        where cur.CursoId !=dpd.CursoId
                        orderby cur.NombreCurso

                        select new CursoGridItem
                        {
                            CursoId = cur.CursoId,
                            NombreCurso = cur.NombreCurso,
                            NoCreditos = cur.NoCreditos ?? 0,
                            Laboratorio = cur.Laboratorio,
                            HorasSemanaMes = cur.HorasSemanaMes,
                            //FacultadId = curso.FacultadId,
                        };

            return query;
        }




    }

    public class CursoGridItem
    {
        public decimal CursoId { get; set; }
        public string NombreCurso { get; set; }
        public decimal NoCreditos { get; set; }
        public string Laboratorio { get; set; }
        public Nullable<decimal> HorasSemanaMes { get; set; }
        public Nullable<decimal> FacultadId { get; set; }
        public bool PuedeSerBorrado { get; set; }
    }
}