﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;

namespace AutentitacionProyect.Repositories
{
    public interface IPensumRepository : IGenericRepository<Pensum>
    {
        IEnumerable<PensumGridItem> GetPensumGrid();
    }
    internal class PensumRepository : GenericRepository<AcademicLoadDBEntities, Pensum>, IPensumRepository
    {
        public PensumRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<PensumGridItem> GetPensumGrid()
        {

            var dc = Context;
            var query = from pensum in dc.Pensum
                join carrera in dc.Carrera on pensum.CarreraId equals carrera.CarreraId
                select new PensumGridItem
                {
                    PensumId = pensum.PensumId,
                    FechaVigencia = pensum.FechaVigencia,
                    FechaFinVigencia = pensum.FechaFinVigencia,
                    CarreraId = pensum.CarreraId,
                    NombreCarrera= carrera.NombreCarrera,
                    Observaciones= pensum.Observaciones,
                };

            return query;
        }
    }

    public class PensumGridItem
    {
        public decimal PensumId { get; set; }
        public Nullable<System.DateTime> FechaVigencia { get; set; }
        public Nullable<System.DateTime> FechaFinVigencia { get; set; }
        public decimal CarreraId { get; set; }
        public string NombreCarrera { get; set; }
        public string Observaciones { get; set; }
        
    }
}