﻿using Dorel.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
//using Dorel.Core.Repositories;

namespace CineApp.UnitOfWork.Repositories
{
    internal class GenericRepository<TDc, T> : IGenericRepository<T>
        where T : class
        where TDc : DbContext, new()
    {
        private TDc _entities;

        private IDbConnection Connection
        {
            get { return _entities.Database.Connection; }
        }

        protected internal TDc Context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        public GenericRepository() : this(new TDc())
        {
        }

        public GenericRepository(TDc context)
        {
            Context = context;
        }

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = _entities.Set<T>();
            return query;
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            var query = _entities.Set<T>().Where(predicate);
            return query;
        }

        public T FindOneBy(Expression<Func<T, bool>> predicate)
        {
            return _entities.Set<T>().SingleOrDefault(predicate);
        }

        public T GetById(object id)
        {
            return _entities.Set<T>().Find(id);
        }

        public virtual void Add(T entity)
        {
            _entities.Set<T>().Add(entity);
        }

        public virtual void Attach(T entity)
        {
            _entities.Set<T>().Attach(entity);
        }

        public virtual void Edit(T entity)
        {
            var entry = _entities.Entry(entity);
            _entities.Set<T>().Attach(entity);
            entry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            if (_entities.Entry(entity).State == EntityState.Detached)
            {
                _entities.Set<T>().Attach(entity);
            }
            _entities.Set<T>().Remove(entity);
        }

        //public virtual void Delete(IEnumerable<T> entities)
        //{
        //    _entities.Set<T>().RemoveRange(entities);
           
        //}

        public void BulkInsertAll(IEnumerable<T> elements)
        {
            var bulkHelper = new BulkInsertHelper(Connection);
            bulkHelper.BulkInsertAll(elements);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                Context.Dispose();
            }

            _disposed = true;
        }
    }
}