﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;
using AutentitacionProyect.Repositories;

namespace AutentitacionProyect.Repositories
{

    public interface IAsignacionRepository : IGenericRepository<AsignacionGridItem>
    {
    //    IEnumerable<AllCursoItem> GetAllCursos(int facultadId);
    //    IEnumerable<UserPermissionItem> GetFacultad();
        IEnumerable<AsignacionGridItem> GetAsignacionGridData(int carreraId, int cicloIdensumId);
    //    IEnumerable<CargaAcademicaItem> GetCargaAcademicaGridSource(int facultadId);
    //    IEnumerable<CargaCursoContratacionItem> GetCargaCursoContratacionGrid(int cargaAcademica);
    //    IEnumerable<DetalleCargaContratacionItem> GetSecondLevelData(int cargaAcademicaId);
    //    IEnumerable<HorarioExpandItem> GetThirdLevelData(int cargaCursoContratacionId);
    //    IEnumerable<CatedraticoItem> GetCatedratico(int facultadId);
    //    IEnumerable<HorarioExportItem> GetExportThirdLevelData(int cargaCursoContratacionId);
    }
    internal class AsignacionRepository : GenericRepository<AcademicLoadDBEntities, AsignacionGridItem>, IAsignacionRepository
    {
        public AsignacionRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }



        public IEnumerable<AsignacionGridItem> GetAsignacionGridData(int carreraId, int cicloId)
        {
            var dc = Context;
            var query = from pensum in dc.Pensum
                        join detallePensum in dc.DetallePensum on pensum.PensumId equals detallePensum.PensumId
                        join curso in dc.Curso on detallePensum.CursoId equals curso.CursoId
                        where pensum.CarreraId == carreraId && detallePensum.Ciclo == cicloId
                        select new AsignacionGridItem
                        {
                            DetallePensumId = detallePensum.DetallePensumId,
                            PensumId = pensum.PensumId,
                            CicloId = detallePensum.Ciclo,
                            CursoId = curso.CursoId,
                            NombreCurso = curso.NombreCurso,
                            Estado = detallePensum.Estado,
                            Obligatorio = detallePensum.Obligatorio,
                            NoCreditosPrerrequisito = detallePensum.NoCreditosPrerrequisito,
                            CanBeDeleted = false,
                        };

            return query;
        }


    }

   
   public class AsignacionGridItem
    {
        public decimal DetallePensumId { get; set; }
        public decimal PensumId { get; set; }
        public decimal? CicloId { get; set; }
        public decimal CursoId { get; set; }
        public string NombreCurso { get; set; }
        public string Estado { get; set; }
        public string Obligatorio { get; set; }
        public decimal? NoCreditosPrerrequisito { get; set; }
        public bool CanBeDeleted { get; set; }
    }

   
}
