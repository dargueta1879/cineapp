﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IFacultadRepository : IGenericRepository<Facultad>
    {
        IEnumerable<FacultadGridItem> GetFacultadGridData();
    }
    internal class FacultadRepository : GenericRepository<AcademicLoadDBEntities, Facultad>, IFacultadRepository
    {
        public FacultadRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<FacultadGridItem> GetFacultadGridData()
        {
            var dc = Context;
            var query = from facultad in dc.Facultad
                        
                        select new FacultadGridItem
                        {
                            FacultadId =facultad.FacultadId ,
                            NombreFacultad = facultad.NombreFacultad,
                            Partida = facultad.Partida,
                            PuedeSerBorrado = true,
                        };

            return query;
        }
    }

    public class FacultadGridItem
    {
        public decimal FacultadId { get; set; }
        public string NombreFacultad { get; set; }
        public string Partida { get; set; }
        public bool PuedeSerBorrado { get; set; }
        
    }
}
