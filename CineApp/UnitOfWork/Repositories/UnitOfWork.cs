﻿using System;
using System.Data.Entity;
//using Elecciones.Core.Helper;
//using AutentitacionProyect.Persistence;
using CineApp.UnitOfWork.Repositories;
using CineApp.Helper;
using CineApp.Persistence;


namespace CineApp.UnitOfWork.Repositories
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly DBCineEntities _context = new DBCineEntities();

        public UnitOfWork()
            : this(new DBCineEntities())
        {
        }

        internal UnitOfWork(DBCineEntities context)
        {
            _context = context;
        }

        public UnitOfWork(string connectionString)
            : this(new DBCineEntities())
        {
            _context.Database.Connection.ConnectionString = connectionString;
        }

        public DbContext Context
        {
            get
            {
                return _context;
            }
        }

        //Specific repositories
        private ICarteleraRepository _carteleraRepository;

        public ICarteleraRepository CarteleraRepository
        {
            get { return _carteleraRepository ?? (_carteleraRepository = new CarteleraRepository(_context)); }
        }


        private ITicketRepository _ticketRepository;

        public ITicketRepository TicketRepository
        {
            get { return _ticketRepository ?? (_ticketRepository = new TicketRepository(_context)); }
        }

        private ISocioRepository _socioRepository;

        public ISocioRepository SocioRepository
        {
            get { return _socioRepository ?? (_socioRepository = new SocioRepository(_context)); }
        }

        private IEmpleadoRepository _empleadoRepository;

        public IEmpleadoRepository EmpleadoRepository
        {
            get { return _empleadoRepository ?? (_empleadoRepository = new EmpleadoRepository(_context)); }
        }




        //private IFacultadRepository _facultadRepository;

        //public IFacultadRepository FacultadRepository
        //{
        //    get { return _facultadRepository ?? (_facultadRepository = new FacultadRepository(_context)); }
        //}

        //private IAccesosRepository _accesosRepository;

        //public IAccesosRepository AccesosRepository
        //{
        //    get { return _accesosRepository ?? (_accesosRepository = new AccesosRepository(_context)); }
        //}

        //private IAccesoRolRepository _accesorolRepository;

        //public IAccesoRolRepository AccesoRolRepository
        //{
        //    get { return _accesorolRepository ?? (_accesorolRepository = new AccesoRolRepository(_context)); }
        //}

        //private IColaboradorRepository _colaboradorRepository;

        //public IColaboradorRepository ColaboradorRepository
        //{
        //    get { return _colaboradorRepository ?? (_colaboradorRepository = new ColaboradorRepository(_context)); }
        //}

        //private IUserProfileRepository _userprofileRepository;

        //public IUserProfileRepository UserProfileRepository
        //{
        //    get { return _userprofileRepository ?? (_userprofileRepository = new UserProfileRepository(_context)); }
        //}

        //private IRoleToUserRepository _roletouserRepository;


        //public IRoleToUserRepository RoleToUserRepository
        //{
        //    get { return _roletouserRepository ?? (_roletouserRepository = new RoleToUserRepository(_context)); }
        //}


        //private ICarreraRepository _carreraRepository;

        //public ICarreraRepository CarreraRepository
        //{
        //    get { return _carreraRepository ?? (_carreraRepository = new CarreraRepository(_context)); }
        //}

        //private IDetalleCargaAcademicaRepository _detallecargaacademicaRepository;

        //public IDetalleCargaAcademicaRepository DetalleCargaAcademicaRepository
        //{
        //    get { return _detallecargaacademicaRepository ?? (_detallecargaacademicaRepository = new DetalleCargaAcademicaRepository(_context)); }
        //}

        //private ICargaAcademicaRepository _cargaacademicaRepository;

        //public ICargaAcademicaRepository CargaAcademicaRepository
        //{
        //    get { return _cargaacademicaRepository ?? (_cargaacademicaRepository = new CargaAcademicaRepository(_context)); }
        //}

        //private IContratoRepository _contratoRepository;

        //public IContratoRepository ContratoRepository
        //{
        //    get { return _contratoRepository ?? (_contratoRepository = new ContratoRepository(_context)); }
        //}








        //private IPensumRepository _pensumRepository;

        //public IPensumRepository PensumRepository
        //{
        //    get { return _pensumRepository ?? (_pensumRepository = new PensumRepository(_context)); }
        //}

        //private ICursoRepository _cursoRepository;

        //public ICursoRepository CursoRepository
        //{
        //    get { return _cursoRepository ?? (_cursoRepository = new CursoRepository(_context)); }
        //}

        //private IAsignacionRepository _asignacionRepository;

        //public IAsignacionRepository AsignacionRepository
        //{
        //    get { return _asignacionRepository ?? (_asignacionRepository = new AsignacionRepository(_context)); }
        //}

        //private IPrerrequisitoRepository _prerrequisitoRepository;

        //public IPrerrequisitoRepository PrerrequisitoRepository
        //{

        //    get { return _prerrequisitoRepository ?? (_prerrequisitoRepository = new PrerrequisitoRepository(_context)); }
        //}


        //private IDetallePensumRepository _detallepensumRepository;

        //public IDetallePensumRepository DetallePensumRepository
        //{


        //    get { return _detallepensumRepository ?? (_detallepensumRepository = new DetallePensumRepository(_context)); }
        //}


        ////private ICargaCursoContratacionRepository _cargaCursoContratacionRepository;
        ////public ICargaCursoContratacionRepository CargaCursoContratacionRepository
        ////{
        ////    get { return _cargaCursoContratacionRepository ?? (_cargaCursoContratacionRepository = new CargaCursoContratacionRepository(_context)); }
        ////}               

        ////private IHorarioCursoRepository _horarioCursoRepository;
        ////public IHorarioCursoRepository HorarioCursoRepository
        ////{
        ////    get { return _horarioCursoRepository ?? (_horarioCursoRepository = new HorarioCursoRepository(_context)); }
        ////}


        ////private IHorarioContratacionRepository _horarioContratacionRepository;
        ////public IHorarioContratacionRepository HorarioContratacionRepository
        ////{
        ////    get { return _horarioContratacionRepository ?? (_horarioContratacionRepository = new HorarioContratacionRepository(_context)); }
        ////}


        ////private IAsignacionPuestoRepository _asignacionPuestoRepository;
        ////public IAsignacionPuestoRepository AsignacionPuestoRepository
        ////{
        ////    get { return _asignacionPuestoRepository ?? (_asignacionPuestoRepository = new AsignacionPuestoRepository(_context)); }
        ////}

        ////private IAsignacionCatedraticoFacultadRepository _asignacionCatedraticoFacultadRepository;
        ////public IAsignacionCatedraticoFacultadRepository AsignacionCatedraticoFacultadRepository
        ////{

        ////    get { return _asignacionCatedraticoFacultadRepository ?? (_asignacionCatedraticoFacultadRepository = new AsignacionCatedraticoFacultadRepository(_context)); }
        ////}

        ////private ICatedraticoRepository _catedraticoRepository;
        ////public ICatedraticoRepository CatedraticoRepository
        ////{


        ////    get { return _catedraticoRepository ?? (_catedraticoRepository = new CatedraticoRepository(_context)); }
        ////}

        ////example
        ////private ITableNameRepository _tableNameRepository;
        ////public ITableNameRepository TableNameRepository
        ////{
        ////    get { return _tableNameRepository ?? (_tableNameRepository = new TableNameRepository(_context)); }
        ////}


        ////Generic repositories

        private IGenericRepository<Pelicula> _peliculaRepository;

        public IGenericRepository<Pelicula> PeliculaRepository
        {
            get
            {
                return _peliculaRepository ?? (_peliculaRepository = new GenericRepository<DBCineEntities, Pelicula>(_context));
            }
        }

        private IGenericRepository<Cine> _cineRepository;

        public IGenericRepository<Cine> CineRepository
        {
            get
            {
                return _cineRepository ?? (_cineRepository = new GenericRepository<DBCineEntities, Cine>(_context));
            }
        }

        private IGenericRepository<Sala> _salaRepository;

        public IGenericRepository<Sala> SalaRepository
        {
            get
            {
                return _salaRepository ?? (_salaRepository = new GenericRepository<DBCineEntities, Sala>(_context));
            }
        }

        private IGenericRepository<EstadoCartelera> _estadocarteleraRepository;
        public IGenericRepository<EstadoCartelera> EstadoCarteleraRepository
        {
            get
            {
                return _estadocarteleraRepository ?? (_estadocarteleraRepository = new GenericRepository<DBCineEntities, EstadoCartelera>(_context));
            }
        }


         private IGenericRepository<Horario> _horarioRepository;
         public IGenericRepository<Horario> HorarioRepository
         {
             get
             {
                 return _horarioRepository ?? (_horarioRepository = new GenericRepository<DBCineEntities, Horario>(_context));
             }
         }

         private IGenericRepository<DetalleTicket> _detalleTicketRepository;
         public IGenericRepository<DetalleTicket> DetalleTicketRepository
         {
             get
             {
                 return _detalleTicketRepository ?? (_detalleTicketRepository = new GenericRepository<DBCineEntities, DetalleTicket>(_context));
             }
         }


         private IGenericRepository<Asociado> _asociadoRepository;
         public IGenericRepository<Asociado> AsociadoRepository
         {
             get
             {
                 return _asociadoRepository ?? (_asociadoRepository = new GenericRepository<DBCineEntities, Asociado>(_context));
             }
         }

         private IGenericRepository<TipoEmpleado> _tipoempleadoRepository;
         public IGenericRepository<TipoEmpleado> TipoEmpleadoRepository
         {
             get
             {
                 return _tipoempleadoRepository ?? (_tipoempleadoRepository = new GenericRepository<DBCineEntities, TipoEmpleado>(_context));
             }
         }

         private IGenericRepository<Planilla> _planillaRepository;
         public IGenericRepository<Planilla> PlanillaRepository
         {
             get
             {
                 return _planillaRepository ?? (_planillaRepository = new GenericRepository<DBCineEntities, Planilla>(_context));
             }
         }



        
        


      


        //private IGenericRepository<Edificio> _edificioRepository;

        //public IGenericRepository<Edificio> EdificioRepository
        //{
        //    get
        //    {
        //        return _edificioRepository ?? (_edificioRepository = new GenericRepository<AcademicLoadDBEntities, Edificio>(_context));
        //    }
        //}

        //private IGenericRepository<webpages_Roles> _webpages_RolesRepository;

        //public IGenericRepository<webpages_Roles> webpages_RolesRepository
        //{
        //    get
        //    {
        //        return _webpages_RolesRepository ?? (_webpages_RolesRepository = new GenericRepository<AcademicLoadDBEntities, webpages_Roles>(_context));
        //    }
        //}

        //private IGenericRepository<HorarioFirma> _horarioFirmaRepository;

        //public IGenericRepository<HorarioFirma> HorarioFirmaRepository
        //{
        //    get
        //    {
        //        return _horarioFirmaRepository ?? (_horarioFirmaRepository = new GenericRepository<AcademicLoadDBEntities, HorarioFirma>(_context));
        //    }
        //}

        //private IGenericRepository<HorarioCurso> _horarioCursoRepository;

        //public IGenericRepository<HorarioCurso> HorarioCursoRepository
        //{
        //    get
        //    {
        //        return _horarioCursoRepository ?? (_horarioCursoRepository = new GenericRepository<AcademicLoadDBEntities, HorarioCurso>(_context));
        //    }
        //}




        //private IGenericRepository<Puesto> _puestoRepository;

        //public IGenericRepository<Puesto> puestoRepository
        //{
        //    get
        //    {
        //        return _puestoRepository ?? (_puestoRepository = new GenericRepository<AcademicLoadDBEntities, Puesto>(_context));
        //    }
        //}



        //private IGenericRepository<Trabajador> _trabajadorRepository;

        //public IGenericRepository<Trabajador> TrabajadorRepository
        //{
        //    get
        //    {
        //        return _trabajadorRepository ??  (_trabajadorRepository = new GenericRepository<EleccionesEntities, Trabajador>(_context));
        //    }
        //}

        //private IGenericRepository<TipoTitulo> _tipotituloRepository;

        //public IGenericRepository<TipoTitulo> TipoTituloRepository
        //{
        //    get
        //    {
        //        return _tipotituloRepository ?? (_tipotituloRepository = new GenericRepository<EleccionesEntities, TipoTitulo>(_context));
        //    }
        //}


        //private IGenericRepository<CargaAcademica> _cargaacademicaRepository;


        //public IGenericRepository<CargaAcademica> CargaAcademicaRepository
        //{

        //    get
        //    {
        //        return _cargaacademicaRepository ?? (_cargaacademicaRepository = new GenericRepository<EleccionesEntities, CargaAcademica>(_context));
        //    }
        //}

        //private IGenericRepository<Puesto> _puestoRepository;

        //public IGenericRepository<Puesto> PuestoRepository
        //{
        //    get
        //    {
        //        return _puestoRepository ?? (_puestoRepository = new GenericRepository<EleccionesEntities, Puesto>(_context));
        //    }
        //}


        //example
        //private IGenericRepository<TableName> _tableNameRepository;
        //public IGenericRepository<TableName> TableNameRepository
        //{
        //    get
        //    {
        //        return _tableNameRepository ??
        //               (_tableNameRepository = new GenericRepository<EleccionesEntities, TableName>(_context));
        //    }
        //}


        //Methods
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int ExecuteCommand(string sql, params object[] parameters)
        {
            return _context.Database.ExecuteSqlCommand(sql, parameters);
        }
    }
}
