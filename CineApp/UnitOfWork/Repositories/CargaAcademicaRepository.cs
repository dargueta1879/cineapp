﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Repositories;
using AutentitacionProyect.Persistence;
using System;

namespace AutentitacionProyect.Repositories
{
    public interface ICargaAcademicaRepository : IGenericRepository<CargaAcademica>
    {
        IEnumerable<CargaAcademicaGridItem> GetCargasAcademicas();
        IEnumerable<HorarioCargaAcademicaItem> GetHorarioCargaAcademica(decimal cargaAcademicaId);
        IEnumerable<CargaAcademicaGridItem> GetCargasAcademicasInJson();
    }
    internal class CargaAcademicaRepository : GenericRepository<AcademicLoadDBEntities, CargaAcademica>, ICargaAcademicaRepository
    {
        public CargaAcademicaRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<CargaAcademicaGridItem> GetCargasAcademicas()
        {
            var dc = Context;
            var query = from ca in dc.CargaAcademica
                join fac in dc.Facultad on ca.FacultadId equals fac.FacultadId
                select new CargaAcademicaGridItem
                {
                    CargaAcademicaId=ca.CargaAcademicaId,
                    FacultadId=fac.FacultadId,
                    NombreFacultad=fac.NombreFacultad,
                    Ciclo=ca.CicloCargaAcademica??0,
                    NombreCargaAcademica=ca.NombreCargaAcademica,
                    FechaInicio=ca.FechaInicio,
                    FechaFin = ca.FechaFin
                };

            return query;
        }

        public IEnumerable<CargaAcademicaGridItem> GetCargasAcademicasInJson()
        {
            var dc = Context;
            var query = from ca in dc.CargaAcademica
                        join fac in dc.Facultad on ca.FacultadId equals fac.FacultadId
                        select new CargaAcademicaGridItem
                        {
                            CargaAcademicaId = ca.CargaAcademicaId,
                            FacultadId = fac.FacultadId,
                            NombreFacultad = fac.NombreFacultad,
                            Ciclo = ca.CicloCargaAcademica??0,
                            NombreCargaAcademica = ca.NombreCargaAcademica,
                            FechaInicio = ca.FechaInicio.Value.Date,
                            FechaFin = ca.FechaFin.Value.Date
                        };

            return query;
        }


        public IEnumerable<HorarioCargaAcademicaItem> GetHorarioCargaAcademica(decimal cargaAcademicaId)
        {
            var dc = Context;
            var query = from hc in dc.HorarioCurso
                        join dca in dc.DetalleCargaAcademica on hc.DetalleCargaAcademicaId equals dca.DetalleCargaAcademicaId
                        join ca in dc.CargaAcademica on dca.CargaAcademicaId equals ca.CargaAcademicaId
                        join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
                        join cur in dc.Curso on dp.CursoId equals cur.CursoId
                        join p in dc.Pensum on dp.PensumId equals p.PensumId
                        join car in dc.Carrera on p.CarreraId equals car.CarreraId
                        join s in dc.Salon on hc.SalonId equals s.SalonId


                        where ca.CargaAcademicaId == cargaAcademicaId
                        orderby dp.Ciclo,car.NombreCarrera,hc.Dia

                        select new HorarioCargaAcademicaItem
                        {

                            Dia = hc.Dia,
                            HoraInicio = hc.HoraInicio,
                            HoraFin = hc.HoraFin,

                            Jornada = hc.Jornada,
                            SalonId = hc.SalonId,
                            DetalleCargaAcademicaId = dca.CargaAcademicaId,
                            DetallePensumId = dp.DetallePensumId,
                            CargaAcademicaId = ca.CargaAcademicaId,
                            NombreCurso = cur.NombreCurso,

                            NombreCarrera = car.NombreCarrera,
                            Ciclo = dp.Ciclo ?? 0,
                            NombreSalon = s.NombreSalon,
                            CursoId = cur.CursoId
                        };

            return query;

        }
    }

    public class CargaAcademicaGridItem
    {
        public decimal CargaAcademicaId { get; set; }
        public decimal FacultadId { get; set; }
        public string NombreFacultad { get; set; }
        public decimal Ciclo { get; set; }
        public string NombreCargaAcademica { get; set; }
       public string NombreEdificio { get; set; }
        public  System.DateTime? FechaInicio { get; set; }
        public System.DateTime?  FechaFin { get; set; }

    }

   

    public class HorarioCargaAcademicaItem
    {

        public string Dia { get; set; }
        public Nullable<System.TimeSpan> HoraInicio { get; set; }
        public Nullable<System.TimeSpan> HoraFin { get; set; }
        public decimal CarreraId { get; set; }
        public string Jornada { get; set; }
        public decimal SalonId { get; set; }
        public decimal DetalleCargaAcademicaId { get; set; }
        public decimal DetallePensumId { get; set; }
        public decimal CargaAcademicaId { get; set; }
        public string NombreCurso { get; set; }
        public string NombreSalon { get; set; }
        public string NombreCarrera { get; set; }
        public decimal Ciclo { get; set; }
        public decimal CursoId { get; set; }


    }
}
