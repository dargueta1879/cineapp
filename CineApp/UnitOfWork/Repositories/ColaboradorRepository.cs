﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IColaboradorRepository : IGenericRepository<Colaborador>
    {
        IEnumerable<ColaboradorGridItem> GetColaboradorGridData();
        IEnumerable<ColaboradorItem> GetColaborador(int id);
        IEnumerable<ColaboradorGridItem> GetColaboradorSinUsuarioGridData();
    }
    internal class ColaboradorRepository : GenericRepository<AcademicLoadDBEntities, Colaborador>, IColaboradorRepository
    {
        public ColaboradorRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

       

        public IEnumerable<ColaboradorGridItem> GetColaboradorGridData()
        {
            var dc = Context;
            var query = from col in dc.Colaborador
                        join est in dc.EstadoUsuario on col.EstadoUsuarioId equals est.EstadoUsuarioId
                        into JoinedEmpDept from dept in JoinedEmpDept.DefaultIfEmpty()
                        join usr in dc.UserProfile on col.UserId equals usr.UserId
                        into JoinedEmpDept2 from dept2 in JoinedEmpDept2.DefaultIfEmpty()
                        orderby col.PrimerNombre, col.SegundoNombre, col.PrimerApellido, col.SegundoApellido
                       


                        select new ColaboradorGridItem
                        {
                            ColaboradorId = col.ColaboradorId,
                            CarneUniversitarioId = col.CarneUniversitarioId,
                            NoColegiado = col.NoColegiado,
                            Dpi = col.Dpi,
                            PrimerNombre = col.PrimerNombre,
                            SegundoNombre = col.SegundoNombre,
                            PrimerApellido = col.PrimerApellido,
                            SegundoApellido = col.SegundoApellido,
                            FechaNacimiento = col.FechaNacimiento,
                            NumTelefono = col.NumTelefono,
                            Direccion = col.Direccion,
                            Municipio = col.Municipio,
                            Departamento = col.Departamento,
                            Correo = col.Correo,
                            EstadoUsuarioId = col.EstadoUsuarioId,
                            UserId = col.UserId,
                            NombreUsuario=dept2.UserName,
                            NombreEstadoUsuario=dept.NombreEstadoUsuario,
                            NombreCompleto=col.PrimerNombre +" "+col.PrimerApellido+" "+col.SegundoApellido
                        };

            return query;
        }

        public IEnumerable<ColaboradorGridItem> GetColaboradorSinUsuarioGridData()
        {

            var dc = Context;
            var query = from col in dc.Colaborador
                        where col.UserId == null
                        orderby col.PrimerNombre, col.SegundoNombre, col.PrimerApellido, col.SegundoApellido
                        



                        select new ColaboradorGridItem
                        {
                            ColaboradorId = col.ColaboradorId,
                            CarneUniversitarioId = col.CarneUniversitarioId,
                            NoColegiado = col.NoColegiado,
                            Dpi = col.Dpi,
                            PrimerNombre = col.PrimerNombre,
                            SegundoNombre = col.SegundoNombre,
                            PrimerApellido = col.PrimerApellido,
                            SegundoApellido = col.SegundoApellido,
                            FechaNacimiento = col.FechaNacimiento,
                            NumTelefono = col.NumTelefono,
                            Direccion = col.Direccion,
                            Municipio = col.Municipio,
                            Departamento = col.Departamento,
                            Correo = col.Correo,
                            EstadoUsuarioId = col.EstadoUsuarioId,
                            
                        };

            return query;
        }

        public IEnumerable<ColaboradorItem> GetColaborador(int id)
        {
            var dc = Context;
            var query = from col in dc.Colaborador
                        join est in dc.EstadoUsuario on col.EstadoUsuarioId equals est.EstadoUsuarioId
                        into JoinedEmpDept
                        from dept in JoinedEmpDept.DefaultIfEmpty()
                        join usr in dc.UserProfile on col.UserId equals usr.UserId
                        into JoinedEmpDept2
                        from dept2 in JoinedEmpDept2.DefaultIfEmpty()
                        where col.ColaboradorId==id



                        select new ColaboradorItem
                        {
                            ColaboradorId = col.ColaboradorId,
                            CarneUniversitarioId = col.CarneUniversitarioId,
                            NoColegiado = col.NoColegiado,
                            Dpi = col.Dpi,
                            PrimerNombre = col.PrimerNombre,
                            SegundoNombre = col.SegundoNombre,
                            PrimerApellido = col.PrimerApellido,
                            SegundoApellido = col.SegundoApellido,
                            FechaNacimiento = col.FechaNacimiento,
                            NumTelefono = col.NumTelefono,
                            Direccion = col.Direccion,
                            Municipio = col.Municipio,
                            Departamento = col.Departamento,
                            Correo = col.Correo,
                            EstadoUsuarioId = col.EstadoUsuarioId,
                            UserId = col.UserId,
                            NombreUsuario = dept2.UserName,
                            NombreEstadoUsuario = dept.NombreEstadoUsuario
                        };

            return query;
        }


    }

    public class ColaboradorGridItem
    {



        public decimal ColaboradorId { get; set; }
        public string CarneUniversitarioId { get; set; }
        public string NoColegiado { get; set; }
        public string Dpi { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string NumTelefono { get; set; }
        public string Direccion { get; set; }
        public string Municipio { get; set; }
        public string Departamento { get; set; }
        public string Correo { get; set; }
        public int? EstadoUsuarioId { get; set; }
        public int? UserId { get; set; }
        public string NombreEstadoUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreCompleto { get; set; }


    }

    public class ColaboradorItem
    {



        public decimal ColaboradorId { get; set; }
        public string CarneUniversitarioId { get; set; }
        public string NoColegiado { get; set; }
        public string Dpi { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string NumTelefono { get; set; }
        public string Direccion { get; set; }
        public string Municipio { get; set; }
        public string Departamento { get; set; }
        public string Correo { get; set; }
        public int? EstadoUsuarioId { get; set; }
        public int? UserId { get; set; }
        public string NombreEstadoUsuario { get; set; }
        public string NombreUsuario { get; set; }

    }
}
