﻿using System.Collections.Generic;
using System.Linq;
using Elecciones.Core.Model;

namespace Elecciones.Core.Repositories
{
    public interface IAsignacionPuestoRepository : IGenericRepository<AsignacionPuesto>
    {
        IEnumerable<AsignacionPuestoItem> GetPuestoGridData(int item);

    }
    internal class AsignacionPuestoRepository: GenericRepository<EleccionesEntities, AsignacionPuesto>, IAsignacionPuestoRepository
    {
        public AsignacionPuestoRepository(EleccionesEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<AsignacionPuestoItem> GetPuestoGridData(int item)
        {
            var dc = Context;
            var query = from asig in dc.AsignacionPuestoes
                join tipopuesto in dc.Puestoes on asig.CodPuesto equals tipopuesto.CodPuesto
                join facultad in dc.Facultads on asig.CodFacultad equals facultad.CodFacultad
                where item == asig.IdUserAccounts
                select new AsignacionPuestoItem
                {
                    CodAsignacionPuesto=asig.CodPuesto,
                    NombreTipoPuesto=tipopuesto.NombreTipoPuesto,
                    NombreFacultad=facultad.NombreFacultad,
                    CodFacultad=facultad.CodFacultad,
                    CodPuesto=tipopuesto.CodPuesto,
                    UserId=asig.IdUserAccounts



                };

            return query;
        }
    }

    public class AsignacionPuestoItem
    {
        public int? CodAsignacionPuesto { get; set; }
        public int CodFacultad { get; set; }
        public string NombreFacultad { get; set; }
        public int CodPuesto { get; set; }
        public string NombreTipoPuesto { get; set; }
        public int? UserId { get; set; }
        public bool PuedeSerBorrado { get; set; }

    }
}
