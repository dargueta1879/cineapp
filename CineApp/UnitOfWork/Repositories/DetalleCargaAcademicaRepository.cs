﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IDetalleCargaAcademicaRepository : IGenericRepository<DetalleCargaAcademica>
    {
        IEnumerable<DetalleCargaAcademicaItem> GetDetalleCargaAcademicaGridDataByIdCargaAcademica(decimal CargaAcademicaId);
        IEnumerable<DetalleCargaAcademicaItem> GetDetalleCargaAcademicaSinAsignarByIdCargaAcademica(decimal CargaAcademicaId);
        IEnumerable<HorarioCursoTraslapadoItem> GetHorarioCursoTraslapado(HorarioCursoTraslapadoItem hcti);
        IEnumerable<HorarioCursoItem> GetHorarioGridByDetalleCargaAcademicaId(decimal DetalleCargaAcademicaId);
        DetalleCargaAcademicaItem getDetalleCargaAcademicaByDetalleCargaAcademicaId(decimal id);

    }
    internal class DetalleCargaAcademicaRepository : GenericRepository<AcademicLoadDBEntities, DetalleCargaAcademica>, IDetalleCargaAcademicaRepository
    {
        public DetalleCargaAcademicaRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<DetalleCargaAcademicaItem> GetDetalleCargaAcademicaGridDataByIdCargaAcademica(decimal CargaAcademicaId)
        {
            var dc = Context;
            var query = from dca in dc.DetalleCargaAcademica
                        join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
                        join c in dc.Curso on dp.CursoId equals c.CursoId
                        join ec in dc.EstadoCargaAcademica on dca.EstadoCargaId equals ec.EstadoCargaId
                        join p in dc.Pensum on dp.PensumId equals p.PensumId
                        join col in dc.Colaborador on dca.ColaboradorId equals col.ColaboradorId
                        join car in dc.Carrera on p.CarreraId equals car.CarreraId
                        where dca.CargaAcademicaId == CargaAcademicaId
                        orderby p.CarreraId, dp.Ciclo

                        select new DetalleCargaAcademicaItem
                        {
                            DetalleCargaAcademicaId = dca.DetalleCargaAcademicaId,
                            CargaAcademicaId = dca.CargaAcademicaId,
                            DetallePensumId = dca.DetallePensumId,
                            HorasCursoMensual = dca.HorasCursoMensual ?? 0,
                            EstadoCargaId = dca.EstadoCargaId,
                            ColaboradorId = dca.ColaboradorId,
                            ContratoId = dca.ContratoId ?? 0,
                            NombreCurso = c.NombreCurso,
                            NombreEstado = ec.NombreEstadoCargaAcademica,
                            NombreColaborador = col.PrimerNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
                            PuedeSerBorrado = true,
                            CodCurso = c.CodCurso,
                            CursoId = c.CursoId,
                            Ciclo = dp.Ciclo ?? 0,
                            NombreCarrera = car.NombreCarrera
                        };

            return query;
        }




        public IEnumerable<DetalleCargaAcademicaItem> GetDetalleCargaAcademicaSinAsignarByIdCargaAcademica(decimal CargaAcademicaId)
        {
            var dc = Context;
            var query = from dca in dc.DetalleCargaAcademica
                        join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
                        join c in dc.Curso on dp.CursoId equals c.CursoId
                        join ec in dc.EstadoCargaAcademica on dca.EstadoCargaId equals ec.EstadoCargaId
                        join p in dc.Pensum on dp.PensumId equals p.PensumId
                        join col in dc.Colaborador on dca.ColaboradorId equals col.ColaboradorId
                        join car in dc.Carrera on p.CarreraId equals car.CarreraId
                        join fac in dc.Facultad on car.FacultadId equals fac.FacultadId

                        where dca.CargaAcademicaId == CargaAcademicaId && ec.NombreEstadoCargaAcademica != "Asignado"
                        orderby p.CarreraId, dp.Ciclo

                        select new DetalleCargaAcademicaItem
                        {
                            DetalleCargaAcademicaId = dca.DetalleCargaAcademicaId,
                            NombreFacultad=fac.NombreFacultad,
                            CargaAcademicaId = dca.CargaAcademicaId,
                            DetallePensumId = dca.DetallePensumId,
                            HorasCursoMensual = dca.HorasCursoMensual ?? 0,
                            EstadoCargaId = dca.EstadoCargaId,
                            ColaboradorId = dca.ColaboradorId,
                            ContratoId = dca.ContratoId ?? 0,
                            NombreCurso = c.NombreCurso,
                            NombreEstado = ec.NombreEstadoCargaAcademica,
                            NombreColaborador = col.PrimerNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
                            PuedeSerBorrado = true,
                            CodCurso = c.CodCurso,
                            Ciclo = dp.Ciclo ?? 0,
                            NombreCarrera = car.NombreCarrera
                        };

            return query;
        }

        public IEnumerable<HorarioCursoTraslapadoItem> GetHorarioCursoTraslapado(HorarioCursoTraslapadoItem hcti)
        {
            var dc = Context;
            var query = from hc in dc.HorarioCurso
                        join dca in dc.DetalleCargaAcademica on hc.DetalleCargaAcademicaId equals dca.DetalleCargaAcademicaId
                        join ca in dc.CargaAcademica on dca.CargaAcademicaId equals ca.CargaAcademicaId
                        join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
                        join cur in dc.Curso on dp.CursoId equals cur.CursoId
                        join p in dc.Pensum on dp.PensumId equals p.PensumId
                        join car in dc.Carrera on p.CarreraId equals car.CarreraId
                        join s in dc.Salon on hc.SalonId equals s.SalonId


                        where ((hc.HoraInicio < hcti.HoraFin && hc.HoraFin > hcti.HoraInicio) || (hc.HoraInicio == hcti.HoraInicio && hc.HoraFin == hcti.HoraFin)) && hc.Dia == hcti.Dia && ca.CargaAcademicaId == hcti.CargaAcademicaId && car.CarreraId==hcti.CarreraId && dp.Ciclo==hcti.Ciclo

                        select new HorarioCursoTraslapadoItem
                        {
                       
                            Dia=hc.Dia,
                            HoraInicio=hc.HoraInicio,
                             HoraFin =hc.HoraFin,
                            
                            Jornada=hc.Jornada, 
                           SalonId=hc.SalonId,
                            DetalleCargaAcademicaId=dca.CargaAcademicaId,
                            DetallePensumId=dp.DetallePensumId,
                            CargaAcademicaId=ca.CargaAcademicaId,
                            NombreCurso=cur.NombreCurso,
                            CarreraId=hcti.CarreraId,
                            NombreCarrera=car.NombreCarrera,
                            Ciclo=dp.Ciclo??0,
                            NombreSalon=s.NombreSalon,
                            CursoId=cur.CursoId
                        };

            return query;

        }


        //public IEnumerable<HorarioCursoTraslapadoItem> GetSalonOcupad(HorarioCursoTraslapadoItem hcti)
        //{
        //    var dc = Context;
        //    var query = from hc in dc.HorarioCurso
        //                join dca in dc.DetalleCargaAcademica on hc.DetalleCargaAcademicaId equals dca.DetalleCargaAcademicaId
        //                join ca in dc.CargaAcademica on dca.CargaAcademicaId equals ca.CargaAcademicaId
        //                join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
        //                join cur in dc.Curso on dp.CursoId equals cur.CursoId
        //                join p in dc.Pensum on dp.PensumId equals p.PensumId
        //                join car in dc.Carrera on p.CarreraId equals car.CarreraId
        //                join s in dc.Salon on hc.SalonId equals s.SalonId


        //                where ((hc.HoraInicio < hcti.HoraFin && hc.HoraFin > hcti.HoraInicio) || (hc.HoraInicio == hcti.HoraInicio && hc.HoraFin == hcti.HoraFin)) && hc.Dia == hcti.Dia && (ca.CargaAcademicaId == hcti.CargaAcademicaId) && ((ca.F < hcti.HoraFin && hc.HoraFin > hcti.HoraInicio) || (hc.HoraInicio == hcti.HoraInicio && hc.HoraFin == hcti.HoraFin))

        //                select new HorarioCursoTraslapadoItem
        //                {
                       
        //                    Dia=hc.Dia,
        //                    HoraInicio=hc.HoraInicio,
        //                     HoraFin =hc.HoraFin,
                            
        //                    Jornada=hc.Jornada, 
        //                   SalonId=hc.SalonId,
        //                    DetalleCargaAcademicaId=dca.CargaAcademicaId,
        //                    DetallePensumId=dp.DetallePensumId,
        //                    CargaAcademicaId=ca.CargaAcademicaId,
        //                    NombreCurso=cur.NombreCurso,
        //                    CarreraId=hcti.CarreraId,
        //                    NombreCarrera=car.NombreCarrera,
        //                    Ciclo=dp.Ciclo??0,
        //                    NombreSalon=s.NombreSalon,
        //                    CursoId=cur.CursoId
        //                };

        //    return query;

        //}


        public IEnumerable<HorarioCursoItem> GetHorarioGridByDetalleCargaAcademicaId(decimal DetalleCargaAcademicaId)
        {
            var dc = Context;
            var query = from dca in dc.DetalleCargaAcademica
                        join hc in dc.HorarioCurso on dca.DetalleCargaAcademicaId equals hc.DetalleCargaAcademicaId
                        join s in dc.Salon on hc.SalonId equals s.SalonId
                        

                        where dca.DetalleCargaAcademicaId==DetalleCargaAcademicaId
                        orderby hc.HoraInicio,hc.HoraFin

                        select new HorarioCursoItem
                        {
                            Dia =hc.Dia,
                            HoraInicio=hc.HoraInicio,
                            HoraFin=hc.HoraFin,
                            Laboratorio=hc.Laboratorio,
                            SalonId=s.SalonId,
                            NombreSalon=s.NombreSalon
                        };

            return query;
        }


        public DetalleCargaAcademicaItem getDetalleCargaAcademicaByDetalleCargaAcademicaId(decimal id)
        {
            var dc = Context;
            var query = from dca in dc.DetalleCargaAcademica
                        join dp in dc.DetallePensum on dca.DetallePensumId equals dp.DetallePensumId
                        join c in dc.Curso on dp.CursoId equals c.CursoId
                        join col in dc.Colaborador on dca.ColaboradorId equals col.ColaboradorId


                        where dca.DetalleCargaAcademicaId == id

                        select new DetalleCargaAcademicaItem
                        {
                            DetalleCargaAcademicaId = dca.DetalleCargaAcademicaId,
                            CargaAcademicaId = dca.CargaAcademicaId,
                            DetallePensumId = dca.DetallePensumId,
                            HorasCursoMensual = dca.HorasCursoMensual ?? 0,
                            EstadoCargaId = dca.EstadoCargaId,
                            ColaboradorId = dca.ColaboradorId,
                            ContratoId = dca.ContratoId ?? 0,
                            NombreCurso = c.NombreCurso,

                            NombreColaborador = col.PrimerNombre + " " + col.PrimerApellido + " " + col.SegundoApellido,
                            PuedeSerBorrado = true,
                            CodCurso = c.CodCurso,
                            Ciclo = dp.Ciclo ?? 0
                        };
                            

            return query.First();
        }


    }




    public class DetalleCargaAcademicaItem
    {
        public decimal DetalleCargaAcademicaId { get; set; }
        public decimal CargaAcademicaId { get; set; }
        public decimal DetallePensumId { get; set; }
        public decimal HorasCursoMensual { get; set; }
        public int EstadoCargaId { get; set; }
        public decimal ContratoId { get; set; }
        public decimal ColaboradorId { get; set; }
        public string NombreCurso { get; set; }
        public string NombreFacultad { get; set; }
        public string NombreColaborador { get; set; }
        public string NombreEstado { get; set; }
        public string NombreCarrera { get; set; }
        public decimal CursoId { get; set; }
        public bool PuedeSerBorrado { get; set; }
        public string CodCurso { get; set; }
        public decimal Ciclo { get; set; }


    }

    public class HorarioCursoTraslapadoItem
    {
       
        public string Dia { get; set; }
        public Nullable<System.TimeSpan> HoraInicio { get; set; }
        public Nullable<System.TimeSpan> HoraFin { get; set; }
        public decimal CarreraId { get; set; }
        public string Jornada { get; set; }
        public decimal SalonId { get; set; }
        public decimal CursoId { get; set; }

        public decimal DetalleCargaAcademicaId { get; set; }
        public decimal DetallePensumId { get; set; }
        public decimal CargaAcademicaId { get; set; }
        public string NombreCurso { get; set; }
        //public string NombreSalon { get; set; }
        public string NombreCarrera { get; set; }
        public string NombreSalon { get; set; }
        public decimal Ciclo { get; set; }



    }

    public class HorarioCursoItem
    {

        public string Dia { get; set; }
        public Nullable<System.TimeSpan> HoraInicio { get; set; }
        public Nullable<System.TimeSpan> HoraFin { get; set; }
       
        public string Jornada { get; set; }
        public decimal SalonId { get; set; }
       

        public decimal DetalleCargaAcademicaId { get; set; }

        public string Laboratorio { get; set; }
        public string NombreSalon { get; set; }
        



    }




    
}
