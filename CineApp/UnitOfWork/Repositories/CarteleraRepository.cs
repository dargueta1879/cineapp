﻿using System.Collections.Generic;
using System.Linq;
using CineApp.Persistence;
using System;
using CineApp.UnitOfWork.Repositories;

namespace CineApp.UnitOfWork.Repositories
{
    public interface ICarteleraRepository : IGenericRepository<Cartelera>
    {
       
        IEnumerable<CarteleraGridItem> GetCarteleraGridData();
        IEnumerable<CarteleraGridItem> GetCarteleraByCine(int CineId);
        IEnumerable<PeliculaItem> GetAllPeliculasInCartelera();
        
    }

    internal class CarteleraRepository : GenericRepository<DBCineEntities, Cartelera>, ICarteleraRepository
    {
        public CarteleraRepository(DBCineEntities ctx)
            : base(ctx)
        {
        }

        //public IEnumerable<CarreraGridItem> GetCarreraGridData()
        //{
        //    var dc = Context;
        //    var query = from carrera in dc.Carrera
        //        join facultad in dc.Facultad on carrera.FacultadId equals facultad.FacultadId
        //        select new CarreraGridItem
        //        {
        //            CarreraId = carrera.CarreraId,
        //            NombreCarrera = carrera.NombreCarrera,
        //            FacultadId = facultad.FacultadId,
        //            NombreFacultad = facultad.NombreFacultad,
        //            PuedeSerBorrado = true,
        //        };

        //    return query;
        //}

        //public CarreraGridItem GetCarreraIdByDetallePensumId(decimal DetallePensumId)
        //{
        //    var dc = Context;
        //    var query = from dp in dc.DetallePensum 
        //                join p in dc.Pensum on dp.PensumId equals p.PensumId
        //                join car in dc.Carrera on p.CarreraId equals car.CarreraId

        //                where dp.DetallePensumId==DetallePensumId
                           
        //                select new CarreraGridItem
        //                {
        //                    CarreraId = car.CarreraId,
        //                    NombreCarrera = car.NombreCarrera,
        //                    FacultadId = car.FacultadId,
        //                    PuedeSerBorrado = true,
        //                };

        //    return query.First()??null;
        //}


        public IEnumerable<CarteleraGridItem> GetCarteleraGridData()
        {
            var dc = Context;
            var query = from cartelera in dc.Cartelera
                        join pelicula in dc.Pelicula on cartelera.PeliculaId equals pelicula.PeliculaId
                        join horario in dc.Horario on cartelera.HorarioId equals horario.HorarioId
                        join sala in dc.Sala on cartelera.SalaId equals sala.SalaId
                        join es in dc.EstadoCartelera on cartelera.EstadoCarteleraId equals es.EstadoCarteleraId
                        join cine in dc.Cine on sala.CineId equals cine.CineId
                        where cartelera.EstadoCarteleraId!=2
                        orderby cine.CineId ascending,horario.HoraInicio ascending,sala.SalaId ascending,pelicula.PeliculaId ascending
                        select new CarteleraGridItem
                        {
                            CarteleraId=cartelera.CarteleraId,
                            PeliculaId=pelicula.PeliculaId,
                            EstadoCarteleraId=es.EstadoCarteleraId,
                            HorarioId=horario.HorarioId,
 
                            
                              CineId=cine.CineId,
                              SalaId=sala.SalaId,
                            
                              NombrePelicula =pelicula.NombrePelicula,
                              NombreEstadoCartelera =es.NombreEstadoCartelera,
                              HoraInicio=horario.HoraInicio,
                              HoraFin=horario.HoraFin,
                              Dia=horario.Dia,
                              NombreSala=sala.NombreSala,
                              NombreCine=cine.NombreCine

                            
                        };

            return query;
        }

        public IEnumerable<CarteleraGridItem> GetCarteleraByCine(int CineId)
        {
            var dc = Context;
            var query = from cartelera in dc.Cartelera
                        join pelicula in dc.Pelicula on cartelera.PeliculaId equals pelicula.PeliculaId
                        join horario in dc.Horario on cartelera.HorarioId equals horario.HorarioId
                        join sala in dc.Sala on cartelera.SalaId equals sala.SalaId
                        join es in dc.EstadoCartelera on cartelera.EstadoCarteleraId equals es.EstadoCarteleraId
                        join cine in dc.Cine on sala.CineId equals cine.CineId
                        where cartelera.EstadoCarteleraId != 2 && sala.CineId==CineId
                        orderby cine.CineId ascending, horario.HoraInicio ascending, sala.SalaId ascending, pelicula.PeliculaId ascending
                        select new CarteleraGridItem
                        {
                            CarteleraId = cartelera.CarteleraId,
                            PeliculaId = pelicula.PeliculaId,
                            EstadoCarteleraId = es.EstadoCarteleraId,
                            HorarioId = horario.HorarioId,


                            CineId = cine.CineId,
                            SalaId = sala.SalaId,
                            NombrePelicula = pelicula.NombrePelicula,
                            NombreEstadoCartelera = es.NombreEstadoCartelera,
                            HoraInicio = horario.HoraInicio,
                            HoraFin = horario.HoraFin,
                            Dia = horario.Dia,
                            NombreSala = sala.NombreSala,
                            NombreCine = cine.NombreCine


                        };

            return query;
        }


        public IEnumerable<PeliculaItem> GetAllPeliculasInCartelera()
        {
            var dc = Context;
            var query = from cartelera in dc.Cartelera
                        join peli in dc.Pelicula on cartelera.PeliculaId equals peli.PeliculaId
                        where peli.EstadoPeliculaId==1
                        group peli by peli.PeliculaId into pe
                        select new PeliculaItem
                        {
                          PeliculaId=pe.Key,
                           NombrePelicula = pe.Select(x => x.NombrePelicula).FirstOrDefault(),
                           Clasificacion=pe.Select(x => x.Clasificacion).FirstOrDefault(),
                           Duracion=pe.Select(x => x.Duracion).FirstOrDefault(),
                           Imagen=pe.Select(x => x.Imagen).FirstOrDefault(),
                           Descripcion=pe.Select(x => x.Descripcion).FirstOrDefault()
                            


                        };

            return query;
        }
    //}
    //public class CarreraComboItem
    //{
    //    public decimal PensumId { get; set; }
    //    public string NombreCarrera { get; set; }
    //}
       
        
    }

    public class PeliculaItem
    {
        public int PeliculaId { get; set; }
        public int EstadoPeliculaId { get; set; }
 

        public string NombrePelicula { get; set; }
        public string Descripcion { get; set; }
        public string Imagen { get; set; }
        public string Clasificacion { get; set; }
        public string Duracion { get; set; }
        
    }

    public class CarteleraGridItem
    {
        public int CarteleraId { get; set; }
        public int PeliculaId { get; set; }
        public int EstadoCarteleraId { get; set; }
        public int HorarioId { get; set; }
        public int CineId { get; set; }
        public int SalaId { get; set; }

        public string NombrePelicula { get; set; }
        public string NombreEstadoCartelera { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFin { get; set; }
        public string Dia { get; set; }
        public string NombreSala { get; set; }
        public string NombreCine { get; set; }
    }
}
