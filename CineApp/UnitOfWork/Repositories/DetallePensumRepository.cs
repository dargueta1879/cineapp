﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutentitacionProyect.Persistence;

namespace AutentitacionProyect.Repositories
{
    public interface IDetallePensumRepository : IGenericRepository<DetallePensum>
    {

        IEnumerable<DetallePensumGridItem> GetDetallePensumGrid(decimal item);
        IEnumerable<DetallePensumGridByCargaAcademicaItem> GetDetallePensumGridByCargaAcademicaId(decimal item);

    }
    internal class DetallePensumRepository : GenericRepository<AcademicLoadDBEntities, DetallePensum>, IDetallePensumRepository
    {
        public DetallePensumRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<DetallePensumGridItem> GetDetallePensumGrid(decimal item)
        {
            var dc = Context;
            var query = from pen in dc.Pensum
                        join detaepen in dc.DetallePensum on pen.PensumId equals detaepen.PensumId
                        join cur in dc.Curso  on detaepen.CursoId equals cur.CursoId
                        where detaepen.PensumId == item
                        orderby detaepen.Ciclo ascending
                        select new DetallePensumGridItem
                        {
                            DetallePensumId=detaepen.DetallePensumId,
                            Estado=detaepen.Estado,
                            Ciclo=detaepen.Ciclo??0,
                            Obligatorio=detaepen.Obligatorio,
                            NoCreditosPrerrequisito=detaepen.NoCreditosPrerrequisito??0,
                            NoCreditosPensum=cur.NoCreditos??0,
                            PensumId =detaepen.PensumId,
                            NombreCurso =cur.NombreCurso,
                            CursoId=cur.CursoId
                        };

            return query;
        }



        public IEnumerable<DetallePensumGridByCargaAcademicaItem> GetDetallePensumGridByCargaAcademicaId(decimal item)
        {
            var dc = Context;
            var query = from fac in dc.Facultad
                        join car in dc.Carrera on fac.FacultadId equals car.FacultadId
                       
                        join pen in dc.Pensum on car.CarreraId equals pen.CarreraId
                        join detaepen in dc.DetallePensum on pen.PensumId equals detaepen.PensumId
                        join cur in dc.Curso on detaepen.CursoId equals cur.CursoId
                        join ca in dc.CargaAcademica on fac.FacultadId equals ca.FacultadId


                        where ca.CargaAcademicaId== item
                        orderby detaepen.Ciclo, car.NombreCarrera ascending
                        select new DetallePensumGridByCargaAcademicaItem
                        {
                            DetallePensumId = detaepen.DetallePensumId,
                            Estado = detaepen.Estado,
                            Ciclo = detaepen.Ciclo ?? 0,
                            Obligatorio = detaepen.Obligatorio,
                            NoCreditosPrerrequisito = detaepen.NoCreditosPrerrequisito ?? 0,
                            PensumId = detaepen.PensumId,
                            NombreCurso = cur.NombreCurso,
                            CursoId = cur.CursoId,
                            CodCurso=cur.CodCurso,
                            HorasSemanaMes =cur.HorasSemanaMes,
                            NombreCarrera=car.NombreCarrera
                        };
            return query;
        }


    }

    public class DetallePensumGridItem
    {
        public decimal DetallePensumId { get; set; }
        public decimal Ciclo { get; set; }
        public string Estado { get; set; }
        public string Obligatorio { get; set; }
        public decimal NoCreditosPrerrequisito { get; set; }
        public decimal NoCreditosPensum { get; set; }
        public decimal PensumId { get; set; }
        public decimal CursoId { get; set; }
        public string NombreCurso { get; set; }
        
    }

    public class DetallePensumGridByCargaAcademicaItem
    {
        public decimal DetallePensumId { get; set; }
        public decimal Ciclo { get; set; }
        public string Estado { get; set; }
        public string Obligatorio { get; set; }
        public decimal NoCreditosPrerrequisito { get; set; }
        
        public decimal PensumId { get; set; }
        public decimal CursoId { get; set; }
        public string NombreCurso { get; set; }
        public decimal CarreraId { get; set; }
        public string NombreCarrera { get; set; }
        public string Laboratorio { get; set; }
        public string CodCurso { get; set; }
        public Nullable<decimal> HorasSemanaMes { get; set; }      
    }





}