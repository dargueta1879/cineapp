﻿using System.Collections.Generic;
using System.Linq;
using Elecciones.Core.Model;
using System;

namespace Elecciones.Core.Repositories
{
    public interface IHorarioContratacionRepository : IGenericRepository<HorarioContratacion>
    {

        IEnumerable<HorarioContratacionItem> GetHorarioContratacionGrid(int codCargaCursoContratacion);

    }
    internal class HorarioContratacionRepository: GenericRepository<EleccionesEntities, HorarioContratacion>, IHorarioContratacionRepository
    {
        public HorarioContratacionRepository(EleccionesEntities ctx)
            : base(ctx)

        {
        }

        public IEnumerable<HorarioContratacionItem> GetHorarioContratacionGrid(int codCargaCursoContratacion)
        {

            var dc = Context;
            var query = from hc in dc.HorarioContratacions
                join ccc in dc.CargaCursoContratacions on hc.CodCargaCurso equals ccc.CodCargaCurso
                        where ccc.CodCargaCurso == codCargaCursoContratacion
                select new HorarioContratacionItem
                {
                   
                    HoraInicio=hc.HoraInicioContratacion,
                    HoraFin=hc.HoraFinContratacion,
                    Dia=hc.DiaContratacion,
                    Jornada=hc.Jornada,
                    CodCargaCurso=hc.CodCargaCurso
                    
                };

            return query;
        }
    }

    public class HorarioContratacionItem
    {

       
        public TimeSpan? HoraInicio { get; set; }
        public TimeSpan? HoraFin { get; set; }
        
        public string Jornada { get; set; }
        public string Dia { get; set; }
        public int CodCargaCurso { get; set; }

    }
}
