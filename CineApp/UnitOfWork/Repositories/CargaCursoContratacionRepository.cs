﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;
using AutentitacionProyect.Repositories;


namespace Elecciones.Core.Repositories
{
    public interface ICargaCursoContratacionRepository : IGenericRepository<CargaCursoContratacion>
    {
        IEnumerable<AllCursoItem> GetAllCursos(int facultadId);
        IEnumerable<UserPermissionItem> GetFacultad();
        IEnumerable<AsignacionGridItem> GetAsignacionGridData(int carreraId, int cicloIdensumId);
        IEnumerable<CargaAcademicaItem> GetCargaAcademicaGridSource(int facultadId);
        IEnumerable<CargaCursoContratacionItem> GetCargaCursoContratacionGrid(int cargaAcademica);
        IEnumerable<DetalleCargaContratacionItem> GetSecondLevelData(int cargaAcademicaId);
        IEnumerable<HorarioExpandItem> GetThirdLevelData(int cargaCursoContratacionId);
        IEnumerable<CatedraticoItem> GetCatedratico(int facultadId);
        IEnumerable<HorarioExportItem> GetExportThirdLevelData(int cargaCursoContratacionId);
    }
    internal class CargaCursoContratacionRepository : GenericRepository<EleccionesEntities, CargaCursoContratacion>, ICargaCursoContratacionRepository
    {
        public CargaCursoContratacionRepository(EleccionesEntities ctx)
            : base(ctx)
        {
        }


        public IEnumerable<UserPermissionItem> GetFacultad()
        {
            var dc = Context;

            var query = from usuario in dc.UserAccounts
                        join asignacionPuesto in dc.AsignacionPuestoes on usuario.Key equals asignacionPuesto.IdUserAccounts
                        join puesto in dc.Puestoes on asignacionPuesto.CodPuesto equals puesto.CodPuesto
                        join facultad in dc.Facultads on asignacionPuesto.CodFacultad equals facultad.CodFacultad
                        where puesto.NombreTipoPuesto == "COORDINADOR"
                        select new UserPermissionItem
                        {
                            Username = usuario.Username,
                            Facultad = facultad.NombreFacultad
                        };

            return query;
        }

        public IEnumerable<AllCursoItem> GetAllCursos(int facultadId)
        {
            var dc = Context;
            var query = from curso in dc.Cursoes
                        join detallePensum in dc.DetallePensums on curso.CodCurso equals detallePensum.CodCurso
                        join pensum in dc.Pensums on detallePensum.CodPensum equals pensum.CodPensum
                        join carrera in dc.Carreras on pensum.CodCarrera equals carrera.CodCarrera
                        where carrera.CodFacultad == facultadId
                        select new AllCursoItem
                        {
                            PensumId = pensum.CodPensum,
                            CicloId = detallePensum.Ciclo ?? 0,
                            DetallePensumId = detallePensum.CodDetallePensum,
                            NombreCurso = curso.NombreCurso,
                        };

            return query;
        }

        public IEnumerable<AsignacionGridItem> GetAsignacionGridData(int carreraId, int cicloId)
        {
            var dc = Context;
            var query = from pensum in dc.Pensums
                        join detallePensum in dc.DetallePensums on pensum.CodPensum equals detallePensum.CodPensum
                        join curso in dc.Cursoes on detallePensum.CodCurso equals curso.CodCurso
                        where pensum.CodCarrera == carreraId && detallePensum.Ciclo == cicloId
                        select new AsignacionGridItem
                        {
                            DetallePensumId = detallePensum.CodDetallePensum,
                            PensumId = pensum.CodPensum,
                            CicloId = detallePensum.Ciclo,
                            CursoId = curso.CodCurso,
                            NombreCurso = curso.NombreCurso,
                            Estado = detallePensum.Estado,
                            Obligatorio = detallePensum.Obligatorio,
                            NoCreditosPrerrequisito = detallePensum.NoCreditosPrerrequisito,
                            CanBeDeleted = false,
                        };

            return query;
        }


        public IEnumerable<CargaAcademicaItem> GetCargaAcademicaGridSource(int facultadId)
        {
            var dc = Context;
            var query = from carga in dc.CargaAcademicas
                        where carga.CodFacultad == facultadId
                        select new CargaAcademicaItem
                        {
                            CodCargaAcademica = carga.CodCargaAcademica,
                            NombreCargaAcademica = carga.NombreCargaAcademica,
                            FechaFin = carga.FechaFin,
                            FechaInicio = carga.FechaInicio,
                        };

            return query;
        }

        //Query de CagaCursoContratacion
        public IEnumerable<CargaCursoContratacionItem> GetCargaCursoContratacionGrid(int cargaAcademica)
        {
            var dc = Context;
            var query = from cargacursocontratacion in dc.CargaCursoContratacions
                        join cargaacademica in dc.CargaAcademicas on cargacursocontratacion.CodCargaAcademica equals cargaacademica.CodCargaAcademica
                        join detalle in dc.DetallePensums on cargacursocontratacion.CodDetallePensum equals detalle.CodDetallePensum
                        join curso in dc.Cursoes on detalle.CodCurso equals curso.CodCurso
                        where cargacursocontratacion.CodCargaAcademica == cargaAcademica
                        select new CargaCursoContratacionItem
                        {
                            CodCargaCurso = cargacursocontratacion.CodCargaCurso,
                            CodCargaAcademica = cargacursocontratacion.CodCargaAcademica,
                            CodDetallePensum = cargacursocontratacion.CodDetallePensum,
                            HorasContratacionMensual = cargacursocontratacion.HorasContratacionMensual ?? 0,
                            BonoMensual = cargacursocontratacion.BonoMensual ?? 0,
                            //Plaza =cargacursocontratacion.Plaza??0,
                            Estado = cargacursocontratacion.Estado,
                            Escala = cargacursocontratacion.Escala ?? 0,
                            CicloContratacion = cargacursocontratacion.CicloDeContratacion ?? 0,
                            //CodUser =,
                            //NombreCatedratico=
                            Atribuciones = cargacursocontratacion.Atribuciones,
                            CodCurso = curso.CodCurso,
                            NombreCurso = curso.NombreCurso
                        };

            return query;
        }

        public IEnumerable<DetalleCargaContratacionItem> GetSecondLevelData(int cargaAcademicaId)
        {
            var dc = Context;
            var query  = from cargaCurso in dc.CargaCursoContratacions
                         join cargaacademica in dc.CargaAcademicas on cargaCurso.CodCargaAcademica equals cargaacademica.CodCargaAcademica
                         join detallePensum in dc.DetallePensums on cargaCurso.CodDetallePensum equals detallePensum.CodDetallePensum
                         join pensum in dc.Pensums on detallePensum.CodPensum equals pensum.CodPensum
                         join carrera in dc.Carreras on pensum.CodCarrera equals carrera.CodCarrera
                       join facultad in dc.Facultads on carrera.CodFacultad equals facultad.CodFacultad
                         join curso in dc.Cursoes on detallePensum.CodCurso equals curso.CodCurso
                         join catedratico in dc.Catedraticoes on cargaCurso.CatedraticoId equals catedratico.CatedraticoId
                        
                           where cargaCurso.CodCargaAcademica == cargaAcademicaId
                         orderby carrera.CodCarrera
                         select new DetalleCargaContratacionItem
                         {
                             CargaCursoContratacionId = cargaCurso.CodCargaCurso,
                             CargaAcademicaId = cargaAcademicaId,
                             PensumId = detallePensum.CodPensum,
                            NombreCarrera = carrera.NombreCarrera,
                             CicloCursoId = detallePensum.Ciclo ?? 0,
                             CicloCurso = detallePensum.Ciclo + " Semestre",
                             DetallePensumId = detallePensum.CodPensum,
                             NombreCurso = curso.NombreCurso,
                             HorasContratacionMensual = cargaCurso.HorasContratacionMensual ?? 0,
                             BonoMensual = cargaCurso.BonoMensual ?? 0,
                             Plaza = cargaCurso.Plaza ?? 0,
                             Estado = cargaCurso.Estado,
                             Escala = cargaCurso.Escala ?? 0,
                             CicloContratacionId = cargaCurso.CicloDeContratacion ?? 0,
                             CicloContratacion = cargaCurso.CicloDeContratacion + " Semestre",
                             Attribuciones = cargaCurso.Atribuciones,
                             UserKeyId = cargaCurso.CatedraticoId,
                             Catedratico = catedratico.NombreCatedratico,
                             Vigencia = "Del " + cargaacademica.FechaInicio.ToString() + " al " + cargaacademica.FechaFin.ToString(),
                             CodUniversitario = catedratico.CodUniversitario,
                             Partida=facultad.Partida,
                             SalarioMensual=cargaCurso.HorasContratacionMensual*cargaCurso.Escala ?? 0,
                             HorasSugeridasCurso=curso.HorasSemana??0,
                             CanBeDeleted = true
                         };

            return query;
        }

        public IEnumerable<HorarioExpandItem> GetThirdLevelData(int cargaCursoContratacionId)
        {
            var dc = Context;
            var curso = dc.CargaCursoContratacions.Single(x => x.CodCargaCurso == cargaCursoContratacionId).DetallePensum.Curso.NombreCurso;

            var returnList = new List<HorarioExpandItem>();
            var horarioCurso = dc.HorarioCursoes.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.Dia.ToUpper() == "LUNES");
            var horarioContratacion = dc.HorarioContratacions.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.DiaContratacion.ToUpper() == "LUNES");
            returnList.Add(new HorarioExpandItem
            {
                RowId = cargaCursoContratacionId + (horarioCurso != null ? horarioCurso.CodHorarioCargaCurso : 0) + (horarioContratacion != null ? horarioContratacion.CodHorarioCargaCurso : 0),
                NombreCurso = curso,
                CargaCursoContratacionId = cargaCursoContratacionId,
                HorarioCursoId = horarioCurso == null ? -1 : horarioCurso.CodHorarioCargaCurso,
                HorarioContratacionId = horarioContratacion == null ? -1 : horarioContratacion.CodHorarioCargaCurso,
                HoraInicio = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraInicio,
                HoraFin = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraFin,
                DiaCurso = "Lunes",
                Laboratorio = horarioCurso == null ? "" : horarioCurso.Laboratorio,
                JornadaCurso = horarioCurso == null ? "" : horarioCurso.Jornada,
                SalonId = horarioCurso == null ? -1 : horarioCurso.CodSalon,
                Salon = horarioCurso != null ? horarioCurso.Salon.NombreSalon : "",
                HoraContratacionInicio = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraInicioContratacion,
                HoraContratacionFin = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraFinContratacion,
                JornadaContratacion = horarioContratacion == null ? "" : horarioContratacion.Jornada,

                CanBeDeleted = true,
            });

            horarioCurso = dc.HorarioCursoes.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.Dia.ToUpper() == "MARTES");
            horarioContratacion = dc.HorarioContratacions.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.DiaContratacion.ToUpper() == "MARTES");
            returnList.Add(new HorarioExpandItem
            {
                RowId = cargaCursoContratacionId + (horarioCurso != null ? horarioCurso.CodHorarioCargaCurso : 0) + (horarioContratacion != null ? horarioContratacion.CodHorarioCargaCurso : 0),
                NombreCurso = curso,
                CargaCursoContratacionId = cargaCursoContratacionId,
                HorarioCursoId = horarioCurso == null ? -1 : horarioCurso.CodHorarioCargaCurso,
                HorarioContratacionId = horarioContratacion == null ? -1 : horarioContratacion.CodHorarioCargaCurso,
                HoraInicio = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraInicio,
                HoraFin = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraFin,
                DiaCurso = "Martes",
                Laboratorio = horarioCurso == null ? "" : horarioCurso.Laboratorio,
                JornadaCurso = horarioCurso == null ? "" : horarioCurso.Jornada,
                SalonId = horarioCurso == null ? -1 : horarioCurso.CodSalon,
                Salon = horarioCurso != null ? horarioCurso.Salon.NombreSalon : "",
                HoraContratacionInicio = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraInicioContratacion,
                HoraContratacionFin = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraFinContratacion,
                JornadaContratacion = horarioContratacion == null ? "" : horarioContratacion.Jornada,

                CanBeDeleted = true,
            });

            horarioCurso = dc.HorarioCursoes.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.Dia.ToUpper() == "MIERCOLES");
            horarioContratacion = dc.HorarioContratacions.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.DiaContratacion.ToUpper() == "MIERCOLES");
            returnList.Add(new HorarioExpandItem
            {
                RowId = cargaCursoContratacionId + (horarioCurso != null ? horarioCurso.CodHorarioCargaCurso : 0) + (horarioContratacion != null ? horarioContratacion.CodHorarioCargaCurso : 0),
                NombreCurso = curso,
                CargaCursoContratacionId = cargaCursoContratacionId,
                HorarioCursoId = horarioCurso == null ? -1 : horarioCurso.CodHorarioCargaCurso,
                HorarioContratacionId = horarioContratacion == null ? -1 : horarioContratacion.CodHorarioCargaCurso,
                HoraInicio = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraInicio,
                HoraFin = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraFin,
                DiaCurso = "Miercoles",
                Laboratorio = horarioCurso == null ? "" : horarioCurso.Laboratorio,
                JornadaCurso = horarioCurso == null ? "" : horarioCurso.Jornada,
                SalonId = horarioCurso == null ? -1 : horarioCurso.CodSalon,
                Salon = horarioCurso != null ? horarioCurso.Salon.NombreSalon : "",
                HoraContratacionInicio = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraInicioContratacion,
                HoraContratacionFin = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraFinContratacion,
                JornadaContratacion = horarioContratacion == null ? "" : horarioContratacion.Jornada,

                CanBeDeleted = true,
            });

            horarioCurso = dc.HorarioCursoes.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.Dia.ToUpper() == "JUEVES");
            horarioContratacion = dc.HorarioContratacions.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.DiaContratacion.ToUpper() == "JUEVES");
            returnList.Add(new HorarioExpandItem
            {
                RowId = cargaCursoContratacionId + (horarioCurso != null ? horarioCurso.CodHorarioCargaCurso : 0) + (horarioContratacion != null ? horarioContratacion.CodHorarioCargaCurso : 0),
                NombreCurso = curso,
                CargaCursoContratacionId = cargaCursoContratacionId,
                HorarioCursoId = horarioCurso == null ? -1 : horarioCurso.CodHorarioCargaCurso,
                HorarioContratacionId = horarioContratacion == null ? -1 : horarioContratacion.CodHorarioCargaCurso,
                HoraInicio = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraInicio,
                HoraFin = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraFin,
                DiaCurso = "Jueves",
                Laboratorio = horarioCurso == null ? "" : horarioCurso.Laboratorio,
                JornadaCurso = horarioCurso == null ? "" : horarioCurso.Jornada,
                SalonId = horarioCurso == null ? -1 : horarioCurso.CodSalon,
                Salon = horarioCurso != null ? horarioCurso.Salon.NombreSalon : "",
                HoraContratacionInicio = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraInicioContratacion,
                HoraContratacionFin = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraFinContratacion,
                JornadaContratacion = horarioContratacion == null ? "" : horarioContratacion.Jornada,

                CanBeDeleted = true,
            });

            horarioCurso = dc.HorarioCursoes.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.Dia.ToUpper() == "VIERNES");
            horarioContratacion = dc.HorarioContratacions.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.DiaContratacion.ToUpper() == "VIERNES");
            returnList.Add(new HorarioExpandItem
            {
                RowId = cargaCursoContratacionId + (horarioCurso != null ? horarioCurso.CodHorarioCargaCurso : 0) + (horarioContratacion != null ? horarioContratacion.CodHorarioCargaCurso : 0),
                NombreCurso = curso,
                CargaCursoContratacionId = cargaCursoContratacionId,
                HorarioCursoId = horarioCurso == null ? -1 : horarioCurso.CodHorarioCargaCurso,
                HorarioContratacionId = horarioContratacion == null ? -1 : horarioContratacion.CodHorarioCargaCurso,
                HoraInicio = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraInicio,
                HoraFin = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraFin,
                DiaCurso = "Viernes",
                Laboratorio = horarioCurso == null ? "" : horarioCurso.Laboratorio,
                JornadaCurso = horarioCurso == null ? "" : horarioCurso.Jornada,
                SalonId = horarioCurso == null ? -1 : horarioCurso.CodSalon,
                Salon = horarioCurso != null ? horarioCurso.Salon.NombreSalon : "",
                HoraContratacionInicio = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraInicioContratacion,
                HoraContratacionFin = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraFinContratacion,
                JornadaContratacion = horarioContratacion == null ? "" : horarioContratacion.Jornada,

                CanBeDeleted = true,
            });

            horarioCurso = dc.HorarioCursoes.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.Dia.ToUpper() == "SABADO");
            horarioContratacion = dc.HorarioContratacions.FirstOrDefault(x => x.CodCargaCurso == cargaCursoContratacionId && x.DiaContratacion.ToUpper() == "SABADO");
            returnList.Add(new HorarioExpandItem
            {
                RowId = cargaCursoContratacionId + (horarioCurso != null ? horarioCurso.CodHorarioCargaCurso : 0) + (horarioContratacion != null ? horarioContratacion.CodHorarioCargaCurso : 0),
                NombreCurso = curso,
                CargaCursoContratacionId = cargaCursoContratacionId,
                HorarioCursoId = horarioCurso == null ? -1 : horarioCurso.CodHorarioCargaCurso,
                HorarioContratacionId = horarioContratacion == null ? -1 : horarioContratacion.CodHorarioCargaCurso,
                HoraInicio = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraInicio,
                HoraFin = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraFin,
                DiaCurso = "Sabado",
                Laboratorio = horarioCurso == null ? "" : horarioCurso.Laboratorio,
                JornadaCurso = horarioCurso == null ? "" : horarioCurso.Jornada,
                SalonId = horarioCurso == null ? -1 : horarioCurso.CodSalon,
                Salon = horarioCurso != null ? horarioCurso.Salon.NombreSalon : "",
                HoraContratacionInicio = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraInicioContratacion,
                HoraContratacionFin = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraFinContratacion,
                JornadaContratacion = horarioContratacion == null ? "" : horarioContratacion.Jornada,

                CanBeDeleted = true,
            });


            return returnList;
        }

        public IEnumerable<HorarioExportItem> GetExportThirdLevelData(int cargaCursoContratacionId)
        {

            var dc = Context;
            var query = from cargaContratacion in dc.CargaCursoContratacions
                        join catedratico in dc.Catedraticoes on cargaContratacion.CatedraticoId equals catedratico.CatedraticoId
                        join detalle in dc.DetallePensums on cargaContratacion.CodDetallePensum equals detalle.CodDetallePensum
                        join pensum in dc.Pensums on detalle.CodPensum equals pensum.CodPensum
                        join carrera in dc.Carreras on pensum.CodCarrera equals carrera.CodCarrera
                        join curso in dc.Cursoes on detalle.CodCurso equals curso.CodCurso
                        join horarioCurso in dc.HorarioCursoes on cargaContratacion.CodCargaCurso equals
                        horarioCurso.CodCargaCurso into cursoOuter
                        from horarioCurso in cursoOuter.DefaultIfEmpty()
                        join horarioContratacion in dc.HorarioContratacions on cargaContratacion.CodCargaCurso equals
                        horarioContratacion.CodCargaCurso into contratacionOuter
                        from horarioContratacion in contratacionOuter.DefaultIfEmpty()
                        join salon in dc.Salons on horarioCurso.CodSalon equals salon.CodSalon into salonOuter
                        from salon in salonOuter.DefaultIfEmpty()
                        where cargaContratacion.CodCargaCurso == cargaCursoContratacionId
                        select new HorarioExportItem
                        {
                            RowId = cargaCursoContratacionId + (horarioCurso != null ? horarioCurso.CodHorarioCargaCurso : 0) + (horarioContratacion != null ? horarioContratacion.CodHorarioCargaCurso : 0),
                            NombreCarrera=carrera.NombreCarrera,

                            NombreCatedratico = catedratico.NombreCatedratico,
                            CodCurso = curso.CodCurso,
                            NombreCurso=curso.NombreCurso,
                            HoraInicio = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraInicio,
                            HoraFin = horarioCurso == null ? (TimeSpan?)null : horarioCurso.HoraFin,
                            DiaCurso = horarioCurso == null ? "" : horarioCurso.Dia,
                            Laboratorio = horarioCurso == null ? "" : horarioCurso.Laboratorio,
                            JornadaCurso = horarioCurso == null ? "" : horarioCurso.Jornada,
                           
                            Salon = salon != null ? salon.NombreSalon : "",
                            HoraContratacionInicio = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraInicioContratacion,
                            HoraContratacionFin = horarioContratacion == null ? (TimeSpan?)null : horarioContratacion.HoraFinContratacion,
                            DiaContratacion = horarioContratacion == null ? "" : horarioContratacion.DiaContratacion,
                            JornadaContratacion = horarioContratacion == null ? "" : horarioContratacion.Jornada,

                            
                        };


            return query;
        }

        public IEnumerable<CatedraticoItem> GetCatedratico(int facultadId)
        {
            var dc = Context;
            var query = from catedratico in dc.Catedraticoes
                        let facultad = dc.AsignacionCatedraticoFacultads.Any(x => x.CatedraticoId == catedratico.CatedraticoId && x.FacultadId == facultadId)
                        where facultad
                        select new CatedraticoItem
                        {
                            UserKeyId = catedratico.CatedraticoId,
                            Catedratico = catedratico.NombreCatedratico,
                        };

            return query;
        }
    }

    public class CatedraticoItem
    {
        public  int UserKeyId { get; set; }
        public string Catedratico { get; set; }
    }

    public class HorarioExpandItem
    {
        public int RowId { get; set; }
        public int CargaCursoContratacionId { get; set; }
        public int HorarioCursoId { get; set; }
        public int HorarioContratacionId { get; set; }
        public string DiaCurso { get; set; }
        public TimeSpan? HoraInicio { get; set; }
        public TimeSpan? HoraFin { get; set; }
        public string Laboratorio { get; set; }
        public string JornadaCurso { get; set; }
        public int SalonId { get; set; }
        public string Salon { get; set; }
        public TimeSpan? HoraContratacionInicio { get; set; }
        public TimeSpan? HoraContratacionFin { get; set; }
        public string JornadaContratacion { get; set; }
        public bool CanBeDeleted { get; set; }
        public string NombreCurso { get; set; }
    }

    public class HorarioExportItem
    {
        public int RowId { get; set; }
        public string NombreCarrera { get; set; }
        public string NombreCatedratico { get; set; }
        public int CodCurso { get; set; }
        public string NombreCurso { get; set; }
        public string JornadaContratacion { get; set; }
        public string DiaContratacion { get; set; }
        
        public TimeSpan? HoraContratacionInicio { get; set; }
        public TimeSpan? HoraContratacionFin { get; set; }
        
        public string JornadaCurso { get; set; }
        public string DiaCurso { get; set; }
        public TimeSpan? HoraInicio { get; set; }
        public TimeSpan? HoraFin { get; set; }
        public string Laboratorio { get; set; }
        public string Salon { get; set; }
       
       
        
        
        
       
        
        
    }

    public class DetalleCargaContratacionItem
    {
        public string NombreCarrera { get; set; }
        public string CodUniversitario { get; set; }
        public string Catedratico { get; set; }
        
        public string NombreCurso { get; set; }
        public string CicloContratacion { get; set; }
        
        public int HorasContratacionMensual { get; set; }
        public int Escala { get; set; }
        public int SalarioMensual { get; set; }
        public int BonoMensual { get; set; }
        public string Vigencia { get; set; }
        public string Partida { get; set; }
        public int Plaza { get; set; }
        public string Attribuciones { get; set; }
        public int HorasSugeridasCurso { get; set; }

        public int CargaCursoContratacionId { get; set; }
        public int CargaAcademicaId { get; set; }
        public int PensumId { get; set; }
       
        public int CicloCursoId { get; set; }
        public string CicloCurso { get; set; }
        public int DetallePensumId { get; set; }
            
        public string Estado { get; set; }
        public int CicloContratacionId { get; set; }
       
        public int UserKeyId { get; set; }
        
        public bool CanBeDeleted { get; set; }
       
        
       
       



    }
    public class CargaCursoContratacionItem {
        public int CodDetallePensum { get; set; }
        public int HorasContratacionMensual { get; set; }
        public int BonoMensual { get; set; }
        public int Plaza { get; set; }
        public string Estado { get; set; }
        public int Escala { get; set; }
        public int CicloContratacion { get; set; }
        public int CodCurso { get; set; }
        public string NombreCurso { get; set; }
        public int CodUser { get; set; }
        public string Atribuciones { get; set; }
        public string NombreCatedratico { get; set; }
        public int CodCargaCurso { get; set; }
        public int CodCargaAcademica { get; set; }
    }

    public class PensumItem
    {
        public int PensumId { get; set; }
    }
   public class AsignacionGridItem
    {
        public int DetallePensumId { get; set; }
        public int PensumId { get; set; }
        public int? CicloId { get; set; }
        public int CursoId { get; set; }
        public string NombreCurso { get; set; }
        public string Estado { get; set; }
        public string Obligatorio { get; set; }
        public int? NoCreditosPrerrequisito { get; set; }
        public bool CanBeDeleted { get; set; }
    }

    public class AllCursoItem
    {
        public int PensumId { get; set; }
        public int CicloId { get; set; }
        public int DetallePensumId { get; set; }
        public string NombreCurso { get; set; }
    }

    public class UserPermissionItem
    {
        public string Username { get; set; }
        public string Facultad { get; set; }
    }

    public class CargaAcademicaItem
    {
        public int CodCargaAcademica { get; set; }
        public string NombreCargaAcademica { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
    }
}
