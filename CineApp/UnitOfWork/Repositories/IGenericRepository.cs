﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CineApp.UnitOfWork.Repositories
{
    public interface IGenericRepository<T> : IDisposable where T : class
    {
        IQueryable<T> GetAll();
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        T FindOneBy(Expression<Func<T, bool>> predicate);
        T GetById(object id);

        void Add(T entity);
        void Attach(T entity);
        void Edit(T entity);
        void Delete(T entity);
        //void Delete(IEnumerable<T> entities);
        void BulkInsertAll(IEnumerable<T> elements);
    }
}