﻿using System.Collections.Generic;
using System.Linq;
using AutentitacionProyect.Persistence;
using System;


namespace AutentitacionProyect.Repositories
{
    public interface IAccesoRolRepository : IGenericRepository<AccesoRol>
    {
        IEnumerable<AccesoRolGridItem> GetAccesoRolGridData(int idRole);
    }
    internal class AccesoRolRepository : GenericRepository<AcademicLoadDBEntities, AccesoRol>, IAccesoRolRepository
    {
        public AccesoRolRepository(AcademicLoadDBEntities ctx)
            : base(ctx)
        {
        }

        public IEnumerable<AccesoRolGridItem> GetAccesoRolGridData(int idRole)
        {

            var dc = Context;
            var query = from accesorol in dc.AccesoRol
                        join accesos in dc.Accesos on accesorol.AccesoId equals accesos.AccesoId
                        where accesorol.RoleId==idRole

                        select new AccesoRolGridItem
                        {
                            AccesoRolId =accesorol.AccesoRolId ,
                            AccesoId = accesorol.AccesoId,
                             RolId= accesorol.RoleId,
                             isSelected="si",
                             NombreControlador=accesos.Controlador,
                             NombreAccion=accesos.Accion
                        };

            return query;
        }
    }

    public class AccesoRolGridItem
    {
        public int AccesoRolId { get; set; }
        public int AccesoId { get; set; }
        public int RolId { get; set; }
        public string isSelected { get; set; }
        public string NombreControlador { get; set; }
        public string NombreAccion { get; set; } 
    }
}
