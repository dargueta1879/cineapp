var app = angular.module("myApp", []);
app.controller("CtrlCartelera", function ($scope, $http) {

    $http({
        method: "GET",
        url: "/Cine/GetAllCines"
    }).then(function mySuccess(response) {
        $scope.cines = response.data;
        console.log($scope.cines);
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
        alert("No ha sido posible conectarse al servidor");
    });

    $http({
        method: "GET",
        url: "/Cartelera/GetAllEstadoCartelera"
    }).then(function mySuccess(response) {
        $scope.estadocarteleras = response.data;
        console.log($scope.estadocarteleras);
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
        alert("No ha sido posible conectarse al servidor");
    });

    $http({
        method: "GET",
        url: "/Cartelera/GetAllHorarios"
    }).then(function mySuccess(response) {
        $scope.horarios = response.data;
        console.log($scope.horarios);
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
        alert("No ha sido posible conectarse al servidor");
    });

    $http({
        method: "get",
        url: 'Pelicula/GetAll'
    }).then(function (response) {
        $scope.peliculas = response.data;
    }, function () {
        alert("No se pudieron cargar peliculas");
    })




    //debugger;  
    $scope.InsertData = function () {
        var Action = document.getElementById("btnSave").getAttribute("value");
        if (Action == "Submit") {
            $scope.Cartelera = {};
            $scope.Cartelera.SalaId = $scope.sala;
            $scope.Cartelera.HorarioId = $scope.horario;
            $scope.Cartelera.EstadoCarteleraId = $scope.estadocartelera;
            $scope.Cartelera.PeliculaId = $scope.pelicula;
            console.log($scope.Cartelera);

            $http({
                method: "post",
                url: 'Cartelera/Insert',
                datatype: "json",
                data: JSON.stringify($scope.Cartelera)
            }).then(function (response) {
                $("alerta").removeClass("hidden");

                $scope.alertmessage = response.data;
                //$('#alertModal').modal('show');

                $scope.GetAllData();
                //$scope.Pelicula.NombrePelicula = "";  
                //$scope.Pelicula.Clasificacion = "";  
                //$scope.Pelicula.Duracion = "";


                //$scope.NombrePelicula = "";
                //$scope.Clasificacion = "";
                //$scope.Duracion = "";




            })
        } else {
            $scope.Pelicula = {};
            $scope.Pelicula.NombrePelicula = $scope.NombrePelicula;
            $scope.Pelicula.Clasificacion = $scope.Clasificacion;
            $scope.Pelicula.Duracion = $scope.Duracion;
            $scope.Pelicula.PeliculaId = document.getElementById("PeliculaId").value;
            $http({
                method: "post",
                url: 'Pelicula/Update',
                datatype: "json",
                data: JSON.stringify($scope.Pelicula)
            }).then(function (response) {
                alert(response.data);
                $scope.GetAllData();
                $scope.NombrePelicula = "";
                $scope.Clasificacion = "";
                $scope.Duracion = "";
                document.getElementById("btnSave").setAttribute("value", "Submit");
                document.getElementById("btnSave").style.backgroundColor = "cornflowerblue";
                document.getElementById("spn").innerHTML = "Agregar nuevo empleado";
            })
        }
    }


    $scope.GetAllData = function () {
        $http({
            method: "get",
            url: 'Cartelera/GetAllCartelera'
        }).then(function (response) {
            $scope.carteleras = response.data;

        }, function () {
            alert("No se pudieron cargar carteleras");
        })
    };
    $scope.Delete = function (Cartelera) {
        $http({
            method: "post",
            url: 'Cartelera/Delete',
            datatype: "json",
            data: JSON.stringify(Cartelera)
        }).then(function (response) {
            alert(response.data);
            $scope.alertmessage = response.data;
            $scope.GetAllData();
        })
    };
    $scope.UpdateEmp = function (Emp) {
        document.getElementById("EmpID_").value = Emp.Emp_Id;
        $scope.EmpName = Emp.Emp_Name;
        $scope.EmpCity = Emp.Emp_City;
        $scope.EmpAge = Emp.Emp_Age;
        document.getElementById("btnSave").setAttribute("value", "Update");
        document.getElementById("btnSave").style.backgroundColor = "Yellow";
        document.getElementById("spn").innerHTML = "Update Employee Information";
    }

    //El siguiente m�todo permitir� cargar las Salas del Cine
    $("#repeatSelect1")
  .change(function () {
      var str = "";
      var value = $("#repeatSelect1 option:selected").val();
      var option = $(this).text();
      console.log(value);


      $.ajax({
          url: '/Cine/GetAllSalasByCineId',
          data: { id: value },

          success: function (result) {
              //Se van a cargar todos los imputs con la respuesta
              var col = result[0];

              //alert("Colaborador: "+col.ColaboradorId+" PrimerNombre: "+col.PrimerNombre);




              $scope.salas = result;
              console.log(result);




          },
          error: function () {


              //alert("No se puede conectar con servidor.");
          }

      });





  })
  .trigger("change");


    $("#repeatSelect2")
.change(function () {
    var str = "";
    var value = $("#repeatSelect2 option:selected").val();
    var option = $(this).text();
    console.log(value);








})
.trigger("change");


});


