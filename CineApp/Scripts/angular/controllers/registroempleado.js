var app = angular.module("myApp", []);  
app.controller("CtrlregistroEmpleado", function($scope, $http) {  
   
    $http({
        method: "GET",
        url: "/Cine/GetAllCines"
    }).then(function mySuccess(response) {
        $scope.cines = response.data;
        console.log($scope.cines);
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
        alert("No ha sido posible conectarse al servidor");
    });

    $http({
        method: "GET",
        url: "/Empleado/GetAllTipoEmpleado"
    }).then(function mySuccess(response) {
        $scope.tipoempleados = response.data;
        console.log($scope.tipoempleados);
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
        alert("No ha sido posible conectarse al servidor");
    });




   
})

var nombres = "";
var apellidos = "";
var fecha = "";
var correo = "";
var dpi = "";
var contrasena = "";
var contrasena2 = "";
var validacion = 0;

$('#btncrear').hover(function () {

   nombres = $('#nombres').val();
   apellidos = $('#apellidos').val();
   dpi = $('#dpi').val();
   correo = $('#correo').val();
   contrasena = $('#contrasena').val();
   contrasena2 = $('#contrasena2').val();
   validacion = 1;

    $('#alert-div').removeClass("d-none");

    console.log("Hicimos un hover");

    $('#alert-div').empty();

    if (nombres == "") {

        agregarAlerta("Campo Nombres en blanco.");
        validacion = 0;

    }

    if (apellidos == "") {
        agregarAlerta("Campo Apellidos en blanco.");
        validacion = 0;
    }

    if (correo == "") {
        agregarAlerta("Campo Correo en blanco.");
        validacion = 0;
    }

    if (dpi == "") {
        agregarAlerta("Campo DPI en blanco.");
        validacion = 0;
    }
   
    if ((contrasena != contrasena2) || (contrasena == "" && contrasena2 == "") || (contrasena.length<5)) {
        agregarAlerta("Contrasena no coincide o su contrasena es menor de 5 caracteres.");
        validacion = 0;
    }


    if (validacion == 1) {

     
        $('#alert-div').addClass('d-none');
    }


});


function agregarAlerta(mensaje) {

    $('#alert-div').append('<p><strong>' + mensaje + '</strong></p>');

}


$('#btncrear').click(function () {


    var dataform = new FormData();

    if (validacion == 1) {
        $('#frmcrear').submit();
    
    }

});


