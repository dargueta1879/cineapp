﻿using System.Web;
using System.Web.Optimization;

namespace CineApp
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery3-3-1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
             "~/Scripts/bootstrap.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/mijs").Include(
                       "~/Scripts/mijs/menu.js"));



            //Bundles para angularjs
            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                       "~/Scripts/angular/angular.js"));

            bundles.Add(new ScriptBundle("~/angular/pelicula").Include(
                       "~/Scripts/angular/controllers/pelicula.js"));
            bundles.Add(new ScriptBundle("~/angular/cartelera").Include(
                       "~/Scripts/angular/controllers/cartelera.js"));

            bundles.Add(new ScriptBundle("~/angular/entradaonline").Include(
                      "~/Scripts/angular/controllers/entradaonline.js"));

            bundles.Add(new ScriptBundle("~/angular/registrosocio").Include(
                      "~/Scripts/angular/controllers/registrosocio.js"));

            bundles.Add(new ScriptBundle("~/angular/registroempleado").Include(
                      "~/Scripts/angular/controllers/registroempleado.js"));

            bundles.Add(new ScriptBundle("~/angular/asociados").Include(
                      "~/Scripts/angular/controllers/asociados.js"));

            bundles.Add(new ScriptBundle("~/angular/empleados").Include(
                      "~/Scripts/angular/controllers/empleados.js"));




            //Terminar bundles angularjs

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de creación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));



            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                 "~/Content/bootstrap/css/bootstrap.css"


                 ));

            bundles.Add(new StyleBundle("~/Content/micss").Include(
                    "~/Content/micss/menu.css"
                    ));

            bundles.Add(new StyleBundle("~/Content/bootstrap/fonts").Include(
                 "~/Content/bootstrap/fonts/glyphicons-halflings-regular.eot",
                 "~/Content/bootstrap/fonts/glyphicons-halflings-regular.svg",
                 "~/Content/bootstrap/fonts/glyphicons-halflings-regular.ttf",
                 "~/Content/bootstrap/fonts/glyphicons-halflings-regular.woff",
                 "~/Content/bootstrap/fonts/glyphicons-halflings-regular.woff2"

                 ));
        }
    }
}