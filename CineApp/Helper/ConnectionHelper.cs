﻿using System.Configuration;
using System.Data.Common;
using System.Data.EntityClient;
//using System.Data.Entity.Core.EntityClient;
//using Elecciones.Core.Common;

namespace CineApp.Helper
{
    public static class ConnectionHelper
    {
        public static string CurrentConnectionString { get; set; }
        public static readonly string DBCineEntities3String = "DBCineEntitiesString";

        static ConnectionHelper()
        {
            ResetConnectionString();
        }

        public static void ResetConnectionString()
        {
            CurrentConnectionString = "DBCineEntities";
        }

        /// <summary>
        /// Gets the default connection string
        /// </summary>
        /// <returns>Connection string</returns>
        public static string GetConnectionString(bool withMetadata = false)
        {
            return GetConnectionString(CurrentConnectionString, withMetadata);
        }

        public static string GetConnectionString(string name, bool withMetadata)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[name].ConnectionString;

            if (withMetadata)
            {
                return connectionString;
            }

            var builder = new EntityConnectionStringBuilder(connectionString);

            return builder.ProviderConnectionString;
        }

        public static string GetConnectionString(DbConnection connection)
        {
            return connection.ConnectionString;
        }
    }
}
